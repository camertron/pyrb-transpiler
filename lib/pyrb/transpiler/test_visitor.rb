module Pyrb
  module Transpiler
    class TestVisitor < StatementVisitor
      def visitTest(ctx)
        # inline if, i.e. "var if foo else baz"
        # why this does not come through with regular if statements is a mystery
        if ctx.children.size > 1 && ctx.children[1].text == 'if'
          context.write_ruby('!!(')
          visit(ctx.children[2])
          context.write_ruby(')')
          context.write_ruby(' ? ')
          visit(ctx.children[0])
          context.write_ruby(' : ')
          visit(ctx.children[4])
        else
          visit_children(ctx)
        end
      end

      def visitOr_test(ctx)
        if ctx.children.size == 1
          visit_children(ctx)
        else
          ctx.and_test.each_with_index do |and_test, idx|
            comp = and_test.not_test[0].comparison

            if idx > 0
              context.write_ruby('.or { ')
            else
              if comp && comp.children.size > 1
                context.write_ruby('(')
              end
            end

            visit(and_test)

            if idx > 0
              context.write_ruby(' }')
            else
              if comp && comp.children.size > 1
                context.write_ruby(')')
              end
            end
          end
        end
      end

      def visitAnd_test(ctx)
        if ctx.children.size == 1
          visit_children(ctx)
        else
          ctx.not_test.each_with_index do |not_test, idx|
            comp = not_test.comparison

            if idx > 0
              context.write_ruby('.and { ')
            else
              if comp && comp.children.size > 1
                context.write_ruby('(')
              end
            end

            visit(not_test)

            if idx > 0
              context.write_ruby(' }')
            else
              if comp && comp.children.size > 1
                context.write_ruby(')')
              end
            end
          end
        end
      end

      def visitNot_test(ctx)
        if ctx.children.size == 1
          visit_children(ctx)
        else
          context.write_ruby('(')
          visit(ctx.children[1])
          context.write_ruby(').not')
        end
      end
    end
  end
end
