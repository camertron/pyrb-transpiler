module Pyrb
  module Transpiler
    class AtomExpressionVisitor < StatementVisitor
      STRING_RE = /(?<!\\)(?:\\x[a-fA-F0-9]{2})+/

      def visitAtom_expr(ctx)
        case for_comp_type(ctx.atom)
          when :list
            visit(ctx.atom.testlist_comp.comp_for)
            return

          when :dict_or_set
            visit(ctx.atom.dictorsetmaker.comp_for)
            return
        end

        if contains_tuple?(ctx.atom)
          context.write_ruby('[')
          visit(ctx.atom.testlist_comp)
          context.write_ruby(']')
          return
        end

        visit(ctx.atom)
        emit_trailer_list(ctx)
      end

      def visitAtom(ctx)
        child_idx = 0

        while child_idx < ctx.children.size
          child = ctx.children[child_idx]

          if child.is_a?(Python3Parser::Testlist_compContext)
            visit(child)
          elsif child.is_a?(Python3Parser::DictorsetmakerContext)
            visit(child)
          elsif child.is_a?(Antlr4::Runtime::TerminalNodeImpl) && child != ctx.NAME && ctx.NAME
            # this handles things like enclosing parens that are otherwise
            # not part of the parse tree
            context.write_ruby(child.text)
          elsif child == ctx.NUMBER
            emit_number(ctx.NUMBER.text)
          elsif ctx.STRING && ctx.STRING.include?(child)
            string_nodes = []
            n = child_idx

            # Collect all sequential string literals into an array so we know
            # how many there are and therefore whether or not to surround
            # them with parens. We do this to support strings that are
            # implicitly concatenated, i.e. "foo" "bar", which is legal syntax
            # in both Python and Ruby.
            while n < ctx.children.size && ctx.STRING.include?(ctx.children[n])
              string_nodes << ctx.children[n]
              n += 1
            end

            if string_nodes.size > 1
              context.write_ruby('(')
            end

            string_nodes.each_with_index do |str, str_idx|
              if str_idx > 0
                context.write_ruby(' + ')
              end

              if str.text.start_with?('f')
                emit_fstring(str.text)
              else
                emit_string(str.text)
              end
            end

            if string_nodes.size > 1
              context.write_ruby(')')
            end

            # this would normally be += string_nodes.size - 1, but since this is a
            # while loop, we need to increment one extra
            child_idx += string_nodes.size
          else
            if !should_emit_terminal(ctx, child)
              child_idx += 1
              next
            end

            # this happens for empty dicts
            if child.text == '{'
              if ctx.dictorsetmaker.nil?
                context.write_ruby('Pyrb::Dict.new')
              end

              child_idx += 1
              next
            elsif child.text == '}'
              child_idx += 1
              next
            end

            if sym = get_symbol_for(child.text)
              context.write_ruby(sym.ruby_expression)
            else
              context.write_ruby(child.text)
            end
          end

          child_idx += 1
        end
      end

      def emit_subscript_list(subscript_list)
        subscript_list.subscript.each do |subscript|
          if subscript.test.size == subscript.children.size
            # indicates we've encountered a basic 'ol subscript, eg. foo[0]
            visit_children(subscript)
          else
            # indicates we've encountered a range-based subscript like foo[0:], foo[0:2], or foo[0:1:2]
            # the number if test()s and their positions in the list of children will tell us which kind

            first = second = step = nil

            if subscript.test.size == 1
              if subscript.children[0] == subscript.test[0]
                first = subscript.test[0]
              else
                second = subscript.test[0]
              end
            elsif subscript.test.size == 2
              first = subscript.test[0]
              second = subscript.test[1]
            end

            # sliceop is always in the third position and indicates the step value
            if subscript.sliceop
              step = subscript.sliceop.test
            end

            # write the thing out
            if step
              emit_augmented_range(first, second, step)
            else
              emit_native_range(first, second)
            end
          end
        end
      end

      def emit_trailer_list(atom_expr, shape = nil)
        shape ||= TrailerShapeBuilder.new(atom_expr).all
        close_prop_access = nil
        trailer_idx = 0

        while trailer_idx < atom_expr.trailer.size
          trailer = atom_expr.trailer[trailer_idx]

          unless shape.includes_trailer?(trailer_idx)
            trailer_idx += 1
            next
          end

          if close_prop_access != nil
            context.write_ruby(close_prop_access)
            close_prop_access = nil
          end

          if (trailer.children.first.text == '(')
            context.write_ruby('.call(')

            if atom_expr.text == 'super' && trailer_idx == 0
              extra_positional = []

              if trailer.arglist
                extra_positional << context.current_scope.self_ref.ruby_expression
              elsif trailer.arglist.argument.size == 1
                extra_positional << context.current_scope.class_ref.ruby_expression
                extra_positional << context.current_scope.self_ref.ruby_expression
              end

              emit_argument_list(trailer.arglist, extra_positional)
            else
              if trailer.arglist
                emit_argument_list(trailer.arglist)
              end
            end

            context.write_ruby(')')
            trailer_idx += 1
            next
          end

          if atom_expr.trailer.size > trailer_idx + 1 && atom_expr.trailer[trailer_idx + 1].children.first.text == '('
            # this is a function call
            func_name = trailer.children[1].text
            argument_list = atom_expr.trailer[trailer_idx + 1].arglist
            emit_function_call(func_name, argument_list)
            trailer_idx += 2
            next
          end

          trailer.children.each_with_index do |child, child_idx|
            next unless shape.includes_child?(trailer_idx, child_idx)

            if child.is_a?(Python3Parser::SubscriptlistContext)
              emit_subscript_list(child)
            elsif child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
              expr = find_child_in_parent_expr(child)

              if child.text == '.'
                if expr && expr.assignment? && expr.side == :left
                  context.write_ruby(".attrs['")
                  close_prop_access = "']"
                else
                  context.write_ruby(".attr('")
                  close_prop_access = "')"
                end
              else
                context.write_ruby(child.text)
              end
            end
          end

          trailer_idx += 1
        end

        if close_prop_access
          context.write_ruby(close_prop_access)
        end
      end

      private

      def should_emit_terminal(atom_ctx, terminal)
        text = terminal.text

        if text == '[' || text == ']'
          # This indicates square brackets around a list comprehension. Since the ruby
          # implementation calls #map, we need to skip emitting square brackets here.
          for_comp_type(atom_ctx) == :none
        elsif text == '{' || text == '}'
          # for loops can be surrounded by {}, which means the loop constructs a dict or set
          for_comp_type(atom_ctx) == :none
        else
          true
        end
      end

      def for_comp_type(atom_ctx)
        if atom_ctx.testlist_comp && atom_ctx.testlist_comp.comp_for
          :list
        elsif atom_ctx.dictorsetmaker && atom_ctx.dictorsetmaker.comp_for
          :dict_or_set
        else
          :none
        end
      end

      def contains_tuple?(atom_ctx)
        # check all children here to account for the case when the python
        # programmer tries to create a tuple with a single element,
        # eg. (1,) - checking all children counts the trailing comma
        atom_ctx.testlist_comp && atom_ctx.testlist_comp.children.size > 1
      end

      def get_symbol_for(python_str)
        if context.symbol_table.include?(python_str)
          return context.symbol_table[python_str]
        end

        if Python.identifier?(python_str)
          if context.current_scope.type == :class
            context.symbol_table << Symbol.new(python_str, "klass.attrs['#{Ruby.to_identifier(python_str)}']")
          elsif context.current_scope.type == :file
            context.symbol_table << Symbol.new(python_str, "exports['#{Ruby.to_identifier(python_str)}']")
          else
            context.symbol_table << Symbol.new(python_str, Ruby.to_identifier(python_str))
          end
        end

        context.symbol_table[python_str]
      end

      def emit_function_call(func_name, argument_list)
        context.write_ruby(".fcall('")
        context.write_ruby(func_name)
        context.write_ruby("'")

        if argument_list && argument_list.argument.size > 0
          context.write_ruby(', ')
          emit_argument_list(argument_list)
        end

        context.write_ruby(')')
      end

      def emit_argument_list(argument_list, extra_positional = [])
        return if argument_list.nil? && extra_positional.empty?

        args_open = true
        kwargs_open = false
        emitted_extras = false

        context.write_ruby('[')

        if argument_list
          argument_list.argument.each_with_index do |arg, idx|
            # Splat!
            is_splat = arg.children[0].text == '*'
            is_kwsplat = arg.children[0].text == '**'

            if is_splat
              context.write_ruby(', ') if idx > 0
              context.write_ruby('*Pyrb.splat(')
            elsif is_kwsplat
              context.write_ruby('], { **(')
              args_open = false
              kwargs_open = true
            else
              context.write_ruby(', ') if idx > 0
            end

            # arguments with more than one test component must represent a
            # keyword argument, so turn them into ruby keyword args
            if arg.test.size > 1
              extra_positional.each_with_index do |ex, ex_idx|
                if ex_idx > 0 && idx > 0
                  context.write_ruby(', ')
                end

                context.write_ruby(ex)
              end

              emitted_extras = true

              if args_open && !kwargs_open
                context.write_ruby('], { ')
                args_open = false
                kwargs_open = true
              end

              visit(arg.test[0])
              context.write_ruby(': ')
              visit(arg.test[1])
            else
              # if the argument is a list comprehension, only visit the
              # comprehension and not the whole argument (avoids printing
              # the expression value twice; the ForStatementVisitor reaches
              # up into the parent to access it).
              if arg.comp_for
                visit(arg.comp_for)
              else
                visit(arg)
              end
            end

            if is_splat || is_kwsplat
              context.write_ruby(')')
            end
          end
        end

        unless emitted_extras
          extra_positional.each_with_index do |ex, ex_idx|
            if ex_idx > 0 || argument_list
              context.write_ruby(', ')
            end

            context.write_ruby(ex)
          end
        end

        if args_open
          context.write_ruby(']')
        elsif kwargs_open
          context.write_ruby(' }')
        end
      end

      def emit_native_range(first, second)
        if first
          visit(first)
        else
          context.write_ruby('0')
        end

        if second
          # ranges in python are always exclusive
          context.write_ruby('...')
          visit(second)
        else
          context.write_ruby('..-1')
        end
      end

      def emit_augmented_range(first, second, step)
        context.write_ruby('Pyrb::Range.new([')

        if first
          visit(first)
        else
          context.write_ruby('nil')
        end

        context.write_ruby(', ')

        if second
          visit(second)
        else
          context.write_ruby('nil')
        end

        if step
          context.write_ruby(', ')
          visit(step)
        end

        context.write_ruby('])')
      end

      def emit_string(orig)
        is_raw = false
        is_bytes = false
        triple_quoted = false
        single_quoted = false
        encoding = 'UTF-8'
        pack_format = 'U'

        orig.each_char.with_index do |current, idx|
          if current == 'r' || current == 'R'
            is_raw = true
          elsif current == 'b' || current == 'B'
            is_bytes = true
            encoding = 'ASCII-8BIT'
            pack_format = 'C'
          else
            orig = orig[idx..-1]
            break
          end
        end

        if orig.start_with?('"""') || orig.start_with?("'''")
          triple_quoted = true
          orig = orig[3...(orig.size - 3)]
        else
          if orig.start_with?("'")
            single_quoted = true
          end

          orig = orig[1...(orig.size - 1)]
        end

        if is_bytes
          context.write_ruby('Pyrb::Bytes.call([')
        end

        if is_raw
          if orig.include?("\n")
            emit_docstring(orig)
          else
            context.write_ruby(Ruby.wrap_raw_string(orig))
          end
        elsif triple_quoted && orig.include?("\n")
          emit_docstring(orig)
        else
          # Python strings can be surrounded by either single or double quotes - both
          # work the same way. Ruby on the other hand only allows interpolation and
          # most escape sequences inside double-quoted strings. To account for this,
          # we emit only double-quoted strings, and escape all single quotes inside them.
          str = Ruby.escape_interpolation_chars(orig)

          if single_quoted || triple_quoted
            str = Ruby.escape_double_quoted_string(str)
          end

          # The remainder of this method body is concerned with identifying runs of sequences
          # in the source string that use the \x notation to specify literal bytes (or, as
          # we'll learn later, Unicode codepoints).
          #
          # For example, because Python strings are encoded in UTF-8 by definition, the "\xE9"
          # string literal is equivalent to the string "é". Python interprets and displays the
          # byte sequences as Unicode codepoints. Unfortunately (or fortunately, depending on
          # how crazy you think that implicit conversion is), Ruby doesn't work the same way.
          # The same sequence in Ruby, "\xE9", is interpreted literally as a single-byte string.
          # No automagical codepoint conversions occur, and we're left with a string that
          # contains an invalid byte sequence (the correct byte sequence is 0xC3, 0xA9). So
          # basically what I'm saying is that Python is ridiculous. But you already knew that.
          #
          # To combat the problem, we need to make sure to specifically handle byte sequences
          # in Python strings. The code below turns a string like "abc \xE9 def" into:
          #
          # ("abc " + [0xE9].pack('U*') + " def")
          #
          # (NOTE: we should be able to rely on the #encoding comment at the top of each converted
          # file to encode all string literals as UTF-8 by default)
          #
          last_end = 0
          counter = 0

          if str =~ STRING_RE
            if encoding == 'UTF-8'
              context.write_ruby('(')
            else
              context.write_ruby('String.new(')
            end

            str.scan(STRING_RE) do |(segment)|
              if counter > 0
                context.write_ruby(' + ')
              end

              start = Regexp.last_match.begin(0)

              if last_end != start
                context.write_ruby('"')
                context.write_ruby(str[last_end...start])
                context.write_ruby('" + ')
              end

              context.write_ruby('[')

              hex_counter = 0

              segment.split('\x').each do |b|
                next if b.empty?

                if hex_counter > 0
                  context.write_ruby(', ')
                end

                hex_counter += 1

                context.write_ruby('0x')
                context.write_ruby(b)
              end

              context.write_ruby("].pack('" + pack_format + "*')")

              last_end = Regexp.last_match.end(0)
              counter += 1
            end

            if last_end != str.size
              if counter > 0
                context.write_ruby(' + ')
              end

              context.write_ruby('"')
              context.write_ruby(str[last_end..-1])
              context.write_ruby('"')
            end

            if encoding != 'UTF-8'
              context.write_ruby(", encoding: '" + encoding + "'")
            end

            context.write_ruby(')')
          else
            context.write_ruby('"')
            context.write_ruby(str)
            context.write_ruby('"')
          end
        end

        if is_bytes
          context.write_ruby('])')
        end
      end

      def emit_fstring(str)
        # remove f and quotes
        str = str[2..-2]

        pattern = /\{[^}]*\}/
        last_end = 0

        context.write_ruby('"')

        str.scan(pattern) do |var|
          match = Regexp.last_match
          context.write_ruby(str[last_end...match.begin(0)])

          # thanks for not doing this for me, python parser
          context.write_ruby('#{')
          fragment = var[1..-2]
          code_fragment = fragment

          # check to see if the expression ends with a suffix like !r (print the expression's
          # __repr__() value) or !s (print the expression's __str__() value)
          if fragment =~ /.*!\w\z/
            code_fragment = fragment[0...-2]
          end

          # code fragments have to end in a newline because otherwise PythonParser will
          # spit out a warning that it encountered an unexpected <EOF>
          lexer = Pyrb::Transpiler::Python3Lexer.new(
            Antlr4::Runtime::CharStreams.from_string("#{code_fragment}\n", 'String')
          )

          lexer.remove_error_listeners
          tokens = Antlr4::Runtime::CommonTokenStream.new(lexer)
          parser = Pyrb::Transpiler::Python3Parser.new(tokens)
          file = parser.file_input
          fragment_context = BaseContext.new(context.scope_stack)
          visitor = StatementVisitor.new(fragment_context)
          visitor.visit(file)
          context.write_ruby(fragment_context.to_s.strip)

          if fragment.end_with?('!r')
            # !r after a variable name in a format string means print that variable's
            # __repr__() value, analogous to Ruby's #inspect
            context.write_ruby(".fcall('__repr__')")
          elsif fragment.end_with?('!s')
            # !s after a variable name in a format string means print that variable's
            # __str__() value, analogous to Ruby's #to_s
            context.write_ruby(".fcall('__str__')")
          end

          context.write_ruby('}')

          last_end = match.end(0)
        end

        if last_end < str.size
          context.write_ruby(str[last_end..-1])
        end

        context.write_ruby('"')
      end

      def emit_docstring(str)
        lines = str.split(/\r?\n/)
        min_ls = nil

        # find first non-empty line that isn't the first line
        first_line = 1.upto(lines.size - 1).find do |i|
          !lines[i].strip.empty?
        end

        first_line ||= 0

        first_line.upto(lines.size - 1) do |i|
          line = lines[i]
          next if line.strip.empty?
          cur_min = 0

          line.each_char do |c|
            c == ' ' ? cur_min += 1 : break
          end

          if min_ls == nil || cur_min < min_ls
            min_ls = cur_min
          end

          # no need to keep going
          break if min_ls == 0
        end

        lines.map! do |line|
          next line unless line.start_with?(' ')
          (line[min_ls..-1] || '').rstrip
        end

        context.write_ruby('<<~__DOC__')
        context.write_heredoc(lines.join("\n").strip)
        context.write_heredoc("\n__DOC__\n")
      end

      def emit_number(num)
        last = num[num.size - 1]

        if (last >= '0' && last <= '9') || num.start_with?('0x')
          context.write_ruby(num)
          return
        end

        rest = num[0..-2]

        case last
          when 'J', 'j'
            # Complex number with a zero imaginary part
            context.write_ruby('Complex(')
            context.write_ruby(rest)
            context.write_ruby(')')

          when 'L', 'l'
            # Long int. No equivalent in ruby, but regular ruby integers can handle
            # numbers of long magnitude, so no need to do anything special here.
            context.write_ruby(rest)

          else
            raise RuntimeError, "Unexpected integer suffix '#{last}'"
        end
      end
    end
  end
end
