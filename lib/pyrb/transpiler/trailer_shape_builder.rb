module Pyrb
  module Transpiler
    class TrailerShapeBuilder
      attr_reader :atom_expr

      def initialize(atom_expr)
        @atom_expr = atom_expr
      end

      def all
        TrailerShape.new.tap do |shape|
          atom_expr.trailer.each_with_index do |trailer, trailer_idx|
            trailer.children.size.times do |child_idx|
              shape.add(trailer_idx, child_idx)
            end
          end
        end
      end

      def last_without_subscript
        TrailerShape.new.tap do |shape|
          last_trailer_idx = atom_expr.trailer.size - 1
          last_trailer = atom_expr.trailer[last_trailer_idx]

          last_trailer.children.size.times do |child_idx|
            shape.add(last_trailer_idx, child_idx)
          end

          first_child = last_trailer.children.first

          if first_child.text == '['
            shape.remove_child(last_trailer_idx, last_trailer.children.size - 1)
          end

          # removes either an opening bracket or a leading .
          shape.remove_child(last_trailer_idx, 0)
        end
      end

      def all_but_last_trailer
        shape = all
        shape.remove_trailer(atom_expr.trailer.size - 1)
        shape
      end
    end
  end
end
