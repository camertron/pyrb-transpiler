module Pyrb
  module Transpiler
    class Argument
      attr_reader :symbol, :type, :position, :default_exp, :splat, :kwsplat

      def initialize(symbol, type, position, splat, kwsplat, default_exp)
        @symbol = symbol
        @type = type
        @position = position
        @splat = splat
        @kwsplat = kwsplat
        @default_exp = default_exp
      end

      def to_param
        ''.tap do |result|
          if splat
            result.append('*')
          elsif kwsplat
            result.append('**')
          end

          result << symbol.ruby_expression

          if default_exp
            result << ' = '
            result << default_exp
          end
        end
      end

      def to_metadata
        ''.tap do |metadata|
          options = {}
          metadata << symbol.python_name
          metadata << ': '

          if splat
            options[:splat] = 'true'
          end

          if kwsplat
            options[:kwsplat] = 'true'
          end

          if default_exp
            options[:default] = default_exp
          end

          if options.empty?
            metadata << 'nil'
          else
            metadata << '{ '

            options.each_pair.with_index do |(key, value), idx|
              metadata << ', ' if idx > 0
              metadata << "#{key}: #{value}"
            end

            metadata << ' }'
          end
        end
      end
    end
  end
end
