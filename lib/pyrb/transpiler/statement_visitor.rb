module Pyrb
  module Transpiler
    class StatementVisitor < PyrbBaseVisitor
      def visitClassdef(ctx)
        ClassDefVisitor.new(context).visit(ctx)
      end

      def visitFuncdef(ctx)
        FuncDefVisitor.new(context).visit(ctx)
      end

      def visitIf_stmt(ctx)
        IfStatementVisitor.new(context).visit(ctx)
      end

      def visitAtom_expr(ctx)
        AtomExpressionVisitor.new(context).visit(ctx)
      end

      def visitRaise_stmt(ctx)
        RaiseExpressionVisitor.new(context).visit(ctx)
      end

      def visitComparison(ctx)
        # For some reason the python grammar wraps a bunch of things up in this
        # comparison context even when the thing in question has nothing to do
        # with comparing. In such cases, there will be no comparison operator
        # (i.e. comp_op() will return null), so avoid creating a visitor.
        if ctx.comp_op.size > 0
          ComparisonVisitor.new(context).visit(ctx)
          return
        end

        visit_children(ctx)
      end

      def visitFor_stmt(ctx)
        ForStatementVisitor.new(context).visit(ctx)
      end

      def visitComp_for(ctx)
        ForStatementVisitor.new(context).visit(ctx)
      end

      def visitExpr(ctx)
        ExpressionVisitor.new(context).visit(ctx)
      end

      def visitStar_expr(ctx)
        StarExpressionVisitor.new(context).visit(ctx)
      end

      def visitExpr_stmt(ctx)
        ExpressionVisitor.new(context).visit(ctx)
      end

      def visitAugassign(ctx)
        AugAssignVisitor.new(context).visit(ctx)
      end

      def visitReturn_stmt(ctx)
        ReturnVisitor.new(context).visit(ctx)
      end

      def visitTry_stmt(ctx)
        TryVisitor.new(context).visit(ctx)
      end

      def visitTerm(ctx)
        TermVisitor.new(context).visit(ctx)
      end

      def visitTestlist_comp(ctx)
        TestListCompVisitor.new(context).visit(ctx)
      end

      def visitTestlist_star_expr(ctx)
        TestListVisitor.new(context).visit(ctx)
      end

      def visitTestlist(ctx)
        TestListVisitor.new(context).visit(ctx)
      end

      def visitWith_stmt(ctx)
        WithStatementVisitor.new(context).visit(ctx)
      end

      def visitTest(ctx)
        TestVisitor.new(context).visit(ctx)
      end

      def visitAnd_test(ctx)
        TestVisitor.new(context).visit(ctx)
      end

      def visitOr_test(ctx)
        TestVisitor.new(context).visit(ctx)
      end

      def visitImport_stmt(ctx)
        # top-level (i.e. file scope level) imports are handled by the ImportExportScanner
        if context.current_scope.type != :file || context.inside_try?
          import_visitor = ImportVisitor.new
          import_visitor.visit(ctx)

          import_visitor.imports.each do |imp|
            imp_with_context = imp.apply_to(context)
            context.write_ruby_line(imp_with_context.to_ruby)
          end
        end
      end

      def visitArith_expr(ctx)
        ArithmeticExpressionVisitor.new(context).visit(ctx)
      end

      def visitLambdef(ctx)
        LambdaVisitor.new(context).visit(ctx)
      end

      def visitLambdef_nocond(ctx)
        LambdaVisitor.new(context).visit(ctx)
      end

      def visitDictorsetmaker(ctx)
        DictOrSetVisitor.new(context).visit(ctx)
      end

      def visitAnd_expr(ctx)
        AndExpressionVisitor.new(context).visit(ctx)
      end

      def visitPower(ctx)
        ExponentVisitor.new(context).visit(ctx)
      end

      def visitDecorated(ctx)
        DecorationVisitor.new(context).visit(ctx)
      end

      def visitFactor(ctx)
        FactorVisitor.new(context).visit(ctx)
      end

      def visitYield_stmt(ctx)
        YieldVisitor.new(context).visit(ctx)
      end

      def visitWhile_stmt(ctx)
        WhileVisitor.new(context).visit(ctx)
      end

      def visitDel_stmt(ctx)
        DelVisitor.new(context).visit(ctx)
      end

      def visitShift_expr(ctx)
        ShiftVisitor.new(context).visit(ctx)
      end

      def visitContinue_stmt(ctx)
        ContinueVisitor.new(context).visit(ctx)
      end

      def visitBreak_stmt(ctx)
        BreakVisitor.new(context).visit(ctx)
      end

      def visitAssert_stmt(ctx)
        AssertVisitor.new(context).visit(ctx)
      end

      def visitGlobal_stmt(ctx)
        GlobalVisitor.new(context).visit(ctx)
      end
    end
  end
end
