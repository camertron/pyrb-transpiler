module Pyrb
  module Transpiler
    class SymbolTable
      attr_reader :parent, :symbols
      attr_accessor :scope

      def initialize(parent)
        @parent = parent
        @symbols = {}
      end

      def <<(symbol)
        if !symbols.include?(symbol.python_name)
          override(symbol)
        end
      end

      def remove(python_name)
        symbols.delete(python_name)
      end

      def [](python_name)
        sym = symbols[python_name]
        return sym if sym
        return parent[python_name] if parent
      end

      def global_symbol(python_name)
        # why is this the same as []?
        sym = symbols[python_name]
        return sym if sym
        return parent[python_name] if parent
      end

      def override(symbol)
        if symbol.type == :local && scope.in_scope?(:block)
          symbol.type = :block_local
        end

        symbols[symbol.python_name] = symbol
      end

      def include?(python_name)
        !!self[python_name]
      end

      def includes_ruby?(ruby_expression)
        symbols.each do |_, symbol|
          return true if symbol.ruby_expression == ruby_expression
        end

        return parent.includes_ruby?(ruby_expression) if parent
        false
      end

      def block_locals
        symbols.each_with_object([]) do |(_, sym), ret|
          ret << sym if sym.type == :block_local
        end
      end

      BASE_TABLE = SymbolTable.new(nil).tap do |base_table|
        base_table << Symbol.builtin('object', 'Pyrb::Object')
        base_table << Symbol.builtin('len', 'Pyrb.len')
        base_table << Symbol.builtin('max', 'Pyrb.max')
        base_table << Symbol.builtin('min', 'Pyrb.min')
        base_table << Symbol.builtin('range', 'Pyrb.range')
        base_table << Symbol.builtin('print', 'Pyrb.print')
        base_table << Symbol.builtin('hasattr', 'Pyrb.hasattr')
        base_table << Symbol.builtin('str', 'Pyrb::Str')
        base_table << Symbol.builtin('dict', 'Pyrb::Dict')
        base_table << Symbol.builtin('dir', 'Pyrb.dir')
        base_table << Symbol.builtin('bool', 'Pyrb::Bool')
        base_table << Symbol.builtin('ValueError', 'Pyrb::ValueError')
        base_table << Symbol.builtin('AttributeError', 'Pyrb::AttributeError')
        base_table << Symbol.builtin('KeyError', 'Pyrb::KeyError')
        base_table << Symbol.builtin('IndexError', 'Pyrb::IndexError')
        base_table << Symbol.builtin('TypeError', 'Pyrb::TypeError')
        base_table << Symbol.builtin('UnicodeError', 'Pyrb::UnicodeError')
        base_table << Symbol.builtin('OverflowError', 'Pyrb::OverflowError')
        base_table << Symbol.builtin('ImportError', 'Pyrb::ImportError')
        base_table << Symbol.builtin('ZeroDivisionError', 'Pyrb::ZeroDivisionError')
        base_table << Symbol.builtin('StopIteration', '::StopIteration')
        base_table << Symbol.builtin('UnicodeEncodeError', 'Pyrb::UnicodeEncodeError')
        base_table << Symbol.builtin('DeprecationWarning', 'Pyrb::DeprecationWarning')
        base_table << Symbol.builtin('OSError', 'Pyrb::OSError')
        base_table << Symbol.builtin('True', 'true')
        base_table << Symbol.builtin('False', 'false')
        base_table << Symbol.builtin('isinstance', 'Pyrb.isinstance')
        base_table << Symbol.builtin('type', 'Pyrb.type')
        base_table << Symbol.builtin('None', 'nil')
        base_table << Symbol.builtin('__name__', '__name__')
        base_table << Symbol.builtin('map', 'Pyrb::Map')
        base_table << Symbol.builtin('filter', 'Pyrb::Filter')
        base_table << Symbol.builtin('int', 'Pyrb::Int')
        base_table << Symbol.builtin('float', 'Pyrb::Float')
        base_table << Symbol.builtin('sum', 'Pyrb.sum')
        base_table << Symbol.builtin('list', 'Pyrb::List')
        base_table << Symbol.builtin('tuple', 'Pyrb::List')
        base_table << Symbol.builtin('set', 'Pyrb::Set')
        base_table << Symbol.builtin('frozenset', 'Pyrb::Frozenset')
        base_table << Symbol.builtin('bytes', 'Pyrb::Bytes')
        base_table << Symbol.builtin('bytearray', 'Pyrb::Bytearray')
        base_table << Symbol.builtin('memoryview', 'Pyrb::Memoryview')
        base_table << Symbol.builtin('sorted', 'Pyrb.sorted')
        base_table << Symbol.builtin('zip', 'Pyrb::Zip')
        base_table << Symbol.builtin('next', 'Pyrb.next_')
        base_table << Symbol.builtin('reversed', 'Pyrb.reversed')
        base_table << Symbol.builtin('enumerate', 'Pyrb.enumerate')
        base_table << Symbol.builtin('classmethod', 'Pyrb.classmethod')
        base_table << Symbol.builtin('staticmethod', 'Pyrb.staticmethod')
        base_table << Symbol.builtin('property', 'Pyrb.property')
        base_table << Symbol.builtin('Exception', 'Pyrb::Exception')
        base_table << Symbol.builtin('getattr', 'Pyrb.getattr')
        base_table << Symbol.builtin('callable', 'Pyrb.callable')
        base_table << Symbol.builtin('any', 'Pyrb.any')
        base_table << Symbol.builtin('all', 'Pyrb.all')
        base_table << Symbol.builtin('abs', 'Pyrb.abs')
        base_table << Symbol.builtin('divmod', 'Pyrb.divmod')
        base_table << Symbol.builtin('chr', 'Pyrb.chr')
        base_table << Symbol.builtin('ord', 'Pyrb.ord')
        base_table << Symbol.builtin('getattr', 'Pyrb.getattr')
        base_table << Symbol.builtin('setattr', 'Pyrb.setattr')
        base_table << Symbol.builtin('round', 'Pyrb.round')
        base_table << Symbol.builtin('eval', 'Pyrb.eeval')  # lord jesus forgive me
        base_table << Symbol.builtin('iter', 'Pyrb.iter')
        base_table << Symbol.builtin('repr', 'Pyrb.repr')
        base_table << Symbol.builtin('super', 'Pyrb.zuper')
        base_table << Symbol.builtin('issubclass', 'Pyrb.issubclass')
        base_table << Symbol.builtin('exit', 'Pyrb.exit')
        base_table << Symbol.builtin('open', 'Pyrb.open')
      end
    end
  end
end
