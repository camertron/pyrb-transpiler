module Pyrb
  module Transpiler
    class LambdaVisitor < StatementVisitor
      def visitLambdef(ctx)
        context.write_ruby('Pyrb.defn(')

        if ctx.varargslist
          arg_list = ArgListVisitor.new(context)
          arg_list.visit(ctx.varargslist)
          context.write_ruby(arg_list.args.to_metadata)
          context.write_ruby(') { |')
          context.write_ruby(arg_list.args.to_args)
          context.write_ruby('| ')

          context.enter_scope(:block, false)

          arg_list.args.each do |arg|
            context.symbol_table << Symbol.new(arg.symbol.python_name, arg.symbol.ruby_expression)
          end
        else
          context.write_ruby(') { ')
          context.enter_scope(:block, false)
        end

        # this is the body, I have no idea why it's called a "test"
        visit(ctx.test)
        context.exit_scope(false)
        context.write_ruby(' }')
      end

      def visitLambdef_nocond(ctx)
        # TODO: figure out WTF this is and how it's different from a regular lambda
      end
    end
  end
end
