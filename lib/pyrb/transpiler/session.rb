module Pyrb
  module Transpiler
    class Session
      attr_reader :scope_stack, :context

      def initialize(bnd)
        @binding = bnd
        @scope_stack = [Scope.new(:file, symbol_table: SymbolTable::BASE_TABLE)]
        @context = BaseContext.new(scope_stack)
      end

      def evaluate(python_code)
        lexer = Pyrb::Transpiler::Python3Lexer.new(
          Antlr4::Runtime::CharStreams.from_string(python_code, 'String')
        )

        tokens = Antlr4::Runtime::CommonTokenStream.new(lexer)
        parser = Pyrb::Transpiler::Python3Parser.new(tokens)
        visitor = StatementVisitor.new(context)
        visitor.visit(parser.file_input)

        begin
          eval(context.to_s, @binding)
        ensure
          context.output.clear
        end
      end
    end
  end
end
