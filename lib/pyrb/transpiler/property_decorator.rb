module Pyrb
  module Transpiler
    class PropertyDecorator
      attr_reader :func_def, :context_ref

      def initialize(func_def, context_ref)
        @func_def = func_def
        @context_ref = context_ref
      end

      def apply_to(context)
        context.write_ruby(context_ref)
        context.write_ruby(".properties['")
        context.write_ruby(func_def.NAME.text)
        context.write_ruby("'] = Pyrb::Property.new([")
        context.enter_scope(:invocation)
        FuncDefVisitor.new(context).visit(func_def)
        context.exit_scope
        context.write_ruby_line("])\n")
      end

      def sub_decorator_for(sub_names, ctx)
        if sub_names.size == 1 && sub_names[0] == 'setter'
          return SetterDecorator.new(self, ctx)
        end

        raise RuntimeException, "Sub-decorator '#{sub_names[0]}' isn't supported by the property decorator"
      end
    end
  end
end
