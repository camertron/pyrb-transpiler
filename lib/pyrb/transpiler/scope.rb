module Pyrb
  module Transpiler
    class Scope
      attr_reader :type, :symbol_table, :parent, :decorators
      attr_writer :self_ref, :class_ref

      def initialize(type, options = {})
        @type = type
        @parent = options[:parent]
        @symbol_table = options[:symbol_table] || SymbolTable.new(parent.symbol_table)
        @decorators = {}

        symbol_table.scope = self
      end

      def add_decorator(name, decorator)
        decorators[name] = decorator
      end

      def has_decorator?(name)
        decorators.include?(name)
      end

      def in_scope?(type_to_check)
        type == type_to_check || (parent && parent.in_scope?(type_to_check))
      end

      def self_ref
        return @self_ref if @self_ref
        return parent.self_ref if parent
        nil
      end

      def class_ref
        return @class_ref if @class_ref
        return parent.class_ref if parent
        nil
      end

      def innermost_local_scope
        case type
          when :file, :class, :method
            self
          else
            parent.innermost_local_scope if parent
        end
      end
    end
  end
end
