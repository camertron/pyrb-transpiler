module Pyrb
  module Transpiler
    class ImportVisitor < StatementVisitor
      attr_reader :imports

      def initialize
        # this visitor is stateless and doesn't emit any code, so
        # pass nil as context
        super(nil)
        @imports = []
      end

      def visitImport_stmt(ctx)
        importables = {}

        # I have no idea what's going on here
        if ctx.import_name
          ctx.import_name.dotted_as_names.dotted_as_name.each do |as_name|
            import_name = nil
            alias_name = nil

            if as_name.children.size > 1 && as_name.children[as_name.children.size - 2].text == 'as'
              alias_name = as_name.children[as_name.children.size - 1].text
              import_name = as_name.dotted_name.text
              importables[import_name] = alias_name
            else
              alias_name = as_name.dotted_name.text
              import_name = alias_name

              last_dot = import_name.rindex('.')

              if last_dot
                importables[import_name] = import_name[(last_dot + 1)..-1]
              else
                importables[alias_name] = import_name
              end
            end
          end
        elsif ctx.import_from.import_as_names
          ctx.import_from.import_as_names.import_as_name.each do |imp|
            name = imp.NAME.first.text

            aliass = if imp.NAME.size > 1
              imp.NAME[1].text
            else
              name
            end

            importables[name] = aliass
          end
        else
          # @TODO fix me (globbed imports)
          importables['*'] = '*'
        end

        from = nil

        if ctx.import_from
          ctx.import_from.children.each do |child|
            if child.is_a?(Python3Parser::Dotted_nameContext)
              from = ctx.import_from.dotted_name.text
              break
            elsif child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
              if child.text != 'from'
                from = child.text
                break
              end
            end
          end
        end

        imports << Import.new(importables, from)
      end
    end
  end
end
