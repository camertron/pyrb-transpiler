module Pyrb
  module Transpiler
    class ReturnVisitor < StatementVisitor
      def visitReturn_stmt(ctx)
        case context.current_scope.type
          when :generator
            context.write_ruby('next')
            visit_children(ctx)

          else
            context.write_ruby('throw(:return')

            if ctx.testlist
              if ctx.children.size > 1
                context.write_ruby(', ')
              end

              if ctx.testlist.test.size > 1
                context.write_ruby('[')
              end

              ctx.testlist.test.each_with_index do |test, idx|
                if idx > 0
                  context.write_ruby(', ')
                end

                visit(test)
              end

              if ctx.testlist.test.size > 1
                context.write_ruby(']')
              end
            end

            context.write_ruby_line(')')
        end
      end
    end
  end
end
