module Pyrb
  module Transpiler
    class Symbol
      def self.builtin(python_name, ruby_expression)
        new(python_name, ruby_expression, :builtin)
      end

      attr_reader :python_name, :ruby_expression
      attr_accessor :type

      def initialize(python_name, ruby_expression, type = :local)
        @python_name = python_name
        @ruby_expression = ruby_expression
        @type = type
      end

      def copy
        self.class.new(python_name.dup, ruby_expression.dup, type)
      end
    end
  end
end
