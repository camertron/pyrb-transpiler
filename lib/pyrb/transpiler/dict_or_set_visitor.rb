module Pyrb
  module Transpiler
    class DictOrSetVisitor < StatementVisitor
      def visitDictorsetmaker(ctx)
        is_set = set?(ctx)

        if is_set
          context.write_ruby('Pyrb::Set.new([[')
        else
          context.write_ruby('Pyrb::Dict.new([{')
        end

        ctx.children.each do |child|
          if child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
            if child.text == ':'
              context.write_ruby(' => ')
            else
              context.write_ruby(Ruby.pad(child.text))
            end
          else
            visit(child)
          end
        end

        if is_set
          context.write_ruby(']])')
        else
          context.write_ruby('}])')
        end
      end

      private

      def set?(ctx)
        ctx.children.none? do |child|
          child.is_a?(Antlr4::Runtime::TerminalNodeImpl) && child.text == ':'
        end
      end
    end
  end
end
