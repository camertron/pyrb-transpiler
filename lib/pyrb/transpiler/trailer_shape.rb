require 'set'

module Pyrb
  module Transpiler
    class TrailerShape
      attr_reader :shape

      def initialize(shape = {})
        @shape = shape
      end

      def add(trailer_idx, child_idx)
        shape[trailer_idx] ||= ::Set.new
        shape[trailer_idx] << child_idx
      end

      def remove_trailer(trailer_idx)
        shape.delete(trailer_idx)
      end

      def remove_child(trailer_idx, child_idx)
        return unless shape.include?(trailer_idx)
        shape[trailer_idx].delete(child_idx)
      end

      def includes_trailer?(trailer_idx)
        shape.include?(trailer_idx)
      end

      def includes_child?(trailer_idx, child_idx)
        return false unless shape.include?(trailer_idx)
        shape[trailer_idx].include?(child_idx)
      end
    end
  end
end
