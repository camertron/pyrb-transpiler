module Pyrb
  module Transpiler
    class BreakVisitor < StatementVisitor
      def visitBreak_stmt(ctx)
        if context.current_scope.in_scope?(:for_loop_with_else)
          context.write_ruby_line('throw :break, true')
        else
          context.write_ruby_line('break')
        end
      end
    end
  end
end
