module Pyrb
  module Transpiler
    class StaticMethodDecorator
      attr_reader :func_def, :context_ref

      def initialize(func_def, context_ref)
        @func_def = func_def
        @context_ref = context_ref
      end

      def apply_to(context)
        context.write_ruby(context_ref)
        context.write_ruby(".attrs['")
        context.write_ruby(func_def.NAME.text)
        context.write_ruby_line("'] = Pyrb.staticmethod.call(")
        context.enter_block
        context.write_ruby(context_ref)
        context.write_ruby(', ')
        context.enter_scope(:invocation, false)
        FuncDefVisitor.new(context).visit(func_def)
        context.exit_scope(false)
        context.exit_block
        context.write_ruby_line(")\n")
      end

      def sub_decorator_for(sub_names, body)
        raise RuntimeException, "The staticmethod decorator doesn't have any sub-decorators"
      end
    end
  end
end
