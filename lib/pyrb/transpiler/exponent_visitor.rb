module Pyrb
  module Transpiler
    class ExponentVisitor < StatementVisitor
      def visitPower(ctx)
        if ctx.children.size == 1
          visit_children(ctx)
        else
          visit(ctx.children[0])
          context.write_ruby(' ** ')
          visit(ctx.children[2])
        end
      end
    end
  end
end
