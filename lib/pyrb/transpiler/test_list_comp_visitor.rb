module Pyrb
  module Transpiler
    class TestListCompVisitor < StatementVisitor
      def visitTestlist_comp(ctx)
        ctx.children.each do |child|
          if child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
            context.write_ruby(Ruby.pad(child.text))
          else
            visit(child)
          end
        end
      end
    end
  end
end
