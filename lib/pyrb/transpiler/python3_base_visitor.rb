# Generated from ./Python3.g4 by ANTLR 4.7.2

require "antlr4/runtime"

module Pyrb::Transpiler
  class Python3BaseVisitor < Antlr4::Runtime::AbstractParseTreeVisitor
    def visitSingle_input(ctx)
      return visit_children(ctx)
    end

    def visitFile_input(ctx)
      return visit_children(ctx)
    end

    def visitEval_input(ctx)
      return visit_children(ctx)
    end

    def visitDecorator(ctx)
      return visit_children(ctx)
    end

    def visitDecorators(ctx)
      return visit_children(ctx)
    end

    def visitDecorated(ctx)
      return visit_children(ctx)
    end

    def visitAsync_funcdef(ctx)
      return visit_children(ctx)
    end

    def visitFuncdef(ctx)
      return visit_children(ctx)
    end

    def visitParameters(ctx)
      return visit_children(ctx)
    end

    def visitTypedargslist(ctx)
      return visit_children(ctx)
    end

    def visitTfpdef(ctx)
      return visit_children(ctx)
    end

    def visitVarargslist(ctx)
      return visit_children(ctx)
    end

    def visitVfpdef(ctx)
      return visit_children(ctx)
    end

    def visitStmt(ctx)
      return visit_children(ctx)
    end

    def visitSimple_stmt(ctx)
      return visit_children(ctx)
    end

    def visitSmall_stmt(ctx)
      return visit_children(ctx)
    end

    def visitExpr_stmt(ctx)
      return visit_children(ctx)
    end

    def visitAnnassign(ctx)
      return visit_children(ctx)
    end

    def visitTestlist_star_expr(ctx)
      return visit_children(ctx)
    end

    def visitAugassign(ctx)
      return visit_children(ctx)
    end

    def visitDel_stmt(ctx)
      return visit_children(ctx)
    end

    def visitPass_stmt(ctx)
      return visit_children(ctx)
    end

    def visitFlow_stmt(ctx)
      return visit_children(ctx)
    end

    def visitBreak_stmt(ctx)
      return visit_children(ctx)
    end

    def visitContinue_stmt(ctx)
      return visit_children(ctx)
    end

    def visitReturn_stmt(ctx)
      return visit_children(ctx)
    end

    def visitYield_stmt(ctx)
      return visit_children(ctx)
    end

    def visitRaise_stmt(ctx)
      return visit_children(ctx)
    end

    def visitImport_stmt(ctx)
      return visit_children(ctx)
    end

    def visitImport_name(ctx)
      return visit_children(ctx)
    end

    def visitImport_from(ctx)
      return visit_children(ctx)
    end

    def visitImport_as_name(ctx)
      return visit_children(ctx)
    end

    def visitDotted_as_name(ctx)
      return visit_children(ctx)
    end

    def visitImport_as_names(ctx)
      return visit_children(ctx)
    end

    def visitDotted_as_names(ctx)
      return visit_children(ctx)
    end

    def visitDotted_name(ctx)
      return visit_children(ctx)
    end

    def visitGlobal_stmt(ctx)
      return visit_children(ctx)
    end

    def visitNonlocal_stmt(ctx)
      return visit_children(ctx)
    end

    def visitAssert_stmt(ctx)
      return visit_children(ctx)
    end

    def visitCompound_stmt(ctx)
      return visit_children(ctx)
    end

    def visitAsync_stmt(ctx)
      return visit_children(ctx)
    end

    def visitIf_stmt(ctx)
      return visit_children(ctx)
    end

    def visitWhile_stmt(ctx)
      return visit_children(ctx)
    end

    def visitFor_stmt(ctx)
      return visit_children(ctx)
    end

    def visitTry_stmt(ctx)
      return visit_children(ctx)
    end

    def visitWith_stmt(ctx)
      return visit_children(ctx)
    end

    def visitWith_item(ctx)
      return visit_children(ctx)
    end

    def visitExcept_clause(ctx)
      return visit_children(ctx)
    end

    def visitSuite(ctx)
      return visit_children(ctx)
    end

    def visitTest(ctx)
      return visit_children(ctx)
    end

    def visitTest_nocond(ctx)
      return visit_children(ctx)
    end

    def visitLambdef(ctx)
      return visit_children(ctx)
    end

    def visitLambdef_nocond(ctx)
      return visit_children(ctx)
    end

    def visitOr_test(ctx)
      return visit_children(ctx)
    end

    def visitAnd_test(ctx)
      return visit_children(ctx)
    end

    def visitNot_test(ctx)
      return visit_children(ctx)
    end

    def visitComparison(ctx)
      return visit_children(ctx)
    end

    def visitComp_op(ctx)
      return visit_children(ctx)
    end

    def visitStar_expr(ctx)
      return visit_children(ctx)
    end

    def visitExpr(ctx)
      return visit_children(ctx)
    end

    def visitXor_expr(ctx)
      return visit_children(ctx)
    end

    def visitAnd_expr(ctx)
      return visit_children(ctx)
    end

    def visitShift_expr(ctx)
      return visit_children(ctx)
    end

    def visitArith_expr(ctx)
      return visit_children(ctx)
    end

    def visitTerm(ctx)
      return visit_children(ctx)
    end

    def visitFactor(ctx)
      return visit_children(ctx)
    end

    def visitPower(ctx)
      return visit_children(ctx)
    end

    def visitAtom_expr(ctx)
      return visit_children(ctx)
    end

    def visitAtom(ctx)
      return visit_children(ctx)
    end

    def visitTestlist_comp(ctx)
      return visit_children(ctx)
    end

    def visitTrailer(ctx)
      return visit_children(ctx)
    end

    def visitSubscriptlist(ctx)
      return visit_children(ctx)
    end

    def visitSubscript(ctx)
      return visit_children(ctx)
    end

    def visitSliceop(ctx)
      return visit_children(ctx)
    end

    def visitExprlist(ctx)
      return visit_children(ctx)
    end

    def visitTestlist(ctx)
      return visit_children(ctx)
    end

    def visitDictorsetmaker(ctx)
      return visit_children(ctx)
    end

    def visitClassdef(ctx)
      return visit_children(ctx)
    end

    def visitArglist(ctx)
      return visit_children(ctx)
    end

    def visitArgument(ctx)
      return visit_children(ctx)
    end

    def visitComp_iter(ctx)
      return visit_children(ctx)
    end

    def visitComp_for(ctx)
      return visit_children(ctx)
    end

    def visitComp_if(ctx)
      return visit_children(ctx)
    end

    def visitEncoding_decl(ctx)
      return visit_children(ctx)
    end

    def visitYield_expr(ctx)
      return visit_children(ctx)
    end

    def visitYield_arg(ctx)
      return visit_children(ctx)
    end
  end
end
