module Pyrb
  module Transpiler
    class ComparisonVisitor < StatementVisitor
      def visitComparison(ctx)
        operator = ctx.children[1].text

        if operator.end_with?('in')
          if operator == 'notin'
            context.write_ruby('!')
          end

          context.write_ruby('(')
          visit(ctx.expr[0])
          context.write_ruby(').in?(')
          visit(ctx.expr[1])
          context.write_ruby(')')
        elsif operator.start_with?('is')
          visit(ctx.expr[0])
          context.write_ruby('.object_id')

          if operator == 'is'
            context.write_ruby(' == ')
          else
            context.write_ruby(' != ')
          end

          visit(ctx.expr[1])
          context.write_ruby('.object_id')
        else
          1.upto(ctx.expr.size - 1) do |i|
            if i > 1
              context.write_ruby(' && ')  # @TODO: revisit this logic
            end

            visit(ctx.expr[i - 1])
            context.write_ruby(Ruby.pad(ctx.comp_op[i - 1].text))
            visit(ctx.expr[i])
          end
        end
      end
    end
  end
end
