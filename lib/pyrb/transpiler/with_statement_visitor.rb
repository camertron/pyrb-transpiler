module Pyrb
  module Transpiler
    class WithStatementVisitor < StatementVisitor
      def visitWith_stmt(ctx)
        # I have no idea why there's more than one of these
        with = ctx.with_item.first
        as_var = nil

        # the "as" variable gets added to the local scope (because with statements
        # don't introduce their own scope)
        if with.expr
          as_var = with.expr.text
          context.symbol_table << Symbol.new(as_var, Ruby.to_identifier(as_var))
          as_var = context.symbol_table[as_var].ruby_expression
          context.write_ruby(as_var)
          context.write_ruby(' = ')
        end

        context.write_ruby('Pyrb.with.call([')
        visit(with.test)
        context.write_ruby(']) do')

        if with.expr
          context.write_ruby(" |#{as_var}|")
        end

        context.write_ruby_line

        context.enter_scope(:block)
        @context = BlockContext.enter(@context)
        visit(ctx.suite)
        @context = @context.exit
        context.exit_scope
        context.write_ruby_line('end')
      end
    end
  end
end
