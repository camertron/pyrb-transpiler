module Pyrb
  module Transpiler
    class FileContext < BaseContext
      attr_reader :is_open

      def initialize(file_path)
        super()
        @is_open = true

        write_ruby_line("# encoding: utf-8\n")
        write_ruby_line("require 'pyrb'\n")

        write_ruby('module ')
        write_ruby_line(Ruby.modularize(file_path))
        enter_scope(:file)

        write_ruby_line('def self.location')
        enter_scope(:method)
        write_ruby_line('__FILE__')
        exit_scope
        write_ruby_line("end\n")

        write_ruby_line('extend Pyrb::Runtime')
        write_ruby_line("using Pyrb::Runtime\n")
      end

      def enter_block(*args)
        check_open
        super
      end

      def exit_block(*args)
        check_open
        super
      end

      def enter_scope(*args)
        check_open
        super
      end

      def exit_scope(*args)
        check_open
        super
      end

      def write_ruby(*args)
        check_open
        super
      end

      def write_ruby_line(*args)
        check_open
        super
      end

      def write_ruby_line(*args)
        check_open
        super
      end

      def ensure_newline(*args)
        check_open
        super
      end

      def to_s
        raise(RuntimeError, 'File context is still open') if is_open
        super
      end

      def close
        write_ruby_line("Pyrb::Sys.exports['modules'][__FILE__] = self")
        exit_scope
        write_ruby_line('end')
        @is_open = false
      end

      private

      def check_open
        raise(RuntimeError, 'File context has been closed') unless is_open
      end
    end
  end
end
