module Pyrb
  module Transpiler
    class DelVisitor < StatementVisitor
      def visitDel_stmt(ctx)
        atom_expr = find_next_atom_expr(ctx.exprlist)
        atom_vis = AtomExpressionVisitor.new(context)

        if atom_expr.trailer.empty?
          sym = context.symbol_table[atom_expr.atom.text]

          if sym
            # pray to god these are always subscripts a la exports["name"]
            # I mean, they should be... famous last words
            idx = sym.ruby_expression.index('[')
            target = sym.ruby_expression[0...idx]
            name = sym.ruby_expression[(idx + 1)...(sym.ruby_expression.size - 1)]
          else
            target = 'exports'
            name = "'#{atom_expr.atom.text}'"
          end

          context.write_ruby(target)
          context.write_ruby('.del(')
          context.write_ruby(name)
          context.write_ruby_line(')')
        else
          atom_vis.visitAtom(atom_expr.atom)

          atom_vis.emit_trailer_list(atom_expr, TrailerShapeBuilder.new(atom_expr).all_but_last_trailer)
          context.write_ruby('.del(')

          # if the last trailer isn't a subscript, it must be dot access, so enter in quotes
          # eg del foo.bar becomes foo.del('bar')
          is_dot_access = false

          if atom_expr.trailer[atom_expr.trailer.size - 1].subscriptlist.nil?
            is_dot_access = true
            context.write_ruby("'")
          end

          atom_vis.emit_trailer_list(atom_expr, TrailerShapeBuilder.new(atom_expr).last_without_subscript)

          if is_dot_access
            context.write_ruby("'")
          end

          context.write_ruby_line(')')
        end
      end
    end
  end
end
