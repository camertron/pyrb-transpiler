module Pyrb
  module Transpiler
    class SetterDecorator
      attr_reader :parent, :func_def

      def initialize(parent, func_def)
        @parent = parent
        @func_def = func_def
      end

      def apply_to(context)
        context.write_ruby(parent.context_ref)
        context.write_ruby(".properties['")
        context.write_ruby(parent.func_def.NAME.text)
        context.write_ruby("'].setter = ")
        context.enter_scope(:invocation, false)
        FuncDefVisitor.new(context).visit(func_def)
        context.exit_scope(false)
      end

      def sub_decorator_for(sub_names, body)
        raise RuntimeException, "Sub-decorator '#{sub_names[0]}' isn't supported by the property setter decorator"
      end
    end
  end
end
