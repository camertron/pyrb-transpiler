module Pyrb
  module Transpiler
    class TestListVisitor < StatementVisitor
      def visitTestlist_star_expr(ctx)
        ctx.children.each do |child|
          if child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
            context.write_ruby(Ruby.pad(child.text))
          else
            visit(child)
          end
        end
      end

      def visitTestlist(ctx)
        ctx.children.each do |child|
          if child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
            context.write_ruby(Ruby.pad(child.text))
          else
            visit(child)
          end
        end
      end
    end
  end
end
