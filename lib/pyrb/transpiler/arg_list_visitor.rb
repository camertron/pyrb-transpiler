module Pyrb
  module Transpiler
    class ArgListVisitor < StatementVisitor
      attr_reader :args

      def initialize(context)
        super
        @args = ArgumentList.new
      end

      def visitArglist(ctx)
        ctx.argument.each do |arg|
          args.add(Symbol.new(arg.text, Ruby.to_identifier(arg.text), :arg), :arg, false, false)
        end
      end

      def visitTypedargslist(ctx)
        visit_generic(ctx.children)
      end

      def visitVarargslist(ctx)
        visit_generic(ctx.children)
      end

      def visitExprlist(ctx)
        visit_generic(ctx.children)
      end

      private

      def visit_generic(nodes)
        cur_name = nil
        splat = false
        kwsplat = false
        default_value = nil
        arg_type = :arg

        nodes.each do |node|
          if node.is_a?(Python3Parser::TfpdefContext) || node.is_a?(Python3Parser::VfpdefContext) || node.is_a?(Python3Parser::ExprContext)
            # use the first child to skip over potential type annotations, which we don't support
            cur_name = node.children[0].text
          elsif node.is_a?(Python3Parser::TestContext)
            default_val = BaseContext.new(context.scope_stack)
            StatementVisitor.new(default_val).visit(node)
            default_value = default_val.to_s
            arg_type = :kw_arg
          elsif node.is_a?(Antlr4::Runtime::TerminalNodeImpl)
            if node.text == '*'
              splat = true
            elsif node.text == '**'
              kwsplat = true
            elsif node.text == ','
              if cur_name
                var_name = Ruby.get_unique_variable_name(cur_name, context.current_scope.symbol_table)
              else
                cur_name = '_'
                var_name = '_'
              end

              args.add(Symbol.new(cur_name, var_name, :arg), arg_type, splat, kwsplat, default_value)
              cur_name = nil
              splat = false
              kwsplat = false
              default_value = nil
              arg_type = :arg
            end
          end
        end

        # gonna need to rethink this if there's a bare splat (i.e. just '*') as the last arg
        if cur_name
          var_name = Ruby.get_unique_variable_name(cur_name, context.current_scope.symbol_table)
          args.add(Symbol.new(cur_name, var_name, :arg), arg_type, splat, kwsplat, default_value)
        end
      end
    end
  end
end
