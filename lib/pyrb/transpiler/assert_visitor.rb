module Pyrb
  module Transpiler
    class AssertVisitor < StatementVisitor
      def visitAssert_stmt(ctx)
        context.write_ruby('Pyrb.assert(')

        ctx.test.each_with_index do |test, index|
          context.write_ruby(', ') if index > 0
          visit(test)
        end

        context.write_ruby_line(')')
      end
    end
  end
end
