# Generated from ./Python3.g4 by ANTLR 4.7.2

require "antlr4/runtime"

module Pyrb::Transpiler
  @@thePython3Listener = Python3Listener.new
  @@thePython3Visitor = Python3Visitor.new

  class Python3Parser < Antlr4::Runtime::Parser
    class << self
      @@_decisionToDFA = []
    end
    @@_sharedContextCache = Antlr4::Runtime::PredictionContextCache.new()
    STRING = 1
    NUMBER = 2
    INTEGER = 3
    DEF = 4
    RETURN = 5
    RAISE = 6
    FROM = 7
    IMPORT = 8
    AS = 9
    GLOBAL = 10
    NONLOCAL = 11
    ASSERT = 12
    IF = 13
    ELIF = 14
    ELSE = 15
    WHILE = 16
    FOR = 17
    IN = 18
    TRY = 19
    FINALLY = 20
    WITH = 21
    EXCEPT = 22
    LAMBDA = 23
    OR = 24
    AND = 25
    NOT = 26
    IS = 27
    NONE = 28
    TRUE = 29
    FALSE = 30
    CLASS = 31
    YIELD = 32
    DEL = 33
    PASS = 34
    CONTINUE = 35
    BREAK = 36
    ASYNC = 37
    AWAIT = 38
    NEWLINE = 39
    NAME = 40
    STRING_LITERAL = 41
    BYTES_LITERAL = 42
    DECIMAL_INTEGER = 43
    OCT_INTEGER = 44
    HEX_INTEGER = 45
    BIN_INTEGER = 46
    FLOAT_NUMBER = 47
    IMAG_NUMBER = 48
    DOT = 49
    ELLIPSIS = 50
    STAR = 51
    OPEN_PAREN = 52
    CLOSE_PAREN = 53
    COMMA = 54
    COLON = 55
    SEMI_COLON = 56
    POWER = 57
    ASSIGN = 58
    OPEN_BRACK = 59
    CLOSE_BRACK = 60
    OR_OP = 61
    XOR = 62
    AND_OP = 63
    LEFT_SHIFT = 64
    RIGHT_SHIFT = 65
    ADD = 66
    MINUS = 67
    DIV = 68
    MOD = 69
    IDIV = 70
    NOT_OP = 71
    OPEN_BRACE = 72
    CLOSE_BRACE = 73
    LESS_THAN = 74
    GREATER_THAN = 75
    EQUALS = 76
    GT_EQ = 77
    LT_EQ = 78
    NOT_EQ_1 = 79
    NOT_EQ_2 = 80
    AT = 81
    ARROW = 82
    ADD_ASSIGN = 83
    SUB_ASSIGN = 84
    MULT_ASSIGN = 85
    AT_ASSIGN = 86
    DIV_ASSIGN = 87
    MOD_ASSIGN = 88
    AND_ASSIGN = 89
    OR_ASSIGN = 90
    XOR_ASSIGN = 91
    LEFT_SHIFT_ASSIGN = 92
    RIGHT_SHIFT_ASSIGN = 93
    POWER_ASSIGN = 94
    IDIV_ASSIGN = 95
    SKIP_ = 96
    UNKNOWN_CHAR = 97
    INDENT = 98
    DEDENT = 99
    RULE_single_input = 0
    RULE_file_input = 1
    RULE_eval_input = 2
    RULE_decorator = 3
    RULE_decorators = 4
    RULE_decorated = 5
    RULE_async_funcdef = 6
    RULE_funcdef = 7
    RULE_parameters = 8
    RULE_typedargslist = 9
    RULE_tfpdef = 10
    RULE_varargslist = 11
    RULE_vfpdef = 12
    RULE_stmt = 13
    RULE_simple_stmt = 14
    RULE_small_stmt = 15
    RULE_expr_stmt = 16
    RULE_annassign = 17
    RULE_testlist_star_expr = 18
    RULE_augassign = 19
    RULE_del_stmt = 20
    RULE_pass_stmt = 21
    RULE_flow_stmt = 22
    RULE_break_stmt = 23
    RULE_continue_stmt = 24
    RULE_return_stmt = 25
    RULE_yield_stmt = 26
    RULE_raise_stmt = 27
    RULE_import_stmt = 28
    RULE_import_name = 29
    RULE_import_from = 30
    RULE_import_as_name = 31
    RULE_dotted_as_name = 32
    RULE_import_as_names = 33
    RULE_dotted_as_names = 34
    RULE_dotted_name = 35
    RULE_global_stmt = 36
    RULE_nonlocal_stmt = 37
    RULE_assert_stmt = 38
    RULE_compound_stmt = 39
    RULE_async_stmt = 40
    RULE_if_stmt = 41
    RULE_while_stmt = 42
    RULE_for_stmt = 43
    RULE_try_stmt = 44
    RULE_with_stmt = 45
    RULE_with_item = 46
    RULE_except_clause = 47
    RULE_suite = 48
    RULE_test = 49
    RULE_test_nocond = 50
    RULE_lambdef = 51
    RULE_lambdef_nocond = 52
    RULE_or_test = 53
    RULE_and_test = 54
    RULE_not_test = 55
    RULE_comparison = 56
    RULE_comp_op = 57
    RULE_star_expr = 58
    RULE_expr = 59
    RULE_xor_expr = 60
    RULE_and_expr = 61
    RULE_shift_expr = 62
    RULE_arith_expr = 63
    RULE_term = 64
    RULE_factor = 65
    RULE_power = 66
    RULE_atom_expr = 67
    RULE_atom = 68
    RULE_testlist_comp = 69
    RULE_trailer = 70
    RULE_subscriptlist = 71
    RULE_subscript = 72
    RULE_sliceop = 73
    RULE_exprlist = 74
    RULE_testlist = 75
    RULE_dictorsetmaker = 76
    RULE_classdef = 77
    RULE_arglist = 78
    RULE_argument = 79
    RULE_comp_iter = 80
    RULE_comp_for = 81
    RULE_comp_if = 82
    RULE_encoding_decl = 83
    RULE_yield_expr = 84
    RULE_yield_arg = 85

    @@ruleNames = [
      "single_input", "file_input", "eval_input", "decorator", "decorators",
      "decorated", "async_funcdef", "funcdef", "parameters", "typedargslist",
      "tfpdef", "varargslist", "vfpdef", "stmt", "simple_stmt", "small_stmt",
      "expr_stmt", "annassign", "testlist_star_expr", "augassign", "del_stmt",
      "pass_stmt", "flow_stmt", "break_stmt", "continue_stmt", "return_stmt",
      "yield_stmt", "raise_stmt", "import_stmt", "import_name", "import_from",
      "import_as_name", "dotted_as_name", "import_as_names", "dotted_as_names",
      "dotted_name", "global_stmt", "nonlocal_stmt", "assert_stmt", "compound_stmt",
      "async_stmt", "if_stmt", "while_stmt", "for_stmt", "try_stmt", "with_stmt",
      "with_item", "except_clause", "suite", "test", "test_nocond", "lambdef",
      "lambdef_nocond", "or_test", "and_test", "not_test", "comparison", "comp_op",
      "star_expr", "expr", "xor_expr", "and_expr", "shift_expr", "arith_expr",
      "term", "factor", "power", "atom_expr", "atom", "testlist_comp", "trailer",
      "subscriptlist", "subscript", "sliceop", "exprlist", "testlist", "dictorsetmaker",
      "classdef", "arglist", "argument", "comp_iter", "comp_for", "comp_if",
      "encoding_decl", "yield_expr", "yield_arg",
    ]

    @@_LITERAL_NAMES = [
      nil, nil, nil, nil, "'def'", "'return'", "'raise'", "'from'", "'import'",
      "'as'", "'global'", "'nonlocal'", "'assert'", "'if'", "'elif'", "'else'",
      "'while'", "'for'", "'in'", "'try'", "'finally'", "'with'", "'except'",
      "'lambda'", "'or'", "'and'", "'not'", "'is'", "'None'", "'True'", "'False'",
      "'class'", "'yield'", "'del'", "'pass'", "'continue'", "'break'", "'async'",
      "'await'", nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, "'.'", "'...'",
      "'*'", "'('", "')'", "','", "':'", "';'", "'**'", "'='", "'['", "']'",
      "'|'", "'^'", "'&'", "'<<'", "'>>'", "'+'", "'-'", "'/'", "'%'", "'//'",
      "'~'", "'{'", "'}'", "'<'", "'>'", "'=='", "'>='", "'<='", "'<>'", "'!='",
      "'@'", "'->'", "'+='", "'-='", "'*='", "'@='", "'/='", "'%='", "'&='",
      "'|='", "'^='", "'<<='", "'>>='", "'**='", "'//='",
    ]

    @@_SYMBOLIC_NAMES = [
      nil, "STRING", "NUMBER", "INTEGER", "DEF", "RETURN", "RAISE", "FROM",
      "IMPORT", "AS", "GLOBAL", "NONLOCAL", "ASSERT", "IF", "ELIF", "ELSE",
      "WHILE", "FOR", "IN", "TRY", "FINALLY", "WITH", "EXCEPT", "LAMBDA", "OR",
      "AND", "NOT", "IS", "NONE", "TRUE", "FALSE", "CLASS", "YIELD", "DEL",
      "PASS", "CONTINUE", "BREAK", "ASYNC", "AWAIT", "NEWLINE", "NAME", "STRING_LITERAL",
      "BYTES_LITERAL", "DECIMAL_INTEGER", "OCT_INTEGER", "HEX_INTEGER", "BIN_INTEGER",
      "FLOAT_NUMBER", "IMAG_NUMBER", "DOT", "ELLIPSIS", "STAR", "OPEN_PAREN",
      "CLOSE_PAREN", "COMMA", "COLON", "SEMI_COLON", "POWER", "ASSIGN", "OPEN_BRACK",
      "CLOSE_BRACK", "OR_OP", "XOR", "AND_OP", "LEFT_SHIFT", "RIGHT_SHIFT",
      "ADD", "MINUS", "DIV", "MOD", "IDIV", "NOT_OP", "OPEN_BRACE", "CLOSE_BRACE",
      "LESS_THAN", "GREATER_THAN", "EQUALS", "GT_EQ", "LT_EQ", "NOT_EQ_1",
      "NOT_EQ_2", "AT", "ARROW", "ADD_ASSIGN", "SUB_ASSIGN", "MULT_ASSIGN",
      "AT_ASSIGN", "DIV_ASSIGN", "MOD_ASSIGN", "AND_ASSIGN", "OR_ASSIGN", "XOR_ASSIGN",
      "LEFT_SHIFT_ASSIGN", "RIGHT_SHIFT_ASSIGN", "POWER_ASSIGN", "IDIV_ASSIGN",
      "SKIP_", "UNKNOWN_CHAR", "INDENT", "DEDENT",
    ]

    @@VOCABULARY = Antlr4::Runtime::VocabularyImpl.new(@@_LITERAL_NAMES, @@_SYMBOLIC_NAMES)

    def get_vocabulary
      @@VOCABULARY
    end

    def getGrammarFileName()
      return "Python3.g4"
    end

    def rule_names()
      return @@ruleNames
    end

    def serialized_atn()
      return @@_serializedATN
    end

    def atn()
      return @@_ATN
    end

    def initialize(input)
      super(input)
      i = 0
      while i < @@_ATN.number_of_decisions()
        @@_decisionToDFA[i] = Antlr4::Runtime::DFA.new(@@_ATN.decision_state(i), i)
        i += 1
      end

      @_interp = Antlr4::Runtime::ParserATNSimulator.new(self, @@_ATN, @@_decisionToDFA, @@_sharedContextCache)
    end

    class Single_inputContext < Antlr4::Runtime::ParserRuleContext
      def NEWLINE()
        return token(Python3Parser::NEWLINE, 0)
      end

      def simple_stmt()
        return rule_context("Simple_stmtContext", 0)
      end

      def compound_stmt()
        return rule_context("Compound_stmtContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_single_input
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterSingle_input))
          listener.enterSingle_input(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitSingle_input))
          listener.exitSingle_input(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitSingle_input))
          return visitor.visitSingle_input(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def single_input()
      _localctx = Single_inputContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 0, RULE_single_input)
      begin
        @_state_number = 177
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::NEWLINE
          enter_outer_alt(_localctx, 1)

          @_state_number = 172
          match(NEWLINE)
        when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::RETURN, Python3Parser::RAISE, Python3Parser::FROM, Python3Parser::IMPORT, Python3Parser::GLOBAL, Python3Parser::NONLOCAL, Python3Parser::ASSERT, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::YIELD, Python3Parser::DEL, Python3Parser::PASS, Python3Parser::CONTINUE, Python3Parser::BREAK, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::STAR, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
          enter_outer_alt(_localctx, 2)

          @_state_number = 173
          simple_stmt()
        when Python3Parser::DEF, Python3Parser::IF, Python3Parser::WHILE, Python3Parser::FOR, Python3Parser::TRY, Python3Parser::WITH, Python3Parser::CLASS, Python3Parser::ASYNC, Python3Parser::AT
          enter_outer_alt(_localctx, 3)

          @_state_number = 174
          compound_stmt()
          @_state_number = 175
          match(NEWLINE)
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class File_inputContext < Antlr4::Runtime::ParserRuleContext
      def EOF()
        return token(Python3Parser::EOF, 0)
      end

      def NEWLINE()
        return tokens(Python3Parser::NEWLINE)
      end

      def NEWLINE_i(i)
        return token(Python3Parser::NEWLINE, i)
      end

      def stmt()
        return rule_contexts("StmtContext")
      end

      def stmt_i(i)
        return rule_context("StmtContext", i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_file_input
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterFile_input))
          listener.enterFile_input(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitFile_input))
          listener.exitFile_input(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitFile_input))
          return visitor.visitFile_input(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def file_input()
      _localctx = File_inputContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 2, RULE_file_input)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 183
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << DEF) | (1 << RETURN) | (1 << RAISE) | (1 << FROM) | (1 << IMPORT) | (1 << GLOBAL) | (1 << NONLOCAL) | (1 << ASSERT) | (1 << IF) | (1 << WHILE) | (1 << FOR) | (1 << TRY) | (1 << WITH) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << CLASS) | (1 << YIELD) | (1 << DEL) | (1 << PASS) | (1 << CONTINUE) | (1 << BREAK) | (1 << ASYNC) | (1 << AWAIT) | (1 << NEWLINE) | (1 << NAME) | (1 << ELLIPSIS) | (1 << STAR) | (1 << OPEN_PAREN) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)) | (1 << (AT - 66)))) != 0))
          @_state_number = 181
          @_err_handler.sync(self)
          case (@_input.la(1))
          when Python3Parser::NEWLINE
            @_state_number = 179
            match(NEWLINE)
          when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::DEF, Python3Parser::RETURN, Python3Parser::RAISE, Python3Parser::FROM, Python3Parser::IMPORT, Python3Parser::GLOBAL, Python3Parser::NONLOCAL, Python3Parser::ASSERT, Python3Parser::IF, Python3Parser::WHILE, Python3Parser::FOR, Python3Parser::TRY, Python3Parser::WITH, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::CLASS, Python3Parser::YIELD, Python3Parser::DEL, Python3Parser::PASS, Python3Parser::CONTINUE, Python3Parser::BREAK, Python3Parser::ASYNC, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::STAR, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE, Python3Parser::AT
            @_state_number = 180
            stmt()
          else
            raise Antlr4::Runtime::NoViableAltException, self
          end
          @_state_number = 185
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
        @_state_number = 186
        match(EOF)
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Eval_inputContext < Antlr4::Runtime::ParserRuleContext
      def testlist()
        return rule_context("TestlistContext", 0)
      end

      def EOF()
        return token(Python3Parser::EOF, 0)
      end

      def NEWLINE()
        return tokens(Python3Parser::NEWLINE)
      end

      def NEWLINE_i(i)
        return token(Python3Parser::NEWLINE, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_eval_input
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterEval_input))
          listener.enterEval_input(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitEval_input))
          listener.exitEval_input(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitEval_input))
          return visitor.visitEval_input(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def eval_input()
      _localctx = Eval_inputContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 4, RULE_eval_input)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 188
        testlist()
        @_state_number = 192
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == NEWLINE)
          @_state_number = 189
          match(NEWLINE)
          @_state_number = 194
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
        @_state_number = 195
        match(EOF)
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class DecoratorContext < Antlr4::Runtime::ParserRuleContext
      def AT()
        return token(Python3Parser::AT, 0)
      end

      def dotted_name()
        return rule_context("Dotted_nameContext", 0)
      end

      def NEWLINE()
        return token(Python3Parser::NEWLINE, 0)
      end

      def OPEN_PAREN()
        return token(Python3Parser::OPEN_PAREN, 0)
      end

      def CLOSE_PAREN()
        return token(Python3Parser::CLOSE_PAREN, 0)
      end

      def arglist()
        return rule_context("ArglistContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_decorator
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterDecorator))
          listener.enterDecorator(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitDecorator))
          listener.exitDecorator(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitDecorator))
          return visitor.visitDecorator(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def decorator()
      _localctx = DecoratorContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 6, RULE_decorator)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 197
        match(AT)
        @_state_number = 198
        dotted_name()
        @_state_number = 204
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == OPEN_PAREN)
          @_state_number = 199
          match(OPEN_PAREN)
          @_state_number = 201
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << STAR) | (1 << OPEN_PAREN) | (1 << POWER) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)))) != 0))
            @_state_number = 200
            arglist()
          end

          @_state_number = 203
          match(CLOSE_PAREN)
        end

        @_state_number = 206
        match(NEWLINE)
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class DecoratorsContext < Antlr4::Runtime::ParserRuleContext
      def decorator()
        return rule_contexts("DecoratorContext")
      end

      def decorator_i(i)
        return rule_context("DecoratorContext", i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_decorators
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterDecorators))
          listener.enterDecorators(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitDecorators))
          listener.exitDecorators(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitDecorators))
          return visitor.visitDecorators(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def decorators()
      _localctx = DecoratorsContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 8, RULE_decorators)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 209
        @_err_handler.sync(self)
        _la = @_input.la(1)
        loop do
          @_state_number = 208
          decorator()
          @_state_number = 211
          @_err_handler.sync(self)
          _la = @_input.la(1)
          break if (!(_la == AT))
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class DecoratedContext < Antlr4::Runtime::ParserRuleContext
      def decorators()
        return rule_context("DecoratorsContext", 0)
      end

      def classdef()
        return rule_context("ClassdefContext", 0)
      end

      def funcdef()
        return rule_context("FuncdefContext", 0)
      end

      def async_funcdef()
        return rule_context("Async_funcdefContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_decorated
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterDecorated))
          listener.enterDecorated(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitDecorated))
          listener.exitDecorated(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitDecorated))
          return visitor.visitDecorated(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def decorated()
      _localctx = DecoratedContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 10, RULE_decorated)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 213
        decorators()
        @_state_number = 217
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::CLASS
          @_state_number = 214
          classdef()
        when Python3Parser::DEF
          @_state_number = 215
          funcdef()
        when Python3Parser::ASYNC
          @_state_number = 216
          async_funcdef()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Async_funcdefContext < Antlr4::Runtime::ParserRuleContext
      def ASYNC()
        return token(Python3Parser::ASYNC, 0)
      end

      def funcdef()
        return rule_context("FuncdefContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_async_funcdef
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterAsync_funcdef))
          listener.enterAsync_funcdef(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitAsync_funcdef))
          listener.exitAsync_funcdef(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitAsync_funcdef))
          return visitor.visitAsync_funcdef(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def async_funcdef()
      _localctx = Async_funcdefContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 12, RULE_async_funcdef)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 219
        match(ASYNC)
        @_state_number = 220
        funcdef()
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class FuncdefContext < Antlr4::Runtime::ParserRuleContext
      def DEF()
        return token(Python3Parser::DEF, 0)
      end

      def NAME()
        return token(Python3Parser::NAME, 0)
      end

      def parameters()
        return rule_context("ParametersContext", 0)
      end

      def COLON()
        return token(Python3Parser::COLON, 0)
      end

      def suite()
        return rule_context("SuiteContext", 0)
      end

      def ARROW()
        return token(Python3Parser::ARROW, 0)
      end

      def test()
        return rule_context("TestContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_funcdef
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterFuncdef))
          listener.enterFuncdef(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitFuncdef))
          listener.exitFuncdef(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitFuncdef))
          return visitor.visitFuncdef(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def funcdef()
      _localctx = FuncdefContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 14, RULE_funcdef)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 222
        match(DEF)
        @_state_number = 223
        match(NAME)
        @_state_number = 224
        parameters()
        @_state_number = 227
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == ARROW)
          @_state_number = 225
          match(ARROW)
          @_state_number = 226
          test()
        end

        @_state_number = 229
        match(COLON)
        @_state_number = 230
        suite()
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class ParametersContext < Antlr4::Runtime::ParserRuleContext
      def OPEN_PAREN()
        return token(Python3Parser::OPEN_PAREN, 0)
      end

      def CLOSE_PAREN()
        return token(Python3Parser::CLOSE_PAREN, 0)
      end

      def typedargslist()
        return rule_context("TypedargslistContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_parameters
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterParameters))
          listener.enterParameters(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitParameters))
          listener.exitParameters(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitParameters))
          return visitor.visitParameters(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def parameters()
      _localctx = ParametersContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 16, RULE_parameters)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 232
        match(OPEN_PAREN)
        @_state_number = 234
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << NAME) | (1 << STAR) | (1 << POWER))) != 0))
          @_state_number = 233
          typedargslist()
        end

        @_state_number = 236
        match(CLOSE_PAREN)
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class TypedargslistContext < Antlr4::Runtime::ParserRuleContext
      def tfpdef()
        return rule_contexts("TfpdefContext")
      end

      def tfpdef_i(i)
        return rule_context("TfpdefContext", i)
      end

      def STAR()
        return token(Python3Parser::STAR, 0)
      end

      def POWER()
        return token(Python3Parser::POWER, 0)
      end

      def ASSIGN()
        return tokens(Python3Parser::ASSIGN)
      end

      def ASSIGN_i(i)
        return token(Python3Parser::ASSIGN, i)
      end

      def test()
        return rule_contexts("TestContext")
      end

      def test_i(i)
        return rule_context("TestContext", i)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_typedargslist
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterTypedargslist))
          listener.enterTypedargslist(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitTypedargslist))
          listener.exitTypedargslist(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitTypedargslist))
          return visitor.visitTypedargslist(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def typedargslist()
      _localctx = TypedargslistContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 18, RULE_typedargslist)
      _la = 0
      begin
        _alt = 0
        enter_outer_alt(_localctx, 1)

        @_state_number = 319
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::NAME
          @_state_number = 238
          tfpdef()
          @_state_number = 241
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == ASSIGN)
            @_state_number = 239
            match(ASSIGN)
            @_state_number = 240
            test()
          end

          @_state_number = 251
          @_err_handler.sync(self)
          _alt = @_interp.adaptive_predict(@_input, 12, @_ctx)
          while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
            if (_alt == 1)
              @_state_number = 243
              match(COMMA)
              @_state_number = 244
              tfpdef()
              @_state_number = 247
              @_err_handler.sync(self)
              _la = @_input.la(1)
              if (_la == ASSIGN)
                @_state_number = 245
                match(ASSIGN)
                @_state_number = 246
                test()
              end
            end
            @_state_number = 253
            @_err_handler.sync(self)
            _alt = @_interp.adaptive_predict(@_input, 12, @_ctx)
          end
          @_state_number = 287
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == COMMA)
            @_state_number = 254
            match(COMMA)
            @_state_number = 285
            @_err_handler.sync(self)
            case (@_input.la(1))
            when STAR
              @_state_number = 255
              match(STAR)
              @_state_number = 257
              @_err_handler.sync(self)
              _la = @_input.la(1)
              if (_la == NAME)
                @_state_number = 256
                tfpdef()
              end

              @_state_number = 267
              @_err_handler.sync(self)
              _alt = @_interp.adaptive_predict(@_input, 15, @_ctx)
              while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
                if (_alt == 1)
                  @_state_number = 259
                  match(COMMA)
                  @_state_number = 260
                  tfpdef()
                  @_state_number = 263
                  @_err_handler.sync(self)
                  _la = @_input.la(1)
                  if (_la == ASSIGN)
                    @_state_number = 261
                    match(ASSIGN)
                    @_state_number = 262
                    test()
                  end
                end
                @_state_number = 269
                @_err_handler.sync(self)
                _alt = @_interp.adaptive_predict(@_input, 15, @_ctx)
              end
              @_state_number = 278
              @_err_handler.sync(self)
              _la = @_input.la(1)
              if (_la == COMMA)
                @_state_number = 270
                match(COMMA)
                @_state_number = 276
                @_err_handler.sync(self)
                _la = @_input.la(1)
                if (_la == POWER)
                  @_state_number = 271
                  match(POWER)
                  @_state_number = 272
                  tfpdef()
                  @_state_number = 274
                  @_err_handler.sync(self)
                  _la = @_input.la(1)
                  if (_la == COMMA)
                    @_state_number = 273
                    match(COMMA)
                  end
                end
              end
            when POWER
              @_state_number = 280
              match(POWER)
              @_state_number = 281
              tfpdef()
              @_state_number = 283
              @_err_handler.sync(self)
              _la = @_input.la(1)
              if (_la == COMMA)
                @_state_number = 282
                match(COMMA)
              end
            when CLOSE_PAREN
            else
            end
          end
        when Python3Parser::STAR
          @_state_number = 289
          match(STAR)
          @_state_number = 291
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == NAME)
            @_state_number = 290
            tfpdef()
          end

          @_state_number = 301
          @_err_handler.sync(self)
          _alt = @_interp.adaptive_predict(@_input, 24, @_ctx)
          while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
            if (_alt == 1)
              @_state_number = 293
              match(COMMA)
              @_state_number = 294
              tfpdef()
              @_state_number = 297
              @_err_handler.sync(self)
              _la = @_input.la(1)
              if (_la == ASSIGN)
                @_state_number = 295
                match(ASSIGN)
                @_state_number = 296
                test()
              end
            end
            @_state_number = 303
            @_err_handler.sync(self)
            _alt = @_interp.adaptive_predict(@_input, 24, @_ctx)
          end
          @_state_number = 312
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == COMMA)
            @_state_number = 304
            match(COMMA)
            @_state_number = 310
            @_err_handler.sync(self)
            _la = @_input.la(1)
            if (_la == POWER)
              @_state_number = 305
              match(POWER)
              @_state_number = 306
              tfpdef()
              @_state_number = 308
              @_err_handler.sync(self)
              _la = @_input.la(1)
              if (_la == COMMA)
                @_state_number = 307
                match(COMMA)
              end
            end
          end
        when Python3Parser::POWER
          @_state_number = 314
          match(POWER)
          @_state_number = 315
          tfpdef()
          @_state_number = 317
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == COMMA)
            @_state_number = 316
            match(COMMA)
          end
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class TfpdefContext < Antlr4::Runtime::ParserRuleContext
      def NAME()
        return token(Python3Parser::NAME, 0)
      end

      def COLON()
        return token(Python3Parser::COLON, 0)
      end

      def test()
        return rule_context("TestContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_tfpdef
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterTfpdef))
          listener.enterTfpdef(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitTfpdef))
          listener.exitTfpdef(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitTfpdef))
          return visitor.visitTfpdef(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def tfpdef()
      _localctx = TfpdefContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 20, RULE_tfpdef)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 321
        match(NAME)
        @_state_number = 324
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == COLON)
          @_state_number = 322
          match(COLON)
          @_state_number = 323
          test()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class VarargslistContext < Antlr4::Runtime::ParserRuleContext
      def vfpdef()
        return rule_contexts("VfpdefContext")
      end

      def vfpdef_i(i)
        return rule_context("VfpdefContext", i)
      end

      def STAR()
        return token(Python3Parser::STAR, 0)
      end

      def POWER()
        return token(Python3Parser::POWER, 0)
      end

      def ASSIGN()
        return tokens(Python3Parser::ASSIGN)
      end

      def ASSIGN_i(i)
        return token(Python3Parser::ASSIGN, i)
      end

      def test()
        return rule_contexts("TestContext")
      end

      def test_i(i)
        return rule_context("TestContext", i)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_varargslist
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterVarargslist))
          listener.enterVarargslist(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitVarargslist))
          listener.exitVarargslist(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitVarargslist))
          return visitor.visitVarargslist(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def varargslist()
      _localctx = VarargslistContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 22, RULE_varargslist)
      _la = 0
      begin
        _alt = 0
        enter_outer_alt(_localctx, 1)

        @_state_number = 407
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::NAME
          @_state_number = 326
          vfpdef()
          @_state_number = 329
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == ASSIGN)
            @_state_number = 327
            match(ASSIGN)
            @_state_number = 328
            test()
          end

          @_state_number = 339
          @_err_handler.sync(self)
          _alt = @_interp.adaptive_predict(@_input, 33, @_ctx)
          while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
            if (_alt == 1)
              @_state_number = 331
              match(COMMA)
              @_state_number = 332
              vfpdef()
              @_state_number = 335
              @_err_handler.sync(self)
              _la = @_input.la(1)
              if (_la == ASSIGN)
                @_state_number = 333
                match(ASSIGN)
                @_state_number = 334
                test()
              end
            end
            @_state_number = 341
            @_err_handler.sync(self)
            _alt = @_interp.adaptive_predict(@_input, 33, @_ctx)
          end
          @_state_number = 375
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == COMMA)
            @_state_number = 342
            match(COMMA)
            @_state_number = 373
            @_err_handler.sync(self)
            case (@_input.la(1))
            when STAR
              @_state_number = 343
              match(STAR)
              @_state_number = 345
              @_err_handler.sync(self)
              _la = @_input.la(1)
              if (_la == NAME)
                @_state_number = 344
                vfpdef()
              end

              @_state_number = 355
              @_err_handler.sync(self)
              _alt = @_interp.adaptive_predict(@_input, 36, @_ctx)
              while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
                if (_alt == 1)
                  @_state_number = 347
                  match(COMMA)
                  @_state_number = 348
                  vfpdef()
                  @_state_number = 351
                  @_err_handler.sync(self)
                  _la = @_input.la(1)
                  if (_la == ASSIGN)
                    @_state_number = 349
                    match(ASSIGN)
                    @_state_number = 350
                    test()
                  end
                end
                @_state_number = 357
                @_err_handler.sync(self)
                _alt = @_interp.adaptive_predict(@_input, 36, @_ctx)
              end
              @_state_number = 366
              @_err_handler.sync(self)
              _la = @_input.la(1)
              if (_la == COMMA)
                @_state_number = 358
                match(COMMA)
                @_state_number = 364
                @_err_handler.sync(self)
                _la = @_input.la(1)
                if (_la == POWER)
                  @_state_number = 359
                  match(POWER)
                  @_state_number = 360
                  vfpdef()
                  @_state_number = 362
                  @_err_handler.sync(self)
                  _la = @_input.la(1)
                  if (_la == COMMA)
                    @_state_number = 361
                    match(COMMA)
                  end
                end
              end
            when POWER
              @_state_number = 368
              match(POWER)
              @_state_number = 369
              vfpdef()
              @_state_number = 371
              @_err_handler.sync(self)
              _la = @_input.la(1)
              if (_la == COMMA)
                @_state_number = 370
                match(COMMA)
              end
            when COLON
            else
            end
          end
        when Python3Parser::STAR
          @_state_number = 377
          match(STAR)
          @_state_number = 379
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == NAME)
            @_state_number = 378
            vfpdef()
          end

          @_state_number = 389
          @_err_handler.sync(self)
          _alt = @_interp.adaptive_predict(@_input, 45, @_ctx)
          while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
            if (_alt == 1)
              @_state_number = 381
              match(COMMA)
              @_state_number = 382
              vfpdef()
              @_state_number = 385
              @_err_handler.sync(self)
              _la = @_input.la(1)
              if (_la == ASSIGN)
                @_state_number = 383
                match(ASSIGN)
                @_state_number = 384
                test()
              end
            end
            @_state_number = 391
            @_err_handler.sync(self)
            _alt = @_interp.adaptive_predict(@_input, 45, @_ctx)
          end
          @_state_number = 400
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == COMMA)
            @_state_number = 392
            match(COMMA)
            @_state_number = 398
            @_err_handler.sync(self)
            _la = @_input.la(1)
            if (_la == POWER)
              @_state_number = 393
              match(POWER)
              @_state_number = 394
              vfpdef()
              @_state_number = 396
              @_err_handler.sync(self)
              _la = @_input.la(1)
              if (_la == COMMA)
                @_state_number = 395
                match(COMMA)
              end
            end
          end
        when Python3Parser::POWER
          @_state_number = 402
          match(POWER)
          @_state_number = 403
          vfpdef()
          @_state_number = 405
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == COMMA)
            @_state_number = 404
            match(COMMA)
          end
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class VfpdefContext < Antlr4::Runtime::ParserRuleContext
      def NAME()
        return token(Python3Parser::NAME, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_vfpdef
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterVfpdef))
          listener.enterVfpdef(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitVfpdef))
          listener.exitVfpdef(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitVfpdef))
          return visitor.visitVfpdef(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def vfpdef()
      _localctx = VfpdefContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 24, RULE_vfpdef)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 409
        match(NAME)
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class StmtContext < Antlr4::Runtime::ParserRuleContext
      def simple_stmt()
        return rule_context("Simple_stmtContext", 0)
      end

      def compound_stmt()
        return rule_context("Compound_stmtContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterStmt))
          listener.enterStmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitStmt))
          listener.exitStmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitStmt))
          return visitor.visitStmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def stmt()
      _localctx = StmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 26, RULE_stmt)
      begin
        @_state_number = 413
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::RETURN, Python3Parser::RAISE, Python3Parser::FROM, Python3Parser::IMPORT, Python3Parser::GLOBAL, Python3Parser::NONLOCAL, Python3Parser::ASSERT, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::YIELD, Python3Parser::DEL, Python3Parser::PASS, Python3Parser::CONTINUE, Python3Parser::BREAK, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::STAR, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
          enter_outer_alt(_localctx, 1)

          @_state_number = 411
          simple_stmt()
        when Python3Parser::DEF, Python3Parser::IF, Python3Parser::WHILE, Python3Parser::FOR, Python3Parser::TRY, Python3Parser::WITH, Python3Parser::CLASS, Python3Parser::ASYNC, Python3Parser::AT
          enter_outer_alt(_localctx, 2)

          @_state_number = 412
          compound_stmt()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Simple_stmtContext < Antlr4::Runtime::ParserRuleContext
      def small_stmt()
        return rule_contexts("Small_stmtContext")
      end

      def small_stmt_i(i)
        return rule_context("Small_stmtContext", i)
      end

      def NEWLINE()
        return token(Python3Parser::NEWLINE, 0)
      end

      def SEMI_COLON()
        return tokens(Python3Parser::SEMI_COLON)
      end

      def SEMI_COLON_i(i)
        return token(Python3Parser::SEMI_COLON, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_simple_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterSimple_stmt))
          listener.enterSimple_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitSimple_stmt))
          listener.exitSimple_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitSimple_stmt))
          return visitor.visitSimple_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def simple_stmt()
      _localctx = Simple_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 28, RULE_simple_stmt)
      _la = 0
      begin
        _alt = 0
        enter_outer_alt(_localctx, 1)

        @_state_number = 415
        small_stmt()
        @_state_number = 420
        @_err_handler.sync(self)
        _alt = @_interp.adaptive_predict(@_input, 52, @_ctx)
        while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
          if (_alt == 1)
            @_state_number = 416
            match(SEMI_COLON)
            @_state_number = 417
            small_stmt()
          end
          @_state_number = 422
          @_err_handler.sync(self)
          _alt = @_interp.adaptive_predict(@_input, 52, @_ctx)
        end
        @_state_number = 424
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == SEMI_COLON)
          @_state_number = 423
          match(SEMI_COLON)
        end

        @_state_number = 426
        match(NEWLINE)
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Small_stmtContext < Antlr4::Runtime::ParserRuleContext
      def expr_stmt()
        return rule_context("Expr_stmtContext", 0)
      end

      def del_stmt()
        return rule_context("Del_stmtContext", 0)
      end

      def pass_stmt()
        return rule_context("Pass_stmtContext", 0)
      end

      def flow_stmt()
        return rule_context("Flow_stmtContext", 0)
      end

      def import_stmt()
        return rule_context("Import_stmtContext", 0)
      end

      def global_stmt()
        return rule_context("Global_stmtContext", 0)
      end

      def nonlocal_stmt()
        return rule_context("Nonlocal_stmtContext", 0)
      end

      def assert_stmt()
        return rule_context("Assert_stmtContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_small_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterSmall_stmt))
          listener.enterSmall_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitSmall_stmt))
          listener.exitSmall_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitSmall_stmt))
          return visitor.visitSmall_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def small_stmt()
      _localctx = Small_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 30, RULE_small_stmt)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 436
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::STAR, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
          @_state_number = 428
          expr_stmt()
        when Python3Parser::DEL
          @_state_number = 429
          del_stmt()
        when Python3Parser::PASS
          @_state_number = 430
          pass_stmt()
        when Python3Parser::RETURN, Python3Parser::RAISE, Python3Parser::YIELD, Python3Parser::CONTINUE, Python3Parser::BREAK
          @_state_number = 431
          flow_stmt()
        when Python3Parser::FROM, Python3Parser::IMPORT
          @_state_number = 432
          import_stmt()
        when Python3Parser::GLOBAL
          @_state_number = 433
          global_stmt()
        when Python3Parser::NONLOCAL
          @_state_number = 434
          nonlocal_stmt()
        when Python3Parser::ASSERT
          @_state_number = 435
          assert_stmt()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Expr_stmtContext < Antlr4::Runtime::ParserRuleContext
      def testlist_star_expr()
        return rule_contexts("Testlist_star_exprContext")
      end

      def testlist_star_expr_i(i)
        return rule_context("Testlist_star_exprContext", i)
      end

      def annassign()
        return rule_context("AnnassignContext", 0)
      end

      def augassign()
        return rule_context("AugassignContext", 0)
      end

      def yield_expr()
        return rule_contexts("Yield_exprContext")
      end

      def yield_expr_i(i)
        return rule_context("Yield_exprContext", i)
      end

      def testlist()
        return rule_context("TestlistContext", 0)
      end

      def ASSIGN()
        return tokens(Python3Parser::ASSIGN)
      end

      def ASSIGN_i(i)
        return token(Python3Parser::ASSIGN, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_expr_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterExpr_stmt))
          listener.enterExpr_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitExpr_stmt))
          listener.exitExpr_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitExpr_stmt))
          return visitor.visitExpr_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def expr_stmt()
      _localctx = Expr_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 32, RULE_expr_stmt)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 438
        testlist_star_expr()
        @_state_number = 455
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::COLON
          @_state_number = 439
          annassign()
        when Python3Parser::ADD_ASSIGN, Python3Parser::SUB_ASSIGN, Python3Parser::MULT_ASSIGN, Python3Parser::AT_ASSIGN, Python3Parser::DIV_ASSIGN, Python3Parser::MOD_ASSIGN, Python3Parser::AND_ASSIGN, Python3Parser::OR_ASSIGN, Python3Parser::XOR_ASSIGN, Python3Parser::LEFT_SHIFT_ASSIGN, Python3Parser::RIGHT_SHIFT_ASSIGN, Python3Parser::POWER_ASSIGN, Python3Parser::IDIV_ASSIGN
          @_state_number = 440
          augassign()
          @_state_number = 443
          @_err_handler.sync(self)
          case (@_input.la(1))
          when Python3Parser::YIELD
            @_state_number = 441
            yield_expr()
          when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
            @_state_number = 442
            testlist()
          else
            raise Antlr4::Runtime::NoViableAltException, self
          end
        when Python3Parser::NEWLINE, Python3Parser::SEMI_COLON, Python3Parser::ASSIGN
          @_state_number = 452
          @_err_handler.sync(self)
          _la = @_input.la(1)
          while (_la == ASSIGN)
            @_state_number = 445
            match(ASSIGN)
            @_state_number = 448
            @_err_handler.sync(self)
            case (@_input.la(1))
            when Python3Parser::YIELD
              @_state_number = 446
              yield_expr()
            when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::STAR, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
              @_state_number = 447
              testlist_star_expr()
            else
              raise Antlr4::Runtime::NoViableAltException, self
            end
            @_state_number = 454
            @_err_handler.sync(self)
            _la = @_input.la(1)
          end
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class AnnassignContext < Antlr4::Runtime::ParserRuleContext
      def COLON()
        return token(Python3Parser::COLON, 0)
      end

      def test()
        return rule_contexts("TestContext")
      end

      def test_i(i)
        return rule_context("TestContext", i)
      end

      def ASSIGN()
        return token(Python3Parser::ASSIGN, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_annassign
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterAnnassign))
          listener.enterAnnassign(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitAnnassign))
          listener.exitAnnassign(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitAnnassign))
          return visitor.visitAnnassign(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def annassign()
      _localctx = AnnassignContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 34, RULE_annassign)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 457
        match(COLON)
        @_state_number = 458
        test()
        @_state_number = 461
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == ASSIGN)
          @_state_number = 459
          match(ASSIGN)
          @_state_number = 460
          test()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Testlist_star_exprContext < Antlr4::Runtime::ParserRuleContext
      def test()
        return rule_contexts("TestContext")
      end

      def test_i(i)
        return rule_context("TestContext", i)
      end

      def star_expr()
        return rule_contexts("Star_exprContext")
      end

      def star_expr_i(i)
        return rule_context("Star_exprContext", i)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_testlist_star_expr
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterTestlist_star_expr))
          listener.enterTestlist_star_expr(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitTestlist_star_expr))
          listener.exitTestlist_star_expr(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitTestlist_star_expr))
          return visitor.visitTestlist_star_expr(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def testlist_star_expr()
      _localctx = Testlist_star_exprContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 36, RULE_testlist_star_expr)
      _la = 0
      begin
        _alt = 0
        enter_outer_alt(_localctx, 1)

        @_state_number = 465
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
          @_state_number = 463
          test()
        when Python3Parser::STAR
          @_state_number = 464
          star_expr()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
        @_state_number = 474
        @_err_handler.sync(self)
        _alt = @_interp.adaptive_predict(@_input, 62, @_ctx)
        while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
          if (_alt == 1)
            @_state_number = 467
            match(COMMA)
            @_state_number = 470
            @_err_handler.sync(self)
            case (@_input.la(1))
            when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
              @_state_number = 468
              test()
            when Python3Parser::STAR
              @_state_number = 469
              star_expr()
            else
              raise Antlr4::Runtime::NoViableAltException, self
            end
          end
          @_state_number = 476
          @_err_handler.sync(self)
          _alt = @_interp.adaptive_predict(@_input, 62, @_ctx)
        end
        @_state_number = 478
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == COMMA)
          @_state_number = 477
          match(COMMA)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class AugassignContext < Antlr4::Runtime::ParserRuleContext
      def ADD_ASSIGN()
        return token(Python3Parser::ADD_ASSIGN, 0)
      end

      def SUB_ASSIGN()
        return token(Python3Parser::SUB_ASSIGN, 0)
      end

      def MULT_ASSIGN()
        return token(Python3Parser::MULT_ASSIGN, 0)
      end

      def AT_ASSIGN()
        return token(Python3Parser::AT_ASSIGN, 0)
      end

      def DIV_ASSIGN()
        return token(Python3Parser::DIV_ASSIGN, 0)
      end

      def MOD_ASSIGN()
        return token(Python3Parser::MOD_ASSIGN, 0)
      end

      def AND_ASSIGN()
        return token(Python3Parser::AND_ASSIGN, 0)
      end

      def OR_ASSIGN()
        return token(Python3Parser::OR_ASSIGN, 0)
      end

      def XOR_ASSIGN()
        return token(Python3Parser::XOR_ASSIGN, 0)
      end

      def LEFT_SHIFT_ASSIGN()
        return token(Python3Parser::LEFT_SHIFT_ASSIGN, 0)
      end

      def RIGHT_SHIFT_ASSIGN()
        return token(Python3Parser::RIGHT_SHIFT_ASSIGN, 0)
      end

      def POWER_ASSIGN()
        return token(Python3Parser::POWER_ASSIGN, 0)
      end

      def IDIV_ASSIGN()
        return token(Python3Parser::IDIV_ASSIGN, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_augassign
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterAugassign))
          listener.enterAugassign(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitAugassign))
          listener.exitAugassign(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitAugassign))
          return visitor.visitAugassign(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def augassign()
      _localctx = AugassignContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 38, RULE_augassign)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 480
        _la = @_input.la(1)
        if (!(((((_la - 83)) & ~0x3f) == 0 && ((1 << (_la - 83)) & ((1 << (ADD_ASSIGN - 83)) | (1 << (SUB_ASSIGN - 83)) | (1 << (MULT_ASSIGN - 83)) | (1 << (AT_ASSIGN - 83)) | (1 << (DIV_ASSIGN - 83)) | (1 << (MOD_ASSIGN - 83)) | (1 << (AND_ASSIGN - 83)) | (1 << (OR_ASSIGN - 83)) | (1 << (XOR_ASSIGN - 83)) | (1 << (LEFT_SHIFT_ASSIGN - 83)) | (1 << (RIGHT_SHIFT_ASSIGN - 83)) | (1 << (POWER_ASSIGN - 83)) | (1 << (IDIV_ASSIGN - 83)))) != 0)))
          @_err_handler.recover_in_line(self)
        else
          if (@_input.la(1) == Antlr4::Runtime::Token::EOF)
            @matchedEOF = true
          end
          @_err_handler.report_match(self)
          consume()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Del_stmtContext < Antlr4::Runtime::ParserRuleContext
      def DEL()
        return token(Python3Parser::DEL, 0)
      end

      def exprlist()
        return rule_context("ExprlistContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_del_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterDel_stmt))
          listener.enterDel_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitDel_stmt))
          listener.exitDel_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitDel_stmt))
          return visitor.visitDel_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def del_stmt()
      _localctx = Del_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 40, RULE_del_stmt)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 482
        match(DEL)
        @_state_number = 483
        exprlist()
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Pass_stmtContext < Antlr4::Runtime::ParserRuleContext
      def PASS()
        return token(Python3Parser::PASS, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_pass_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterPass_stmt))
          listener.enterPass_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitPass_stmt))
          listener.exitPass_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitPass_stmt))
          return visitor.visitPass_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def pass_stmt()
      _localctx = Pass_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 42, RULE_pass_stmt)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 485
        match(PASS)
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Flow_stmtContext < Antlr4::Runtime::ParserRuleContext
      def break_stmt()
        return rule_context("Break_stmtContext", 0)
      end

      def continue_stmt()
        return rule_context("Continue_stmtContext", 0)
      end

      def return_stmt()
        return rule_context("Return_stmtContext", 0)
      end

      def raise_stmt()
        return rule_context("Raise_stmtContext", 0)
      end

      def yield_stmt()
        return rule_context("Yield_stmtContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_flow_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterFlow_stmt))
          listener.enterFlow_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitFlow_stmt))
          listener.exitFlow_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitFlow_stmt))
          return visitor.visitFlow_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def flow_stmt()
      _localctx = Flow_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 44, RULE_flow_stmt)
      begin
        @_state_number = 492
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::BREAK
          enter_outer_alt(_localctx, 1)

          @_state_number = 487
          break_stmt()
        when Python3Parser::CONTINUE
          enter_outer_alt(_localctx, 2)

          @_state_number = 488
          continue_stmt()
        when Python3Parser::RETURN
          enter_outer_alt(_localctx, 3)

          @_state_number = 489
          return_stmt()
        when Python3Parser::RAISE
          enter_outer_alt(_localctx, 4)

          @_state_number = 490
          raise_stmt()
        when Python3Parser::YIELD
          enter_outer_alt(_localctx, 5)

          @_state_number = 491
          yield_stmt()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Break_stmtContext < Antlr4::Runtime::ParserRuleContext
      def BREAK()
        return token(Python3Parser::BREAK, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_break_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterBreak_stmt))
          listener.enterBreak_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitBreak_stmt))
          listener.exitBreak_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitBreak_stmt))
          return visitor.visitBreak_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def break_stmt()
      _localctx = Break_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 46, RULE_break_stmt)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 494
        match(BREAK)
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Continue_stmtContext < Antlr4::Runtime::ParserRuleContext
      def CONTINUE()
        return token(Python3Parser::CONTINUE, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_continue_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterContinue_stmt))
          listener.enterContinue_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitContinue_stmt))
          listener.exitContinue_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitContinue_stmt))
          return visitor.visitContinue_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def continue_stmt()
      _localctx = Continue_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 48, RULE_continue_stmt)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 496
        match(CONTINUE)
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Return_stmtContext < Antlr4::Runtime::ParserRuleContext
      def RETURN()
        return token(Python3Parser::RETURN, 0)
      end

      def testlist()
        return rule_context("TestlistContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_return_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterReturn_stmt))
          listener.enterReturn_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitReturn_stmt))
          listener.exitReturn_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitReturn_stmt))
          return visitor.visitReturn_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def return_stmt()
      _localctx = Return_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 50, RULE_return_stmt)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 498
        match(RETURN)
        @_state_number = 500
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << OPEN_PAREN) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)))) != 0))
          @_state_number = 499
          testlist()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Yield_stmtContext < Antlr4::Runtime::ParserRuleContext
      def yield_expr()
        return rule_context("Yield_exprContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_yield_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterYield_stmt))
          listener.enterYield_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitYield_stmt))
          listener.exitYield_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitYield_stmt))
          return visitor.visitYield_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def yield_stmt()
      _localctx = Yield_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 52, RULE_yield_stmt)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 502
        yield_expr()
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Raise_stmtContext < Antlr4::Runtime::ParserRuleContext
      def RAISE()
        return token(Python3Parser::RAISE, 0)
      end

      def test()
        return rule_contexts("TestContext")
      end

      def test_i(i)
        return rule_context("TestContext", i)
      end

      def FROM()
        return token(Python3Parser::FROM, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_raise_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterRaise_stmt))
          listener.enterRaise_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitRaise_stmt))
          listener.exitRaise_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitRaise_stmt))
          return visitor.visitRaise_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def raise_stmt()
      _localctx = Raise_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 54, RULE_raise_stmt)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 504
        match(RAISE)
        @_state_number = 510
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << OPEN_PAREN) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)))) != 0))
          @_state_number = 505
          test()
          @_state_number = 508
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == FROM)
            @_state_number = 506
            match(FROM)
            @_state_number = 507
            test()
          end
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Import_stmtContext < Antlr4::Runtime::ParserRuleContext
      def import_name()
        return rule_context("Import_nameContext", 0)
      end

      def import_from()
        return rule_context("Import_fromContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_import_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterImport_stmt))
          listener.enterImport_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitImport_stmt))
          listener.exitImport_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitImport_stmt))
          return visitor.visitImport_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def import_stmt()
      _localctx = Import_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 56, RULE_import_stmt)
      begin
        @_state_number = 514
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::IMPORT
          enter_outer_alt(_localctx, 1)

          @_state_number = 512
          import_name()
        when Python3Parser::FROM
          enter_outer_alt(_localctx, 2)

          @_state_number = 513
          import_from()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Import_nameContext < Antlr4::Runtime::ParserRuleContext
      def IMPORT()
        return token(Python3Parser::IMPORT, 0)
      end

      def dotted_as_names()
        return rule_context("Dotted_as_namesContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_import_name
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterImport_name))
          listener.enterImport_name(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitImport_name))
          listener.exitImport_name(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitImport_name))
          return visitor.visitImport_name(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def import_name()
      _localctx = Import_nameContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 58, RULE_import_name)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 516
        match(IMPORT)
        @_state_number = 517
        dotted_as_names()
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Import_fromContext < Antlr4::Runtime::ParserRuleContext
      def FROM()
        return token(Python3Parser::FROM, 0)
      end

      def IMPORT()
        return token(Python3Parser::IMPORT, 0)
      end

      def dotted_name()
        return rule_context("Dotted_nameContext", 0)
      end

      def STAR()
        return token(Python3Parser::STAR, 0)
      end

      def OPEN_PAREN()
        return token(Python3Parser::OPEN_PAREN, 0)
      end

      def import_as_names()
        return rule_context("Import_as_namesContext", 0)
      end

      def CLOSE_PAREN()
        return token(Python3Parser::CLOSE_PAREN, 0)
      end

      def DOT()
        return tokens(Python3Parser::DOT)
      end

      def DOT_i(i)
        return token(Python3Parser::DOT, i)
      end

      def ELLIPSIS()
        return tokens(Python3Parser::ELLIPSIS)
      end

      def ELLIPSIS_i(i)
        return token(Python3Parser::ELLIPSIS, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_import_from
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterImport_from))
          listener.enterImport_from(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitImport_from))
          listener.exitImport_from(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitImport_from))
          return visitor.visitImport_from(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def import_from()
      _localctx = Import_fromContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 60, RULE_import_from)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 519
        match(FROM)
        @_state_number = 532
        @_err_handler.sync(self)
        case (@_interp.adaptive_predict(@_input, 71, @_ctx))
        when 1
          @_state_number = 523
          @_err_handler.sync(self)
          _la = @_input.la(1)
          while (_la == DOT || _la == ELLIPSIS)
            @_state_number = 520
            _la = @_input.la(1)
            if (!(_la == DOT || _la == ELLIPSIS))
              @_err_handler.recover_in_line(self)
            else
              if (@_input.la(1) == Antlr4::Runtime::Token::EOF)
                @matchedEOF = true
              end
              @_err_handler.report_match(self)
              consume()
            end
            @_state_number = 525
            @_err_handler.sync(self)
            _la = @_input.la(1)
          end
          @_state_number = 526
          dotted_name()
        when 2
          @_state_number = 528
          @_err_handler.sync(self)
          _la = @_input.la(1)
          loop do
            @_state_number = 527
            _la = @_input.la(1)
            if (!(_la == DOT || _la == ELLIPSIS))
              @_err_handler.recover_in_line(self)
            else
              if (@_input.la(1) == Antlr4::Runtime::Token::EOF)
                @matchedEOF = true
              end
              @_err_handler.report_match(self)
              consume()
            end
            @_state_number = 530
            @_err_handler.sync(self)
            _la = @_input.la(1)
            break if (!(_la == DOT || _la == ELLIPSIS))
          end
        end
        @_state_number = 534
        match(IMPORT)
        @_state_number = 541
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::STAR
          @_state_number = 535
          match(STAR)
        when Python3Parser::OPEN_PAREN
          @_state_number = 536
          match(OPEN_PAREN)
          @_state_number = 537
          import_as_names()
          @_state_number = 538
          match(CLOSE_PAREN)
        when Python3Parser::NAME
          @_state_number = 540
          import_as_names()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Import_as_nameContext < Antlr4::Runtime::ParserRuleContext
      def NAME()
        return tokens(Python3Parser::NAME)
      end

      def NAME_i(i)
        return token(Python3Parser::NAME, i)
      end

      def AS()
        return token(Python3Parser::AS, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_import_as_name
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterImport_as_name))
          listener.enterImport_as_name(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitImport_as_name))
          listener.exitImport_as_name(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitImport_as_name))
          return visitor.visitImport_as_name(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def import_as_name()
      _localctx = Import_as_nameContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 62, RULE_import_as_name)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 543
        match(NAME)
        @_state_number = 546
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == AS)
          @_state_number = 544
          match(AS)
          @_state_number = 545
          match(NAME)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Dotted_as_nameContext < Antlr4::Runtime::ParserRuleContext
      def dotted_name()
        return rule_context("Dotted_nameContext", 0)
      end

      def AS()
        return token(Python3Parser::AS, 0)
      end

      def NAME()
        return token(Python3Parser::NAME, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_dotted_as_name
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterDotted_as_name))
          listener.enterDotted_as_name(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitDotted_as_name))
          listener.exitDotted_as_name(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitDotted_as_name))
          return visitor.visitDotted_as_name(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def dotted_as_name()
      _localctx = Dotted_as_nameContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 64, RULE_dotted_as_name)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 548
        dotted_name()
        @_state_number = 551
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == AS)
          @_state_number = 549
          match(AS)
          @_state_number = 550
          match(NAME)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Import_as_namesContext < Antlr4::Runtime::ParserRuleContext
      def import_as_name()
        return rule_contexts("Import_as_nameContext")
      end

      def import_as_name_i(i)
        return rule_context("Import_as_nameContext", i)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_import_as_names
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterImport_as_names))
          listener.enterImport_as_names(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitImport_as_names))
          listener.exitImport_as_names(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitImport_as_names))
          return visitor.visitImport_as_names(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def import_as_names()
      _localctx = Import_as_namesContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 66, RULE_import_as_names)
      _la = 0
      begin
        _alt = 0
        enter_outer_alt(_localctx, 1)

        @_state_number = 553
        import_as_name()
        @_state_number = 558
        @_err_handler.sync(self)
        _alt = @_interp.adaptive_predict(@_input, 75, @_ctx)
        while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
          if (_alt == 1)
            @_state_number = 554
            match(COMMA)
            @_state_number = 555
            import_as_name()
          end
          @_state_number = 560
          @_err_handler.sync(self)
          _alt = @_interp.adaptive_predict(@_input, 75, @_ctx)
        end
        @_state_number = 562
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == COMMA)
          @_state_number = 561
          match(COMMA)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Dotted_as_namesContext < Antlr4::Runtime::ParserRuleContext
      def dotted_as_name()
        return rule_contexts("Dotted_as_nameContext")
      end

      def dotted_as_name_i(i)
        return rule_context("Dotted_as_nameContext", i)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_dotted_as_names
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterDotted_as_names))
          listener.enterDotted_as_names(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitDotted_as_names))
          listener.exitDotted_as_names(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitDotted_as_names))
          return visitor.visitDotted_as_names(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def dotted_as_names()
      _localctx = Dotted_as_namesContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 68, RULE_dotted_as_names)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 564
        dotted_as_name()
        @_state_number = 569
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == COMMA)
          @_state_number = 565
          match(COMMA)
          @_state_number = 566
          dotted_as_name()
          @_state_number = 571
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Dotted_nameContext < Antlr4::Runtime::ParserRuleContext
      def NAME()
        return tokens(Python3Parser::NAME)
      end

      def NAME_i(i)
        return token(Python3Parser::NAME, i)
      end

      def DOT()
        return tokens(Python3Parser::DOT)
      end

      def DOT_i(i)
        return token(Python3Parser::DOT, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_dotted_name
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterDotted_name))
          listener.enterDotted_name(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitDotted_name))
          listener.exitDotted_name(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitDotted_name))
          return visitor.visitDotted_name(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def dotted_name()
      _localctx = Dotted_nameContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 70, RULE_dotted_name)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 572
        match(NAME)
        @_state_number = 577
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == DOT)
          @_state_number = 573
          match(DOT)
          @_state_number = 574
          match(NAME)
          @_state_number = 579
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Global_stmtContext < Antlr4::Runtime::ParserRuleContext
      def GLOBAL()
        return token(Python3Parser::GLOBAL, 0)
      end

      def NAME()
        return tokens(Python3Parser::NAME)
      end

      def NAME_i(i)
        return token(Python3Parser::NAME, i)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_global_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterGlobal_stmt))
          listener.enterGlobal_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitGlobal_stmt))
          listener.exitGlobal_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitGlobal_stmt))
          return visitor.visitGlobal_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def global_stmt()
      _localctx = Global_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 72, RULE_global_stmt)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 580
        match(GLOBAL)
        @_state_number = 581
        match(NAME)
        @_state_number = 586
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == COMMA)
          @_state_number = 582
          match(COMMA)
          @_state_number = 583
          match(NAME)
          @_state_number = 588
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Nonlocal_stmtContext < Antlr4::Runtime::ParserRuleContext
      def NONLOCAL()
        return token(Python3Parser::NONLOCAL, 0)
      end

      def NAME()
        return tokens(Python3Parser::NAME)
      end

      def NAME_i(i)
        return token(Python3Parser::NAME, i)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_nonlocal_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterNonlocal_stmt))
          listener.enterNonlocal_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitNonlocal_stmt))
          listener.exitNonlocal_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitNonlocal_stmt))
          return visitor.visitNonlocal_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def nonlocal_stmt()
      _localctx = Nonlocal_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 74, RULE_nonlocal_stmt)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 589
        match(NONLOCAL)
        @_state_number = 590
        match(NAME)
        @_state_number = 595
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == COMMA)
          @_state_number = 591
          match(COMMA)
          @_state_number = 592
          match(NAME)
          @_state_number = 597
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Assert_stmtContext < Antlr4::Runtime::ParserRuleContext
      def ASSERT()
        return token(Python3Parser::ASSERT, 0)
      end

      def test()
        return rule_contexts("TestContext")
      end

      def test_i(i)
        return rule_context("TestContext", i)
      end

      def COMMA()
        return token(Python3Parser::COMMA, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_assert_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterAssert_stmt))
          listener.enterAssert_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitAssert_stmt))
          listener.exitAssert_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitAssert_stmt))
          return visitor.visitAssert_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def assert_stmt()
      _localctx = Assert_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 76, RULE_assert_stmt)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 598
        match(ASSERT)
        @_state_number = 599
        test()
        @_state_number = 602
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == COMMA)
          @_state_number = 600
          match(COMMA)
          @_state_number = 601
          test()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Compound_stmtContext < Antlr4::Runtime::ParserRuleContext
      def if_stmt()
        return rule_context("If_stmtContext", 0)
      end

      def while_stmt()
        return rule_context("While_stmtContext", 0)
      end

      def for_stmt()
        return rule_context("For_stmtContext", 0)
      end

      def try_stmt()
        return rule_context("Try_stmtContext", 0)
      end

      def with_stmt()
        return rule_context("With_stmtContext", 0)
      end

      def funcdef()
        return rule_context("FuncdefContext", 0)
      end

      def classdef()
        return rule_context("ClassdefContext", 0)
      end

      def decorated()
        return rule_context("DecoratedContext", 0)
      end

      def async_stmt()
        return rule_context("Async_stmtContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_compound_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterCompound_stmt))
          listener.enterCompound_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitCompound_stmt))
          listener.exitCompound_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitCompound_stmt))
          return visitor.visitCompound_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def compound_stmt()
      _localctx = Compound_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 78, RULE_compound_stmt)
      begin
        @_state_number = 613
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::IF
          enter_outer_alt(_localctx, 1)

          @_state_number = 604
          if_stmt()
        when Python3Parser::WHILE
          enter_outer_alt(_localctx, 2)

          @_state_number = 605
          while_stmt()
        when Python3Parser::FOR
          enter_outer_alt(_localctx, 3)

          @_state_number = 606
          for_stmt()
        when Python3Parser::TRY
          enter_outer_alt(_localctx, 4)

          @_state_number = 607
          try_stmt()
        when Python3Parser::WITH
          enter_outer_alt(_localctx, 5)

          @_state_number = 608
          with_stmt()
        when Python3Parser::DEF
          enter_outer_alt(_localctx, 6)

          @_state_number = 609
          funcdef()
        when Python3Parser::CLASS
          enter_outer_alt(_localctx, 7)

          @_state_number = 610
          classdef()
        when Python3Parser::AT
          enter_outer_alt(_localctx, 8)

          @_state_number = 611
          decorated()
        when Python3Parser::ASYNC
          enter_outer_alt(_localctx, 9)

          @_state_number = 612
          async_stmt()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Async_stmtContext < Antlr4::Runtime::ParserRuleContext
      def ASYNC()
        return token(Python3Parser::ASYNC, 0)
      end

      def funcdef()
        return rule_context("FuncdefContext", 0)
      end

      def with_stmt()
        return rule_context("With_stmtContext", 0)
      end

      def for_stmt()
        return rule_context("For_stmtContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_async_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterAsync_stmt))
          listener.enterAsync_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitAsync_stmt))
          listener.exitAsync_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitAsync_stmt))
          return visitor.visitAsync_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def async_stmt()
      _localctx = Async_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 80, RULE_async_stmt)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 615
        match(ASYNC)
        @_state_number = 619
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::DEF
          @_state_number = 616
          funcdef()
        when Python3Parser::WITH
          @_state_number = 617
          with_stmt()
        when Python3Parser::FOR
          @_state_number = 618
          for_stmt()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class If_stmtContext < Antlr4::Runtime::ParserRuleContext
      def IF()
        return token(Python3Parser::IF, 0)
      end

      def test()
        return rule_contexts("TestContext")
      end

      def test_i(i)
        return rule_context("TestContext", i)
      end

      def COLON()
        return tokens(Python3Parser::COLON)
      end

      def COLON_i(i)
        return token(Python3Parser::COLON, i)
      end

      def suite()
        return rule_contexts("SuiteContext")
      end

      def suite_i(i)
        return rule_context("SuiteContext", i)
      end

      def ELIF()
        return tokens(Python3Parser::ELIF)
      end

      def ELIF_i(i)
        return token(Python3Parser::ELIF, i)
      end

      def ELSE()
        return token(Python3Parser::ELSE, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_if_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterIf_stmt))
          listener.enterIf_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitIf_stmt))
          listener.exitIf_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitIf_stmt))
          return visitor.visitIf_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def if_stmt()
      _localctx = If_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 82, RULE_if_stmt)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 621
        match(IF)
        @_state_number = 622
        test()
        @_state_number = 623
        match(COLON)
        @_state_number = 624
        suite()
        @_state_number = 632
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == ELIF)
          @_state_number = 625
          match(ELIF)
          @_state_number = 626
          test()
          @_state_number = 627
          match(COLON)
          @_state_number = 628
          suite()
          @_state_number = 634
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
        @_state_number = 638
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == ELSE)
          @_state_number = 635
          match(ELSE)
          @_state_number = 636
          match(COLON)
          @_state_number = 637
          suite()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class While_stmtContext < Antlr4::Runtime::ParserRuleContext
      def WHILE()
        return token(Python3Parser::WHILE, 0)
      end

      def test()
        return rule_context("TestContext", 0)
      end

      def COLON()
        return tokens(Python3Parser::COLON)
      end

      def COLON_i(i)
        return token(Python3Parser::COLON, i)
      end

      def suite()
        return rule_contexts("SuiteContext")
      end

      def suite_i(i)
        return rule_context("SuiteContext", i)
      end

      def ELSE()
        return token(Python3Parser::ELSE, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_while_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterWhile_stmt))
          listener.enterWhile_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitWhile_stmt))
          listener.exitWhile_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitWhile_stmt))
          return visitor.visitWhile_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def while_stmt()
      _localctx = While_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 84, RULE_while_stmt)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 640
        match(WHILE)
        @_state_number = 641
        test()
        @_state_number = 642
        match(COLON)
        @_state_number = 643
        suite()
        @_state_number = 647
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == ELSE)
          @_state_number = 644
          match(ELSE)
          @_state_number = 645
          match(COLON)
          @_state_number = 646
          suite()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class For_stmtContext < Antlr4::Runtime::ParserRuleContext
      def FOR()
        return token(Python3Parser::FOR, 0)
      end

      def exprlist()
        return rule_context("ExprlistContext", 0)
      end

      def IN()
        return token(Python3Parser::IN, 0)
      end

      def testlist()
        return rule_context("TestlistContext", 0)
      end

      def COLON()
        return tokens(Python3Parser::COLON)
      end

      def COLON_i(i)
        return token(Python3Parser::COLON, i)
      end

      def suite()
        return rule_contexts("SuiteContext")
      end

      def suite_i(i)
        return rule_context("SuiteContext", i)
      end

      def ELSE()
        return token(Python3Parser::ELSE, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_for_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterFor_stmt))
          listener.enterFor_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitFor_stmt))
          listener.exitFor_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitFor_stmt))
          return visitor.visitFor_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def for_stmt()
      _localctx = For_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 86, RULE_for_stmt)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 649
        match(FOR)
        @_state_number = 650
        exprlist()
        @_state_number = 651
        match(IN)
        @_state_number = 652
        testlist()
        @_state_number = 653
        match(COLON)
        @_state_number = 654
        suite()
        @_state_number = 658
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == ELSE)
          @_state_number = 655
          match(ELSE)
          @_state_number = 656
          match(COLON)
          @_state_number = 657
          suite()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Try_stmtContext < Antlr4::Runtime::ParserRuleContext
      def TRY()
        return token(Python3Parser::TRY, 0)
      end

      def COLON()
        return tokens(Python3Parser::COLON)
      end

      def COLON_i(i)
        return token(Python3Parser::COLON, i)
      end

      def suite()
        return rule_contexts("SuiteContext")
      end

      def suite_i(i)
        return rule_context("SuiteContext", i)
      end

      def FINALLY()
        return token(Python3Parser::FINALLY, 0)
      end

      def except_clause()
        return rule_contexts("Except_clauseContext")
      end

      def except_clause_i(i)
        return rule_context("Except_clauseContext", i)
      end

      def ELSE()
        return token(Python3Parser::ELSE, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_try_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterTry_stmt))
          listener.enterTry_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitTry_stmt))
          listener.exitTry_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitTry_stmt))
          return visitor.visitTry_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def try_stmt()
      _localctx = Try_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 88, RULE_try_stmt)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 660
        match(TRY)
        @_state_number = 661
        match(COLON)
        @_state_number = 662
        suite()
        @_state_number = 684
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::EXCEPT
          @_state_number = 667
          @_err_handler.sync(self)
          _la = @_input.la(1)
          loop do
            @_state_number = 663
            except_clause()
            @_state_number = 664
            match(COLON)
            @_state_number = 665
            suite()
            @_state_number = 669
            @_err_handler.sync(self)
            _la = @_input.la(1)
            break if (!(_la == EXCEPT))
          end
          @_state_number = 674
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == ELSE)
            @_state_number = 671
            match(ELSE)
            @_state_number = 672
            match(COLON)
            @_state_number = 673
            suite()
          end

          @_state_number = 679
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == FINALLY)
            @_state_number = 676
            match(FINALLY)
            @_state_number = 677
            match(COLON)
            @_state_number = 678
            suite()
          end
        when Python3Parser::FINALLY
          @_state_number = 681
          match(FINALLY)
          @_state_number = 682
          match(COLON)
          @_state_number = 683
          suite()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class With_stmtContext < Antlr4::Runtime::ParserRuleContext
      def WITH()
        return token(Python3Parser::WITH, 0)
      end

      def with_item()
        return rule_contexts("With_itemContext")
      end

      def with_item_i(i)
        return rule_context("With_itemContext", i)
      end

      def COLON()
        return token(Python3Parser::COLON, 0)
      end

      def suite()
        return rule_context("SuiteContext", 0)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_with_stmt
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterWith_stmt))
          listener.enterWith_stmt(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitWith_stmt))
          listener.exitWith_stmt(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitWith_stmt))
          return visitor.visitWith_stmt(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def with_stmt()
      _localctx = With_stmtContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 90, RULE_with_stmt)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 686
        match(WITH)
        @_state_number = 687
        with_item()
        @_state_number = 692
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == COMMA)
          @_state_number = 688
          match(COMMA)
          @_state_number = 689
          with_item()
          @_state_number = 694
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
        @_state_number = 695
        match(COLON)
        @_state_number = 696
        suite()
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class With_itemContext < Antlr4::Runtime::ParserRuleContext
      def test()
        return rule_context("TestContext", 0)
      end

      def AS()
        return token(Python3Parser::AS, 0)
      end

      def expr()
        return rule_context("ExprContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_with_item
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterWith_item))
          listener.enterWith_item(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitWith_item))
          listener.exitWith_item(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitWith_item))
          return visitor.visitWith_item(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def with_item()
      _localctx = With_itemContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 92, RULE_with_item)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 698
        test()
        @_state_number = 701
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == AS)
          @_state_number = 699
          match(AS)
          @_state_number = 700
          expr()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Except_clauseContext < Antlr4::Runtime::ParserRuleContext
      def EXCEPT()
        return token(Python3Parser::EXCEPT, 0)
      end

      def test()
        return rule_context("TestContext", 0)
      end

      def AS()
        return token(Python3Parser::AS, 0)
      end

      def NAME()
        return token(Python3Parser::NAME, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_except_clause
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterExcept_clause))
          listener.enterExcept_clause(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitExcept_clause))
          listener.exitExcept_clause(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitExcept_clause))
          return visitor.visitExcept_clause(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def except_clause()
      _localctx = Except_clauseContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 94, RULE_except_clause)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 703
        match(EXCEPT)
        @_state_number = 709
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << OPEN_PAREN) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)))) != 0))
          @_state_number = 704
          test()
          @_state_number = 707
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == AS)
            @_state_number = 705
            match(AS)
            @_state_number = 706
            match(NAME)
          end
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class SuiteContext < Antlr4::Runtime::ParserRuleContext
      def simple_stmt()
        return rule_context("Simple_stmtContext", 0)
      end

      def NEWLINE()
        return token(Python3Parser::NEWLINE, 0)
      end

      def INDENT()
        return token(Python3Parser::INDENT, 0)
      end

      def DEDENT()
        return token(Python3Parser::DEDENT, 0)
      end

      def stmt()
        return rule_contexts("StmtContext")
      end

      def stmt_i(i)
        return rule_context("StmtContext", i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_suite
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterSuite))
          listener.enterSuite(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitSuite))
          listener.exitSuite(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitSuite))
          return visitor.visitSuite(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def suite()
      _localctx = SuiteContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 96, RULE_suite)
      _la = 0
      begin
        @_state_number = 721
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::RETURN, Python3Parser::RAISE, Python3Parser::FROM, Python3Parser::IMPORT, Python3Parser::GLOBAL, Python3Parser::NONLOCAL, Python3Parser::ASSERT, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::YIELD, Python3Parser::DEL, Python3Parser::PASS, Python3Parser::CONTINUE, Python3Parser::BREAK, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::STAR, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
          enter_outer_alt(_localctx, 1)

          @_state_number = 711
          simple_stmt()
        when Python3Parser::NEWLINE
          enter_outer_alt(_localctx, 2)

          @_state_number = 712
          match(NEWLINE)
          @_state_number = 713
          match(INDENT)
          @_state_number = 715
          @_err_handler.sync(self)
          _la = @_input.la(1)
          loop do
            @_state_number = 714
            stmt()
            @_state_number = 717
            @_err_handler.sync(self)
            _la = @_input.la(1)
            break if (!((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << DEF) | (1 << RETURN) | (1 << RAISE) | (1 << FROM) | (1 << IMPORT) | (1 << GLOBAL) | (1 << NONLOCAL) | (1 << ASSERT) | (1 << IF) | (1 << WHILE) | (1 << FOR) | (1 << TRY) | (1 << WITH) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << CLASS) | (1 << YIELD) | (1 << DEL) | (1 << PASS) | (1 << CONTINUE) | (1 << BREAK) | (1 << ASYNC) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << STAR) | (1 << OPEN_PAREN) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)) | (1 << (AT - 66)))) != 0)))
          end
          @_state_number = 719
          match(DEDENT)
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class TestContext < Antlr4::Runtime::ParserRuleContext
      def or_test()
        return rule_contexts("Or_testContext")
      end

      def or_test_i(i)
        return rule_context("Or_testContext", i)
      end

      def IF()
        return token(Python3Parser::IF, 0)
      end

      def ELSE()
        return token(Python3Parser::ELSE, 0)
      end

      def test()
        return rule_context("TestContext", 0)
      end

      def lambdef()
        return rule_context("LambdefContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_test
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterTest))
          listener.enterTest(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitTest))
          listener.exitTest(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitTest))
          return visitor.visitTest(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def test()
      _localctx = TestContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 98, RULE_test)
      _la = 0
      begin
        @_state_number = 732
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
          enter_outer_alt(_localctx, 1)

          @_state_number = 723
          or_test()
          @_state_number = 729
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == IF)
            @_state_number = 724
            match(IF)
            @_state_number = 725
            or_test()
            @_state_number = 726
            match(ELSE)
            @_state_number = 727
            test()
          end
        when Python3Parser::LAMBDA
          enter_outer_alt(_localctx, 2)

          @_state_number = 731
          lambdef()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Test_nocondContext < Antlr4::Runtime::ParserRuleContext
      def or_test()
        return rule_context("Or_testContext", 0)
      end

      def lambdef_nocond()
        return rule_context("Lambdef_nocondContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_test_nocond
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterTest_nocond))
          listener.enterTest_nocond(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitTest_nocond))
          listener.exitTest_nocond(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitTest_nocond))
          return visitor.visitTest_nocond(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def test_nocond()
      _localctx = Test_nocondContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 100, RULE_test_nocond)
      begin
        @_state_number = 736
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
          enter_outer_alt(_localctx, 1)

          @_state_number = 734
          or_test()
        when Python3Parser::LAMBDA
          enter_outer_alt(_localctx, 2)

          @_state_number = 735
          lambdef_nocond()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class LambdefContext < Antlr4::Runtime::ParserRuleContext
      def LAMBDA()
        return token(Python3Parser::LAMBDA, 0)
      end

      def COLON()
        return token(Python3Parser::COLON, 0)
      end

      def test()
        return rule_context("TestContext", 0)
      end

      def varargslist()
        return rule_context("VarargslistContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_lambdef
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterLambdef))
          listener.enterLambdef(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitLambdef))
          listener.exitLambdef(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitLambdef))
          return visitor.visitLambdef(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def lambdef()
      _localctx = LambdefContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 102, RULE_lambdef)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 738
        match(LAMBDA)
        @_state_number = 740
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << NAME) | (1 << STAR) | (1 << POWER))) != 0))
          @_state_number = 739
          varargslist()
        end

        @_state_number = 742
        match(COLON)
        @_state_number = 743
        test()
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Lambdef_nocondContext < Antlr4::Runtime::ParserRuleContext
      def LAMBDA()
        return token(Python3Parser::LAMBDA, 0)
      end

      def COLON()
        return token(Python3Parser::COLON, 0)
      end

      def test_nocond()
        return rule_context("Test_nocondContext", 0)
      end

      def varargslist()
        return rule_context("VarargslistContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_lambdef_nocond
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterLambdef_nocond))
          listener.enterLambdef_nocond(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitLambdef_nocond))
          listener.exitLambdef_nocond(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitLambdef_nocond))
          return visitor.visitLambdef_nocond(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def lambdef_nocond()
      _localctx = Lambdef_nocondContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 104, RULE_lambdef_nocond)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 745
        match(LAMBDA)
        @_state_number = 747
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << NAME) | (1 << STAR) | (1 << POWER))) != 0))
          @_state_number = 746
          varargslist()
        end

        @_state_number = 749
        match(COLON)
        @_state_number = 750
        test_nocond()
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Or_testContext < Antlr4::Runtime::ParserRuleContext
      def and_test()
        return rule_contexts("And_testContext")
      end

      def and_test_i(i)
        return rule_context("And_testContext", i)
      end

      def OR()
        return tokens(Python3Parser::OR)
      end

      def OR_i(i)
        return token(Python3Parser::OR, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_or_test
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterOr_test))
          listener.enterOr_test(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitOr_test))
          listener.exitOr_test(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitOr_test))
          return visitor.visitOr_test(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def or_test()
      _localctx = Or_testContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 106, RULE_or_test)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 752
        and_test()
        @_state_number = 757
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == OR)
          @_state_number = 753
          match(OR)
          @_state_number = 754
          and_test()
          @_state_number = 759
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class And_testContext < Antlr4::Runtime::ParserRuleContext
      def not_test()
        return rule_contexts("Not_testContext")
      end

      def not_test_i(i)
        return rule_context("Not_testContext", i)
      end

      def AND()
        return tokens(Python3Parser::AND)
      end

      def AND_i(i)
        return token(Python3Parser::AND, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_and_test
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterAnd_test))
          listener.enterAnd_test(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitAnd_test))
          listener.exitAnd_test(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitAnd_test))
          return visitor.visitAnd_test(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def and_test()
      _localctx = And_testContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 108, RULE_and_test)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 760
        not_test()
        @_state_number = 765
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == AND)
          @_state_number = 761
          match(AND)
          @_state_number = 762
          not_test()
          @_state_number = 767
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Not_testContext < Antlr4::Runtime::ParserRuleContext
      def NOT()
        return token(Python3Parser::NOT, 0)
      end

      def not_test()
        return rule_context("Not_testContext", 0)
      end

      def comparison()
        return rule_context("ComparisonContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_not_test
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterNot_test))
          listener.enterNot_test(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitNot_test))
          listener.exitNot_test(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitNot_test))
          return visitor.visitNot_test(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def not_test()
      _localctx = Not_testContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 110, RULE_not_test)
      begin
        @_state_number = 771
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::NOT
          enter_outer_alt(_localctx, 1)

          @_state_number = 768
          match(NOT)
          @_state_number = 769
          not_test()
        when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
          enter_outer_alt(_localctx, 2)

          @_state_number = 770
          comparison()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class ComparisonContext < Antlr4::Runtime::ParserRuleContext
      def expr()
        return rule_contexts("ExprContext")
      end

      def expr_i(i)
        return rule_context("ExprContext", i)
      end

      def comp_op()
        return rule_contexts("Comp_opContext")
      end

      def comp_op_i(i)
        return rule_context("Comp_opContext", i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_comparison
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterComparison))
          listener.enterComparison(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitComparison))
          listener.exitComparison(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitComparison))
          return visitor.visitComparison(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def comparison()
      _localctx = ComparisonContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 112, RULE_comparison)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 773
        expr()
        @_state_number = 779
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (((((_la - 18)) & ~0x3f) == 0 && ((1 << (_la - 18)) & ((1 << (IN - 18)) | (1 << (NOT - 18)) | (1 << (IS - 18)) | (1 << (LESS_THAN - 18)) | (1 << (GREATER_THAN - 18)) | (1 << (EQUALS - 18)) | (1 << (GT_EQ - 18)) | (1 << (LT_EQ - 18)) | (1 << (NOT_EQ_1 - 18)) | (1 << (NOT_EQ_2 - 18)))) != 0))
          @_state_number = 774
          comp_op()
          @_state_number = 775
          expr()
          @_state_number = 781
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Comp_opContext < Antlr4::Runtime::ParserRuleContext
      def LESS_THAN()
        return token(Python3Parser::LESS_THAN, 0)
      end

      def GREATER_THAN()
        return token(Python3Parser::GREATER_THAN, 0)
      end

      def EQUALS()
        return token(Python3Parser::EQUALS, 0)
      end

      def GT_EQ()
        return token(Python3Parser::GT_EQ, 0)
      end

      def LT_EQ()
        return token(Python3Parser::LT_EQ, 0)
      end

      def NOT_EQ_1()
        return token(Python3Parser::NOT_EQ_1, 0)
      end

      def NOT_EQ_2()
        return token(Python3Parser::NOT_EQ_2, 0)
      end

      def IN()
        return token(Python3Parser::IN, 0)
      end

      def NOT()
        return token(Python3Parser::NOT, 0)
      end

      def IS()
        return token(Python3Parser::IS, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_comp_op
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterComp_op))
          listener.enterComp_op(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitComp_op))
          listener.exitComp_op(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitComp_op))
          return visitor.visitComp_op(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def comp_op()
      _localctx = Comp_opContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 114, RULE_comp_op)
      begin
        @_state_number = 795
        @_err_handler.sync(self)
        case (@_interp.adaptive_predict(@_input, 107, @_ctx))
        when 1
          enter_outer_alt(_localctx, 1)

          @_state_number = 782
          match(LESS_THAN)
        when 2
          enter_outer_alt(_localctx, 2)

          @_state_number = 783
          match(GREATER_THAN)
        when 3
          enter_outer_alt(_localctx, 3)

          @_state_number = 784
          match(EQUALS)
        when 4
          enter_outer_alt(_localctx, 4)

          @_state_number = 785
          match(GT_EQ)
        when 5
          enter_outer_alt(_localctx, 5)

          @_state_number = 786
          match(LT_EQ)
        when 6
          enter_outer_alt(_localctx, 6)

          @_state_number = 787
          match(NOT_EQ_1)
        when 7
          enter_outer_alt(_localctx, 7)

          @_state_number = 788
          match(NOT_EQ_2)
        when 8
          enter_outer_alt(_localctx, 8)

          @_state_number = 789
          match(IN)
        when 9
          enter_outer_alt(_localctx, 9)

          @_state_number = 790
          match(NOT)
          @_state_number = 791
          match(IN)
        when 10
          enter_outer_alt(_localctx, 10)

          @_state_number = 792
          match(IS)
        when 11
          enter_outer_alt(_localctx, 11)

          @_state_number = 793
          match(IS)
          @_state_number = 794
          match(NOT)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Star_exprContext < Antlr4::Runtime::ParserRuleContext
      def STAR()
        return token(Python3Parser::STAR, 0)
      end

      def expr()
        return rule_context("ExprContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_star_expr
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterStar_expr))
          listener.enterStar_expr(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitStar_expr))
          listener.exitStar_expr(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitStar_expr))
          return visitor.visitStar_expr(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def star_expr()
      _localctx = Star_exprContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 116, RULE_star_expr)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 797
        match(STAR)
        @_state_number = 798
        expr()
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class ExprContext < Antlr4::Runtime::ParserRuleContext
      def xor_expr()
        return rule_contexts("Xor_exprContext")
      end

      def xor_expr_i(i)
        return rule_context("Xor_exprContext", i)
      end

      def OR_OP()
        return tokens(Python3Parser::OR_OP)
      end

      def OR_OP_i(i)
        return token(Python3Parser::OR_OP, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_expr
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterExpr))
          listener.enterExpr(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitExpr))
          listener.exitExpr(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitExpr))
          return visitor.visitExpr(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def expr()
      _localctx = ExprContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 118, RULE_expr)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 800
        xor_expr()
        @_state_number = 805
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == OR_OP)
          @_state_number = 801
          match(OR_OP)
          @_state_number = 802
          xor_expr()
          @_state_number = 807
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Xor_exprContext < Antlr4::Runtime::ParserRuleContext
      def and_expr()
        return rule_contexts("And_exprContext")
      end

      def and_expr_i(i)
        return rule_context("And_exprContext", i)
      end

      def XOR()
        return tokens(Python3Parser::XOR)
      end

      def XOR_i(i)
        return token(Python3Parser::XOR, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_xor_expr
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterXor_expr))
          listener.enterXor_expr(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitXor_expr))
          listener.exitXor_expr(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitXor_expr))
          return visitor.visitXor_expr(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def xor_expr()
      _localctx = Xor_exprContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 120, RULE_xor_expr)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 808
        and_expr()
        @_state_number = 813
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == XOR)
          @_state_number = 809
          match(XOR)
          @_state_number = 810
          and_expr()
          @_state_number = 815
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class And_exprContext < Antlr4::Runtime::ParserRuleContext
      def shift_expr()
        return rule_contexts("Shift_exprContext")
      end

      def shift_expr_i(i)
        return rule_context("Shift_exprContext", i)
      end

      def AND_OP()
        return tokens(Python3Parser::AND_OP)
      end

      def AND_OP_i(i)
        return token(Python3Parser::AND_OP, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_and_expr
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterAnd_expr))
          listener.enterAnd_expr(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitAnd_expr))
          listener.exitAnd_expr(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitAnd_expr))
          return visitor.visitAnd_expr(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def and_expr()
      _localctx = And_exprContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 122, RULE_and_expr)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 816
        shift_expr()
        @_state_number = 821
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == AND_OP)
          @_state_number = 817
          match(AND_OP)
          @_state_number = 818
          shift_expr()
          @_state_number = 823
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Shift_exprContext < Antlr4::Runtime::ParserRuleContext
      def arith_expr()
        return rule_contexts("Arith_exprContext")
      end

      def arith_expr_i(i)
        return rule_context("Arith_exprContext", i)
      end

      def LEFT_SHIFT()
        return tokens(Python3Parser::LEFT_SHIFT)
      end

      def LEFT_SHIFT_i(i)
        return token(Python3Parser::LEFT_SHIFT, i)
      end

      def RIGHT_SHIFT()
        return tokens(Python3Parser::RIGHT_SHIFT)
      end

      def RIGHT_SHIFT_i(i)
        return token(Python3Parser::RIGHT_SHIFT, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_shift_expr
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterShift_expr))
          listener.enterShift_expr(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitShift_expr))
          listener.exitShift_expr(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitShift_expr))
          return visitor.visitShift_expr(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def shift_expr()
      _localctx = Shift_exprContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 124, RULE_shift_expr)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 824
        arith_expr()
        @_state_number = 829
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == LEFT_SHIFT || _la == RIGHT_SHIFT)
          @_state_number = 825
          _la = @_input.la(1)
          if (!(_la == LEFT_SHIFT || _la == RIGHT_SHIFT))
            @_err_handler.recover_in_line(self)
          else
            if (@_input.la(1) == Antlr4::Runtime::Token::EOF)
              @matchedEOF = true
            end
            @_err_handler.report_match(self)
            consume()
          end
          @_state_number = 826
          arith_expr()
          @_state_number = 831
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Arith_exprContext < Antlr4::Runtime::ParserRuleContext
      def term()
        return rule_contexts("TermContext")
      end

      def term_i(i)
        return rule_context("TermContext", i)
      end

      def ADD()
        return tokens(Python3Parser::ADD)
      end

      def ADD_i(i)
        return token(Python3Parser::ADD, i)
      end

      def MINUS()
        return tokens(Python3Parser::MINUS)
      end

      def MINUS_i(i)
        return token(Python3Parser::MINUS, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_arith_expr
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterArith_expr))
          listener.enterArith_expr(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitArith_expr))
          listener.exitArith_expr(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitArith_expr))
          return visitor.visitArith_expr(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def arith_expr()
      _localctx = Arith_exprContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 126, RULE_arith_expr)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 832
        term()
        @_state_number = 837
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (_la == ADD || _la == MINUS)
          @_state_number = 833
          _la = @_input.la(1)
          if (!(_la == ADD || _la == MINUS))
            @_err_handler.recover_in_line(self)
          else
            if (@_input.la(1) == Antlr4::Runtime::Token::EOF)
              @matchedEOF = true
            end
            @_err_handler.report_match(self)
            consume()
          end
          @_state_number = 834
          term()
          @_state_number = 839
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class TermContext < Antlr4::Runtime::ParserRuleContext
      def factor()
        return rule_contexts("FactorContext")
      end

      def factor_i(i)
        return rule_context("FactorContext", i)
      end

      def STAR()
        return tokens(Python3Parser::STAR)
      end

      def STAR_i(i)
        return token(Python3Parser::STAR, i)
      end

      def AT()
        return tokens(Python3Parser::AT)
      end

      def AT_i(i)
        return token(Python3Parser::AT, i)
      end

      def DIV()
        return tokens(Python3Parser::DIV)
      end

      def DIV_i(i)
        return token(Python3Parser::DIV, i)
      end

      def MOD()
        return tokens(Python3Parser::MOD)
      end

      def MOD_i(i)
        return token(Python3Parser::MOD, i)
      end

      def IDIV()
        return tokens(Python3Parser::IDIV)
      end

      def IDIV_i(i)
        return token(Python3Parser::IDIV, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_term
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterTerm))
          listener.enterTerm(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitTerm))
          listener.exitTerm(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitTerm))
          return visitor.visitTerm(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def term()
      _localctx = TermContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 128, RULE_term)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 840
        factor()
        @_state_number = 845
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while (((((_la - 51)) & ~0x3f) == 0 && ((1 << (_la - 51)) & ((1 << (STAR - 51)) | (1 << (DIV - 51)) | (1 << (MOD - 51)) | (1 << (IDIV - 51)) | (1 << (AT - 51)))) != 0))
          @_state_number = 841
          _la = @_input.la(1)
          if (!(((((_la - 51)) & ~0x3f) == 0 && ((1 << (_la - 51)) & ((1 << (STAR - 51)) | (1 << (DIV - 51)) | (1 << (MOD - 51)) | (1 << (IDIV - 51)) | (1 << (AT - 51)))) != 0)))
            @_err_handler.recover_in_line(self)
          else
            if (@_input.la(1) == Antlr4::Runtime::Token::EOF)
              @matchedEOF = true
            end
            @_err_handler.report_match(self)
            consume()
          end
          @_state_number = 842
          factor()
          @_state_number = 847
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class FactorContext < Antlr4::Runtime::ParserRuleContext
      def factor()
        return rule_context("FactorContext", 0)
      end

      def ADD()
        return token(Python3Parser::ADD, 0)
      end

      def MINUS()
        return token(Python3Parser::MINUS, 0)
      end

      def NOT_OP()
        return token(Python3Parser::NOT_OP, 0)
      end

      def power()
        return rule_context("PowerContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_factor
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterFactor))
          listener.enterFactor(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitFactor))
          listener.exitFactor(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitFactor))
          return visitor.visitFactor(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def factor()
      _localctx = FactorContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 130, RULE_factor)
      _la = 0
      begin
        @_state_number = 851
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP
          enter_outer_alt(_localctx, 1)

          @_state_number = 848
          _la = @_input.la(1)
          if (!(((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)))) != 0)))
            @_err_handler.recover_in_line(self)
          else
            if (@_input.la(1) == Antlr4::Runtime::Token::EOF)
              @matchedEOF = true
            end
            @_err_handler.report_match(self)
            consume()
          end
          @_state_number = 849
          factor()
        when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::OPEN_BRACE
          enter_outer_alt(_localctx, 2)

          @_state_number = 850
          power()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class PowerContext < Antlr4::Runtime::ParserRuleContext
      def atom_expr()
        return rule_context("Atom_exprContext", 0)
      end

      def POWER()
        return token(Python3Parser::POWER, 0)
      end

      def factor()
        return rule_context("FactorContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_power
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterPower))
          listener.enterPower(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitPower))
          listener.exitPower(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitPower))
          return visitor.visitPower(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def power()
      _localctx = PowerContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 132, RULE_power)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 853
        atom_expr()
        @_state_number = 856
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == POWER)
          @_state_number = 854
          match(POWER)
          @_state_number = 855
          factor()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Atom_exprContext < Antlr4::Runtime::ParserRuleContext
      def atom()
        return rule_context("AtomContext", 0)
      end

      def AWAIT()
        return token(Python3Parser::AWAIT, 0)
      end

      def trailer()
        return rule_contexts("TrailerContext")
      end

      def trailer_i(i)
        return rule_context("TrailerContext", i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_atom_expr
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterAtom_expr))
          listener.enterAtom_expr(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitAtom_expr))
          listener.exitAtom_expr(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitAtom_expr))
          return visitor.visitAtom_expr(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def atom_expr()
      _localctx = Atom_exprContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 134, RULE_atom_expr)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 859
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == AWAIT)
          @_state_number = 858
          match(AWAIT)
        end

        @_state_number = 861
        atom()
        @_state_number = 865
        @_err_handler.sync(self)
        _la = @_input.la(1)
        while ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << DOT) | (1 << OPEN_PAREN) | (1 << OPEN_BRACK))) != 0))
          @_state_number = 862
          trailer()
          @_state_number = 867
          @_err_handler.sync(self)
          _la = @_input.la(1)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class AtomContext < Antlr4::Runtime::ParserRuleContext
      def OPEN_PAREN()
        return token(Python3Parser::OPEN_PAREN, 0)
      end

      def CLOSE_PAREN()
        return token(Python3Parser::CLOSE_PAREN, 0)
      end

      def OPEN_BRACK()
        return token(Python3Parser::OPEN_BRACK, 0)
      end

      def CLOSE_BRACK()
        return token(Python3Parser::CLOSE_BRACK, 0)
      end

      def OPEN_BRACE()
        return token(Python3Parser::OPEN_BRACE, 0)
      end

      def CLOSE_BRACE()
        return token(Python3Parser::CLOSE_BRACE, 0)
      end

      def NAME()
        return token(Python3Parser::NAME, 0)
      end

      def NUMBER()
        return token(Python3Parser::NUMBER, 0)
      end

      def ELLIPSIS()
        return token(Python3Parser::ELLIPSIS, 0)
      end

      def NONE()
        return token(Python3Parser::NONE, 0)
      end

      def TRUE()
        return token(Python3Parser::TRUE, 0)
      end

      def FALSE()
        return token(Python3Parser::FALSE, 0)
      end

      def yield_expr()
        return rule_context("Yield_exprContext", 0)
      end

      def testlist_comp()
        return rule_context("Testlist_compContext", 0)
      end

      def dictorsetmaker()
        return rule_context("DictorsetmakerContext", 0)
      end

      def STRING()
        return tokens(Python3Parser::STRING)
      end

      def STRING_i(i)
        return token(Python3Parser::STRING, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_atom
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterAtom))
          listener.enterAtom(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitAtom))
          listener.exitAtom(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitAtom))
          return visitor.visitAtom(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def atom()
      _localctx = AtomContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 136, RULE_atom)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 895
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::OPEN_PAREN
          @_state_number = 868
          match(OPEN_PAREN)
          @_state_number = 871
          @_err_handler.sync(self)
          case (@_input.la(1))
          when YIELD
            @_state_number = 869
            yield_expr()
          when STRING, NUMBER, LAMBDA, NOT, NONE, TRUE, FALSE, AWAIT, NAME, ELLIPSIS, STAR, OPEN_PAREN, OPEN_BRACK, ADD, MINUS, NOT_OP, OPEN_BRACE
            @_state_number = 870
            testlist_comp()
          when CLOSE_PAREN
          else
          end
          @_state_number = 873
          match(CLOSE_PAREN)
        when Python3Parser::OPEN_BRACK
          @_state_number = 874
          match(OPEN_BRACK)
          @_state_number = 876
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << STAR) | (1 << OPEN_PAREN) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)))) != 0))
            @_state_number = 875
            testlist_comp()
          end

          @_state_number = 878
          match(CLOSE_BRACK)
        when Python3Parser::OPEN_BRACE
          @_state_number = 879
          match(OPEN_BRACE)
          @_state_number = 881
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << STAR) | (1 << OPEN_PAREN) | (1 << POWER) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)))) != 0))
            @_state_number = 880
            dictorsetmaker()
          end

          @_state_number = 883
          match(CLOSE_BRACE)
        when Python3Parser::NAME
          @_state_number = 884
          match(NAME)
        when Python3Parser::NUMBER
          @_state_number = 885
          match(NUMBER)
        when Python3Parser::STRING
          @_state_number = 887
          @_err_handler.sync(self)
          _la = @_input.la(1)
          loop do
            @_state_number = 886
            match(STRING)
            @_state_number = 889
            @_err_handler.sync(self)
            _la = @_input.la(1)
            break if (!(_la == STRING))
          end
        when Python3Parser::ELLIPSIS
          @_state_number = 891
          match(ELLIPSIS)
        when Python3Parser::NONE
          @_state_number = 892
          match(NONE)
        when Python3Parser::TRUE
          @_state_number = 893
          match(TRUE)
        when Python3Parser::FALSE
          @_state_number = 894
          match(FALSE)
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Testlist_compContext < Antlr4::Runtime::ParserRuleContext
      def test()
        return rule_contexts("TestContext")
      end

      def test_i(i)
        return rule_context("TestContext", i)
      end

      def star_expr()
        return rule_contexts("Star_exprContext")
      end

      def star_expr_i(i)
        return rule_context("Star_exprContext", i)
      end

      def comp_for()
        return rule_context("Comp_forContext", 0)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_testlist_comp
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterTestlist_comp))
          listener.enterTestlist_comp(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitTestlist_comp))
          listener.exitTestlist_comp(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitTestlist_comp))
          return visitor.visitTestlist_comp(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def testlist_comp()
      _localctx = Testlist_compContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 138, RULE_testlist_comp)
      _la = 0
      begin
        _alt = 0
        enter_outer_alt(_localctx, 1)

        @_state_number = 899
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
          @_state_number = 897
          test()
        when Python3Parser::STAR
          @_state_number = 898
          star_expr()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
        @_state_number = 915
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::FOR, Python3Parser::ASYNC
          @_state_number = 901
          comp_for()
        when Python3Parser::CLOSE_PAREN, Python3Parser::COMMA, Python3Parser::CLOSE_BRACK
          @_state_number = 909
          @_err_handler.sync(self)
          _alt = @_interp.adaptive_predict(@_input, 125, @_ctx)
          while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
            if (_alt == 1)
              @_state_number = 902
              match(COMMA)
              @_state_number = 905
              @_err_handler.sync(self)
              case (@_input.la(1))
              when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
                @_state_number = 903
                test()
              when Python3Parser::STAR
                @_state_number = 904
                star_expr()
              else
                raise Antlr4::Runtime::NoViableAltException, self
              end
            end
            @_state_number = 911
            @_err_handler.sync(self)
            _alt = @_interp.adaptive_predict(@_input, 125, @_ctx)
          end
          @_state_number = 913
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == COMMA)
            @_state_number = 912
            match(COMMA)
          end
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class TrailerContext < Antlr4::Runtime::ParserRuleContext
      def OPEN_PAREN()
        return token(Python3Parser::OPEN_PAREN, 0)
      end

      def CLOSE_PAREN()
        return token(Python3Parser::CLOSE_PAREN, 0)
      end

      def arglist()
        return rule_context("ArglistContext", 0)
      end

      def OPEN_BRACK()
        return token(Python3Parser::OPEN_BRACK, 0)
      end

      def subscriptlist()
        return rule_context("SubscriptlistContext", 0)
      end

      def CLOSE_BRACK()
        return token(Python3Parser::CLOSE_BRACK, 0)
      end

      def DOT()
        return token(Python3Parser::DOT, 0)
      end

      def NAME()
        return token(Python3Parser::NAME, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_trailer
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterTrailer))
          listener.enterTrailer(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitTrailer))
          listener.exitTrailer(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitTrailer))
          return visitor.visitTrailer(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def trailer()
      _localctx = TrailerContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 140, RULE_trailer)
      _la = 0
      begin
        @_state_number = 928
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::OPEN_PAREN
          enter_outer_alt(_localctx, 1)

          @_state_number = 917
          match(OPEN_PAREN)
          @_state_number = 919
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << STAR) | (1 << OPEN_PAREN) | (1 << POWER) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)))) != 0))
            @_state_number = 918
            arglist()
          end

          @_state_number = 921
          match(CLOSE_PAREN)
        when Python3Parser::OPEN_BRACK
          enter_outer_alt(_localctx, 2)

          @_state_number = 922
          match(OPEN_BRACK)
          @_state_number = 923
          subscriptlist()
          @_state_number = 924
          match(CLOSE_BRACK)
        when Python3Parser::DOT
          enter_outer_alt(_localctx, 3)

          @_state_number = 926
          match(DOT)
          @_state_number = 927
          match(NAME)
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class SubscriptlistContext < Antlr4::Runtime::ParserRuleContext
      def subscript()
        return rule_contexts("SubscriptContext")
      end

      def subscript_i(i)
        return rule_context("SubscriptContext", i)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_subscriptlist
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterSubscriptlist))
          listener.enterSubscriptlist(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitSubscriptlist))
          listener.exitSubscriptlist(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitSubscriptlist))
          return visitor.visitSubscriptlist(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def subscriptlist()
      _localctx = SubscriptlistContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 142, RULE_subscriptlist)
      _la = 0
      begin
        _alt = 0
        enter_outer_alt(_localctx, 1)

        @_state_number = 930
        subscript()
        @_state_number = 935
        @_err_handler.sync(self)
        _alt = @_interp.adaptive_predict(@_input, 130, @_ctx)
        while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
          if (_alt == 1)
            @_state_number = 931
            match(COMMA)
            @_state_number = 932
            subscript()
          end
          @_state_number = 937
          @_err_handler.sync(self)
          _alt = @_interp.adaptive_predict(@_input, 130, @_ctx)
        end
        @_state_number = 939
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == COMMA)
          @_state_number = 938
          match(COMMA)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class SubscriptContext < Antlr4::Runtime::ParserRuleContext
      def test()
        return rule_contexts("TestContext")
      end

      def test_i(i)
        return rule_context("TestContext", i)
      end

      def COLON()
        return token(Python3Parser::COLON, 0)
      end

      def sliceop()
        return rule_context("SliceopContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_subscript
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterSubscript))
          listener.enterSubscript(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitSubscript))
          listener.exitSubscript(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitSubscript))
          return visitor.visitSubscript(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def subscript()
      _localctx = SubscriptContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 144, RULE_subscript)
      _la = 0
      begin
        @_state_number = 952
        @_err_handler.sync(self)
        case (@_interp.adaptive_predict(@_input, 135, @_ctx))
        when 1
          enter_outer_alt(_localctx, 1)

          @_state_number = 941
          test()
        when 2
          enter_outer_alt(_localctx, 2)

          @_state_number = 943
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << OPEN_PAREN) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)))) != 0))
            @_state_number = 942
            test()
          end

          @_state_number = 945
          match(COLON)
          @_state_number = 947
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << OPEN_PAREN) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)))) != 0))
            @_state_number = 946
            test()
          end

          @_state_number = 950
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == COLON)
            @_state_number = 949
            sliceop()
          end
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class SliceopContext < Antlr4::Runtime::ParserRuleContext
      def COLON()
        return token(Python3Parser::COLON, 0)
      end

      def test()
        return rule_context("TestContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_sliceop
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterSliceop))
          listener.enterSliceop(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitSliceop))
          listener.exitSliceop(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitSliceop))
          return visitor.visitSliceop(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def sliceop()
      _localctx = SliceopContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 146, RULE_sliceop)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 954
        match(COLON)
        @_state_number = 956
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << OPEN_PAREN) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)))) != 0))
          @_state_number = 955
          test()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class ExprlistContext < Antlr4::Runtime::ParserRuleContext
      def expr()
        return rule_contexts("ExprContext")
      end

      def expr_i(i)
        return rule_context("ExprContext", i)
      end

      def star_expr()
        return rule_contexts("Star_exprContext")
      end

      def star_expr_i(i)
        return rule_context("Star_exprContext", i)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_exprlist
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterExprlist))
          listener.enterExprlist(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitExprlist))
          listener.exitExprlist(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitExprlist))
          return visitor.visitExprlist(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def exprlist()
      _localctx = ExprlistContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 148, RULE_exprlist)
      _la = 0
      begin
        _alt = 0
        enter_outer_alt(_localctx, 1)

        @_state_number = 960
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
          @_state_number = 958
          expr()
        when Python3Parser::STAR
          @_state_number = 959
          star_expr()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
        @_state_number = 969
        @_err_handler.sync(self)
        _alt = @_interp.adaptive_predict(@_input, 139, @_ctx)
        while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
          if (_alt == 1)
            @_state_number = 962
            match(COMMA)
            @_state_number = 965
            @_err_handler.sync(self)
            case (@_input.la(1))
            when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
              @_state_number = 963
              expr()
            when Python3Parser::STAR
              @_state_number = 964
              star_expr()
            else
              raise Antlr4::Runtime::NoViableAltException, self
            end
          end
          @_state_number = 971
          @_err_handler.sync(self)
          _alt = @_interp.adaptive_predict(@_input, 139, @_ctx)
        end
        @_state_number = 973
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == COMMA)
          @_state_number = 972
          match(COMMA)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class TestlistContext < Antlr4::Runtime::ParserRuleContext
      def test()
        return rule_contexts("TestContext")
      end

      def test_i(i)
        return rule_context("TestContext", i)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_testlist
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterTestlist))
          listener.enterTestlist(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitTestlist))
          listener.exitTestlist(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitTestlist))
          return visitor.visitTestlist(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def testlist()
      _localctx = TestlistContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 150, RULE_testlist)
      _la = 0
      begin
        _alt = 0
        enter_outer_alt(_localctx, 1)

        @_state_number = 975
        test()
        @_state_number = 980
        @_err_handler.sync(self)
        _alt = @_interp.adaptive_predict(@_input, 141, @_ctx)
        while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
          if (_alt == 1)
            @_state_number = 976
            match(COMMA)
            @_state_number = 977
            test()
          end
          @_state_number = 982
          @_err_handler.sync(self)
          _alt = @_interp.adaptive_predict(@_input, 141, @_ctx)
        end
        @_state_number = 984
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == COMMA)
          @_state_number = 983
          match(COMMA)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class DictorsetmakerContext < Antlr4::Runtime::ParserRuleContext
      def test()
        return rule_contexts("TestContext")
      end

      def test_i(i)
        return rule_context("TestContext", i)
      end

      def COLON()
        return tokens(Python3Parser::COLON)
      end

      def COLON_i(i)
        return token(Python3Parser::COLON, i)
      end

      def POWER()
        return tokens(Python3Parser::POWER)
      end

      def POWER_i(i)
        return token(Python3Parser::POWER, i)
      end

      def expr()
        return rule_contexts("ExprContext")
      end

      def expr_i(i)
        return rule_context("ExprContext", i)
      end

      def comp_for()
        return rule_context("Comp_forContext", 0)
      end

      def star_expr()
        return rule_contexts("Star_exprContext")
      end

      def star_expr_i(i)
        return rule_context("Star_exprContext", i)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_dictorsetmaker
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterDictorsetmaker))
          listener.enterDictorsetmaker(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitDictorsetmaker))
          listener.exitDictorsetmaker(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitDictorsetmaker))
          return visitor.visitDictorsetmaker(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def dictorsetmaker()
      _localctx = DictorsetmakerContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 152, RULE_dictorsetmaker)
      _la = 0
      begin
        _alt = 0
        enter_outer_alt(_localctx, 1)

        @_state_number = 1034
        @_err_handler.sync(self)
        case (@_interp.adaptive_predict(@_input, 153, @_ctx))
        when 1
          @_state_number = 992
          @_err_handler.sync(self)
          case (@_input.la(1))
          when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
            @_state_number = 986
            test()
            @_state_number = 987
            match(COLON)
            @_state_number = 988
            test()
          when Python3Parser::POWER
            @_state_number = 990
            match(POWER)
            @_state_number = 991
            expr()
          else
            raise Antlr4::Runtime::NoViableAltException, self
          end
          @_state_number = 1012
          @_err_handler.sync(self)
          case (@_input.la(1))
          when Python3Parser::FOR, Python3Parser::ASYNC
            @_state_number = 994
            comp_for()
          when Python3Parser::COMMA, Python3Parser::CLOSE_BRACE
            @_state_number = 1006
            @_err_handler.sync(self)
            _alt = @_interp.adaptive_predict(@_input, 145, @_ctx)
            while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
              if (_alt == 1)
                @_state_number = 995
                match(COMMA)
                @_state_number = 1002
                @_err_handler.sync(self)
                case (@_input.la(1))
                when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
                  @_state_number = 996
                  test()
                  @_state_number = 997
                  match(COLON)
                  @_state_number = 998
                  test()
                when Python3Parser::POWER
                  @_state_number = 1000
                  match(POWER)
                  @_state_number = 1001
                  expr()
                else
                  raise Antlr4::Runtime::NoViableAltException, self
                end
              end
              @_state_number = 1008
              @_err_handler.sync(self)
              _alt = @_interp.adaptive_predict(@_input, 145, @_ctx)
            end
            @_state_number = 1010
            @_err_handler.sync(self)
            _la = @_input.la(1)
            if (_la == COMMA)
              @_state_number = 1009
              match(COMMA)
            end
          else
            raise Antlr4::Runtime::NoViableAltException, self
          end
        when 2
          @_state_number = 1016
          @_err_handler.sync(self)
          case (@_input.la(1))
          when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
            @_state_number = 1014
            test()
          when Python3Parser::STAR
            @_state_number = 1015
            star_expr()
          else
            raise Antlr4::Runtime::NoViableAltException, self
          end
          @_state_number = 1032
          @_err_handler.sync(self)
          case (@_input.la(1))
          when Python3Parser::FOR, Python3Parser::ASYNC
            @_state_number = 1018
            comp_for()
          when Python3Parser::COMMA, Python3Parser::CLOSE_BRACE
            @_state_number = 1026
            @_err_handler.sync(self)
            _alt = @_interp.adaptive_predict(@_input, 150, @_ctx)
            while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
              if (_alt == 1)
                @_state_number = 1019
                match(COMMA)
                @_state_number = 1022
                @_err_handler.sync(self)
                case (@_input.la(1))
                when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
                  @_state_number = 1020
                  test()
                when Python3Parser::STAR
                  @_state_number = 1021
                  star_expr()
                else
                  raise Antlr4::Runtime::NoViableAltException, self
                end
              end
              @_state_number = 1028
              @_err_handler.sync(self)
              _alt = @_interp.adaptive_predict(@_input, 150, @_ctx)
            end
            @_state_number = 1030
            @_err_handler.sync(self)
            _la = @_input.la(1)
            if (_la == COMMA)
              @_state_number = 1029
              match(COMMA)
            end
          else
            raise Antlr4::Runtime::NoViableAltException, self
          end
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class ClassdefContext < Antlr4::Runtime::ParserRuleContext
      def CLASS()
        return token(Python3Parser::CLASS, 0)
      end

      def NAME()
        return token(Python3Parser::NAME, 0)
      end

      def COLON()
        return token(Python3Parser::COLON, 0)
      end

      def suite()
        return rule_context("SuiteContext", 0)
      end

      def OPEN_PAREN()
        return token(Python3Parser::OPEN_PAREN, 0)
      end

      def CLOSE_PAREN()
        return token(Python3Parser::CLOSE_PAREN, 0)
      end

      def arglist()
        return rule_context("ArglistContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_classdef
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterClassdef))
          listener.enterClassdef(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitClassdef))
          listener.exitClassdef(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitClassdef))
          return visitor.visitClassdef(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def classdef()
      _localctx = ClassdefContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 154, RULE_classdef)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 1036
        match(CLASS)
        @_state_number = 1037
        match(NAME)
        @_state_number = 1043
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == OPEN_PAREN)
          @_state_number = 1038
          match(OPEN_PAREN)
          @_state_number = 1040
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << STAR) | (1 << OPEN_PAREN) | (1 << POWER) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)))) != 0))
            @_state_number = 1039
            arglist()
          end

          @_state_number = 1042
          match(CLOSE_PAREN)
        end

        @_state_number = 1045
        match(COLON)
        @_state_number = 1046
        suite()
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class ArglistContext < Antlr4::Runtime::ParserRuleContext
      def argument()
        return rule_contexts("ArgumentContext")
      end

      def argument_i(i)
        return rule_context("ArgumentContext", i)
      end

      def COMMA()
        return tokens(Python3Parser::COMMA)
      end

      def COMMA_i(i)
        return token(Python3Parser::COMMA, i)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_arglist
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterArglist))
          listener.enterArglist(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitArglist))
          listener.exitArglist(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitArglist))
          return visitor.visitArglist(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def arglist()
      _localctx = ArglistContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 156, RULE_arglist)
      _la = 0
      begin
        _alt = 0
        enter_outer_alt(_localctx, 1)

        @_state_number = 1048
        argument()
        @_state_number = 1053
        @_err_handler.sync(self)
        _alt = @_interp.adaptive_predict(@_input, 156, @_ctx)
        while (_alt != 2 && _alt != Antlr4::Runtime::ATN::INVALID_ALT_NUMBER)
          if (_alt == 1)
            @_state_number = 1049
            match(COMMA)
            @_state_number = 1050
            argument()
          end
          @_state_number = 1055
          @_err_handler.sync(self)
          _alt = @_interp.adaptive_predict(@_input, 156, @_ctx)
        end
        @_state_number = 1057
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == COMMA)
          @_state_number = 1056
          match(COMMA)
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class ArgumentContext < Antlr4::Runtime::ParserRuleContext
      def test()
        return rule_contexts("TestContext")
      end

      def test_i(i)
        return rule_context("TestContext", i)
      end

      def ASSIGN()
        return token(Python3Parser::ASSIGN, 0)
      end

      def POWER()
        return token(Python3Parser::POWER, 0)
      end

      def STAR()
        return token(Python3Parser::STAR, 0)
      end

      def comp_for()
        return rule_context("Comp_forContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_argument
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterArgument))
          listener.enterArgument(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitArgument))
          listener.exitArgument(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitArgument))
          return visitor.visitArgument(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def argument()
      _localctx = ArgumentContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 158, RULE_argument)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 1071
        @_err_handler.sync(self)
        case (@_interp.adaptive_predict(@_input, 159, @_ctx))
        when 1
          @_state_number = 1059
          test()
          @_state_number = 1061
          @_err_handler.sync(self)
          _la = @_input.la(1)
          if (_la == FOR || _la == ASYNC)
            @_state_number = 1060
            comp_for()
          end
        when 2
          @_state_number = 1063
          test()
          @_state_number = 1064
          match(ASSIGN)
          @_state_number = 1065
          test()
        when 3
          @_state_number = 1067
          match(POWER)
          @_state_number = 1068
          test()
        when 4
          @_state_number = 1069
          match(STAR)
          @_state_number = 1070
          test()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Comp_iterContext < Antlr4::Runtime::ParserRuleContext
      def comp_for()
        return rule_context("Comp_forContext", 0)
      end

      def comp_if()
        return rule_context("Comp_ifContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_comp_iter
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterComp_iter))
          listener.enterComp_iter(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitComp_iter))
          listener.exitComp_iter(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitComp_iter))
          return visitor.visitComp_iter(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def comp_iter()
      _localctx = Comp_iterContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 160, RULE_comp_iter)
      begin
        @_state_number = 1075
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::FOR, Python3Parser::ASYNC
          enter_outer_alt(_localctx, 1)

          @_state_number = 1073
          comp_for()
        when Python3Parser::IF
          enter_outer_alt(_localctx, 2)

          @_state_number = 1074
          comp_if()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Comp_forContext < Antlr4::Runtime::ParserRuleContext
      def FOR()
        return token(Python3Parser::FOR, 0)
      end

      def exprlist()
        return rule_context("ExprlistContext", 0)
      end

      def IN()
        return token(Python3Parser::IN, 0)
      end

      def or_test()
        return rule_context("Or_testContext", 0)
      end

      def ASYNC()
        return token(Python3Parser::ASYNC, 0)
      end

      def comp_iter()
        return rule_context("Comp_iterContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_comp_for
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterComp_for))
          listener.enterComp_for(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitComp_for))
          listener.exitComp_for(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitComp_for))
          return visitor.visitComp_for(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def comp_for()
      _localctx = Comp_forContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 162, RULE_comp_for)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 1078
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if (_la == ASYNC)
          @_state_number = 1077
          match(ASYNC)
        end

        @_state_number = 1080
        match(FOR)
        @_state_number = 1081
        exprlist()
        @_state_number = 1082
        match(IN)
        @_state_number = 1083
        or_test()
        @_state_number = 1085
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << IF) | (1 << FOR) | (1 << ASYNC))) != 0))
          @_state_number = 1084
          comp_iter()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Comp_ifContext < Antlr4::Runtime::ParserRuleContext
      def IF()
        return token(Python3Parser::IF, 0)
      end

      def test_nocond()
        return rule_context("Test_nocondContext", 0)
      end

      def comp_iter()
        return rule_context("Comp_iterContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_comp_if
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterComp_if))
          listener.enterComp_if(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitComp_if))
          listener.exitComp_if(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitComp_if))
          return visitor.visitComp_if(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def comp_if()
      _localctx = Comp_ifContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 164, RULE_comp_if)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 1087
        match(IF)
        @_state_number = 1088
        test_nocond()
        @_state_number = 1090
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << IF) | (1 << FOR) | (1 << ASYNC))) != 0))
          @_state_number = 1089
          comp_iter()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Encoding_declContext < Antlr4::Runtime::ParserRuleContext
      def NAME()
        return token(Python3Parser::NAME, 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_encoding_decl
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterEncoding_decl))
          listener.enterEncoding_decl(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitEncoding_decl))
          listener.exitEncoding_decl(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitEncoding_decl))
          return visitor.visitEncoding_decl(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def encoding_decl()
      _localctx = Encoding_declContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 166, RULE_encoding_decl)
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 1092
        match(NAME)
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Yield_exprContext < Antlr4::Runtime::ParserRuleContext
      def YIELD()
        return token(Python3Parser::YIELD, 0)
      end

      def yield_arg()
        return rule_context("Yield_argContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_yield_expr
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterYield_expr))
          listener.enterYield_expr(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitYield_expr))
          listener.exitYield_expr(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitYield_expr))
          return visitor.visitYield_expr(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def yield_expr()
      _localctx = Yield_exprContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 168, RULE_yield_expr)
      _la = 0
      begin
        enter_outer_alt(_localctx, 1)

        @_state_number = 1094
        match(YIELD)
        @_state_number = 1096
        @_err_handler.sync(self)
        _la = @_input.la(1)
        if ((((_la) & ~0x3f) == 0 && ((1 << _la) & ((1 << STRING) | (1 << NUMBER) | (1 << FROM) | (1 << LAMBDA) | (1 << NOT) | (1 << NONE) | (1 << TRUE) | (1 << FALSE) | (1 << AWAIT) | (1 << NAME) | (1 << ELLIPSIS) | (1 << OPEN_PAREN) | (1 << OPEN_BRACK))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1 << (_la - 66)) & ((1 << (ADD - 66)) | (1 << (MINUS - 66)) | (1 << (NOT_OP - 66)) | (1 << (OPEN_BRACE - 66)))) != 0))
          @_state_number = 1095
          yield_arg()
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    class Yield_argContext < Antlr4::Runtime::ParserRuleContext
      def FROM()
        return token(Python3Parser::FROM, 0)
      end

      def test()
        return rule_context("TestContext", 0)
      end

      def testlist()
        return rule_context("TestlistContext", 0)
      end

      def initialize(parent, invokingState)
        super(parent, invokingState)
      end

      def getRuleIndex()
        return RULE_yield_arg
      end

      def enter_rule(listener)
        if (listener.respond_to?(:enterYield_arg))
          listener.enterYield_arg(self)
        end
      end

      def exit_rule(listener)
        if (listener.respond_to?(:exitYield_arg))
          listener.exitYield_arg(self)
        end
      end

      def accept(visitor)
        if (visitor.respond_to?(:visitYield_arg))
          return visitor.visitYield_arg(self)
        else
          return visitor.visit_children(self)
        end
      end
    end

    def yield_arg()
      _localctx = Yield_argContext.new(@_ctx, @_state_number)
      enter_rule(_localctx, 170, RULE_yield_arg)
      begin
        @_state_number = 1101
        @_err_handler.sync(self)
        case (@_input.la(1))
        when Python3Parser::FROM
          enter_outer_alt(_localctx, 1)

          @_state_number = 1098
          match(FROM)
          @_state_number = 1099
          test()
        when Python3Parser::STRING, Python3Parser::NUMBER, Python3Parser::LAMBDA, Python3Parser::NOT, Python3Parser::NONE, Python3Parser::TRUE, Python3Parser::FALSE, Python3Parser::AWAIT, Python3Parser::NAME, Python3Parser::ELLIPSIS, Python3Parser::OPEN_PAREN, Python3Parser::OPEN_BRACK, Python3Parser::ADD, Python3Parser::MINUS, Python3Parser::NOT_OP, Python3Parser::OPEN_BRACE
          enter_outer_alt(_localctx, 2)

          @_state_number = 1100
          testlist()
        else
          raise Antlr4::Runtime::NoViableAltException, self
        end
      rescue Antlr4::Runtime::RecognitionException => re
        _localctx.exception = re
        @_err_handler.report_error(self, re)
        @_err_handler.recover(self, re)
      ensure
        exit_rule()
      end
      return _localctx
    end

    @@_serializedATN = ["\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964",
                        "\3e\u0452\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4",
                        "\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t",
                        "\20\4\21\t\21\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27",
                        "\t\27\4\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4",
                        "\36\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'",
                        "\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61",
                        "\t\61\4\62\t\62\4\63\t\63\4\64\t\64\4\65\t\65\4\66\t\66\4\67\t\67\4",
                        "8\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4",
                        "C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4",
                        "N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT\4U\tU\4V\tV\4W\tW\3\2\3\2",
                        "\3\2\3\2\3\2\5\2\u00b4\n\2\3\3\3\3\7\3\u00b8\n\3\f\3\16\3\u00bb\13",
                        "\3\3\3\3\3\3\4\3\4\7\4\u00c1\n\4\f\4\16\4\u00c4\13\4\3\4\3\4\3\5\3",
                        "\5\3\5\3\5\5\5\u00cc\n\5\3\5\5\5\u00cf\n\5\3\5\3\5\3\6\6\6\u00d4\n",
                        "\6\r\6\16\6\u00d5\3\7\3\7\3\7\3\7\5\7\u00dc\n\7\3\b\3\b\3\b\3\t\3\t",
                        "\3\t\3\t\3\t\5\t\u00e6\n\t\3\t\3\t\3\t\3\n\3\n\5\n\u00ed\n\n\3\n\3",
                        "\n\3\13\3\13\3\13\5\13\u00f4\n\13\3\13\3\13\3\13\3\13\5\13\u00fa\n",
                        "\13\7\13\u00fc\n\13\f\13\16\13\u00ff\13\13\3\13\3\13\3\13\5\13\u0104",
                        "\n\13\3\13\3\13\3\13\3\13\5\13\u010a\n\13\7\13\u010c\n\13\f\13\16\13",
                        "\u010f\13\13\3\13\3\13\3\13\3\13\5\13\u0115\n\13\5\13\u0117\n\13\5",
                        "\13\u0119\n\13\3\13\3\13\3\13\5\13\u011e\n\13\5\13\u0120\n\13\5\13",
                        "\u0122\n\13\3\13\3\13\5\13\u0126\n\13\3\13\3\13\3\13\3\13\5\13\u012c",
                        "\n\13\7\13\u012e\n\13\f\13\16\13\u0131\13\13\3\13\3\13\3\13\3\13\5",
                        "\13\u0137\n\13\5\13\u0139\n\13\5\13\u013b\n\13\3\13\3\13\3\13\5\13",
                        "\u0140\n\13\5\13\u0142\n\13\3\f\3\f\3\f\5\f\u0147\n\f\3\r\3\r\3\r\5",
                        "\r\u014c\n\r\3\r\3\r\3\r\3\r\5\r\u0152\n\r\7\r\u0154\n\r\f\r\16\r\u0157",
                        "\13\r\3\r\3\r\3\r\5\r\u015c\n\r\3\r\3\r\3\r\3\r\5\r\u0162\n\r\7\r\u0164",
                        "\n\r\f\r\16\r\u0167\13\r\3\r\3\r\3\r\3\r\5\r\u016d\n\r\5\r\u016f\n",
                        "\r\5\r\u0171\n\r\3\r\3\r\3\r\5\r\u0176\n\r\5\r\u0178\n\r\5\r\u017a",
                        "\n\r\3\r\3\r\5\r\u017e\n\r\3\r\3\r\3\r\3\r\5\r\u0184\n\r\7\r\u0186",
                        "\n\r\f\r\16\r\u0189\13\r\3\r\3\r\3\r\3\r\5\r\u018f\n\r\5\r\u0191\n",
                        "\r\5\r\u0193\n\r\3\r\3\r\3\r\5\r\u0198\n\r\5\r\u019a\n\r\3\16\3\16",
                        "\3\17\3\17\5\17\u01a0\n\17\3\20\3\20\3\20\7\20\u01a5\n\20\f\20\16\20",
                        "\u01a8\13\20\3\20\5\20\u01ab\n\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21",
                        "\3\21\3\21\3\21\5\21\u01b7\n\21\3\22\3\22\3\22\3\22\3\22\5\22\u01be",
                        "\n\22\3\22\3\22\3\22\5\22\u01c3\n\22\7\22\u01c5\n\22\f\22\16\22\u01c8",
                        "\13\22\5\22\u01ca\n\22\3\23\3\23\3\23\3\23\5\23\u01d0\n\23\3\24\3\24",
                        "\5\24\u01d4\n\24\3\24\3\24\3\24\5\24\u01d9\n\24\7\24\u01db\n\24\f\24",
                        "\16\24\u01de\13\24\3\24\5\24\u01e1\n\24\3\25\3\25\3\26\3\26\3\26\3",
                        "\27\3\27\3\30\3\30\3\30\3\30\3\30\5\30\u01ef\n\30\3\31\3\31\3\32\3",
                        "\32\3\33\3\33\5\33\u01f7\n\33\3\34\3\34\3\35\3\35\3\35\3\35\5\35\u01ff",
                        "\n\35\5\35\u0201\n\35\3\36\3\36\5\36\u0205\n\36\3\37\3\37\3\37\3 \3",
                        " \7 \u020c\n \f \16 \u020f\13 \3 \3 \6 \u0213\n \r \16 \u0214\5 \u0217",
                        "\n \3 \3 \3 \3 \3 \3 \3 \5 \u0220\n \3!\3!\3!\5!\u0225\n!\3\"\3\"\3",
                        "\"\5\"\u022a\n\"\3#\3#\3#\7#\u022f\n#\f#\16#\u0232\13#\3#\5#\u0235",
                        "\n#\3$\3$\3$\7$\u023a\n$\f$\16$\u023d\13$\3%\3%\3%\7%\u0242\n%\f%\16",
                        "%\u0245\13%\3&\3&\3&\3&\7&\u024b\n&\f&\16&\u024e\13&\3\'\3\'\3\'\3",
                        "\'\7\'\u0254\n\'\f\'\16\'\u0257\13\'\3(\3(\3(\3(\5(\u025d\n(\3)\3)",
                        "\3)\3)\3)\3)\3)\3)\3)\5)\u0268\n)\3*\3*\3*\3*\5*\u026e\n*\3+\3+\3+",
                        "\3+\3+\3+\3+\3+\3+\7+\u0279\n+\f+\16+\u027c\13+\3+\3+\3+\5+\u0281\n",
                        "+\3,\3,\3,\3,\3,\3,\3,\5,\u028a\n,\3-\3-\3-\3-\3-\3-\3-\3-\3-\5-\u0295",
                        "\n-\3.\3.\3.\3.\3.\3.\3.\6.\u029e\n.\r.\16.\u029f\3.\3.\3.\5.\u02a5",
                        "\n.\3.\3.\3.\5.\u02aa\n.\3.\3.\3.\5.\u02af\n.\3/\3/\3/\3/\7/\u02b5",
                        "\n/\f/\16/\u02b8\13/\3/\3/\3/\3\60\3\60\3\60\5\60\u02c0\n\60\3\61\3",
                        "\61\3\61\3\61\5\61\u02c6\n\61\5\61\u02c8\n\61\3\62\3\62\3\62\3\62\6",
                        "\62\u02ce\n\62\r\62\16\62\u02cf\3\62\3\62\5\62\u02d4\n\62\3\63\3\63",
                        "\3\63\3\63\3\63\3\63\5\63\u02dc\n\63\3\63\5\63\u02df\n\63\3\64\3\64",
                        "\5\64\u02e3\n\64\3\65\3\65\5\65\u02e7\n\65\3\65\3\65\3\65\3\66\3\66",
                        "\5\66\u02ee\n\66\3\66\3\66\3\66\3\67\3\67\3\67\7\67\u02f6\n\67\f\67",
                        "\16\67\u02f9\13\67\38\38\38\78\u02fe\n8\f8\168\u0301\138\39\39\39\5",
                        "9\u0306\n9\3:\3:\3:\3:\7:\u030c\n:\f:\16:\u030f\13:\3;\3;\3;\3;\3;",
                        "\3;\3;\3;\3;\3;\3;\3;\3;\5;\u031e\n;\3<\3<\3<\3=\3=\3=\7=\u0326\n=",
                        "\f=\16=\u0329\13=\3>\3>\3>\7>\u032e\n>\f>\16>\u0331\13>\3?\3?\3?\7",
                        "?\u0336\n?\f?\16?\u0339\13?\3@\3@\3@\7@\u033e\n@\f@\16@\u0341\13@\3",
                        "A\3A\3A\7A\u0346\nA\fA\16A\u0349\13A\3B\3B\3B\7B\u034e\nB\fB\16B\u0351",
                        "\13B\3C\3C\3C\5C\u0356\nC\3D\3D\3D\5D\u035b\nD\3E\5E\u035e\nE\3E\3",
                        "E\7E\u0362\nE\fE\16E\u0365\13E\3F\3F\3F\5F\u036a\nF\3F\3F\3F\5F\u036f",
                        "\nF\3F\3F\3F\5F\u0374\nF\3F\3F\3F\3F\6F\u037a\nF\rF\16F\u037b\3F\3",
                        "F\3F\3F\5F\u0382\nF\3G\3G\5G\u0386\nG\3G\3G\3G\3G\5G\u038c\nG\7G\u038e",
                        "\nG\fG\16G\u0391\13G\3G\5G\u0394\nG\5G\u0396\nG\3H\3H\5H\u039a\nH\3",
                        "H\3H\3H\3H\3H\3H\3H\5H\u03a3\nH\3I\3I\3I\7I\u03a8\nI\fI\16I\u03ab\13",
                        "I\3I\5I\u03ae\nI\3J\3J\5J\u03b2\nJ\3J\3J\5J\u03b6\nJ\3J\5J\u03b9\n",
                        "J\5J\u03bb\nJ\3K\3K\5K\u03bf\nK\3L\3L\5L\u03c3\nL\3L\3L\3L\5L\u03c8",
                        "\nL\7L\u03ca\nL\fL\16L\u03cd\13L\3L\5L\u03d0\nL\3M\3M\3M\7M\u03d5\n",
                        "M\fM\16M\u03d8\13M\3M\5M\u03db\nM\3N\3N\3N\3N\3N\3N\5N\u03e3\nN\3N",
                        "\3N\3N\3N\3N\3N\3N\3N\5N\u03ed\nN\7N\u03ef\nN\fN\16N\u03f2\13N\3N\5",
                        "N\u03f5\nN\5N\u03f7\nN\3N\3N\5N\u03fb\nN\3N\3N\3N\3N\5N\u0401\nN\7",
                        "N\u0403\nN\fN\16N\u0406\13N\3N\5N\u0409\nN\5N\u040b\nN\5N\u040d\nN",
                        "\3O\3O\3O\3O\5O\u0413\nO\3O\5O\u0416\nO\3O\3O\3O\3P\3P\3P\7P\u041e",
                        "\nP\fP\16P\u0421\13P\3P\5P\u0424\nP\3Q\3Q\5Q\u0428\nQ\3Q\3Q\3Q\3Q\3",
                        "Q\3Q\3Q\3Q\5Q\u0432\nQ\3R\3R\5R\u0436\nR\3S\5S\u0439\nS\3S\3S\3S\3",
                        "S\3S\5S\u0440\nS\3T\3T\3T\5T\u0445\nT\3U\3U\3V\3V\5V\u044b\nV\3W\3",
                        "W\3W\5W\u0450\nW\3W\2\2X\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"",
                        "$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082",
                        "\u0084\u0086\u0088\u008a\u008c\u008e\u0090\u0092\u0094\u0096\u0098",
                        "\u009a\u009c\u009e\u00a0\u00a2\u00a4\u00a6\u00a8\u00aa\u00ac\2\b\3",
                        "\2Ua\3\2\63\64\3\2BC\3\2DE\5\2\65\65FHSS\4\2DEII\2\u04cf\2\u00b3\3",
                        "\2\2\2\4\u00b9\3\2\2\2\6\u00be\3\2\2\2\b\u00c7\3\2\2\2\n\u00d3\3\2",
                        "\2\2\f\u00d7\3\2\2\2\16\u00dd\3\2\2\2\20\u00e0\3\2\2\2\22\u00ea\3\2",
                        "\2\2\24\u0141\3\2\2\2\26\u0143\3\2\2\2\30\u0199\3\2\2\2\32\u019b\3",
                        "\2\2\2\34\u019f\3\2\2\2\36\u01a1\3\2\2\2 \u01b6\3\2\2\2\"\u01b8\3\2",
                        "\2\2$\u01cb\3\2\2\2&\u01d3\3\2\2\2(\u01e2\3\2\2\2*\u01e4\3\2\2\2,\u01e7",
                        "\3\2\2\2.\u01ee\3\2\2\2\60\u01f0\3\2\2\2\62\u01f2\3\2\2\2\64\u01f4",
                        "\3\2\2\2\66\u01f8\3\2\2\28\u01fa\3\2\2\2:\u0204\3\2\2\2<\u0206\3\2",
                        "\2\2>\u0209\3\2\2\2@\u0221\3\2\2\2B\u0226\3\2\2\2D\u022b\3\2\2\2F\u0236",
                        "\3\2\2\2H\u023e\3\2\2\2J\u0246\3\2\2\2L\u024f\3\2\2\2N\u0258\3\2\2",
                        "\2P\u0267\3\2\2\2R\u0269\3\2\2\2T\u026f\3\2\2\2V\u0282\3\2\2\2X\u028b",
                        "\3\2\2\2Z\u0296\3\2\2\2\\\u02b0\3\2\2\2^\u02bc\3\2\2\2`\u02c1\3\2\2",
                        "\2b\u02d3\3\2\2\2d\u02de\3\2\2\2f\u02e2\3\2\2\2h\u02e4\3\2\2\2j\u02eb",
                        "\3\2\2\2l\u02f2\3\2\2\2n\u02fa\3\2\2\2p\u0305\3\2\2\2r\u0307\3\2\2",
                        "\2t\u031d\3\2\2\2v\u031f\3\2\2\2x\u0322\3\2\2\2z\u032a\3\2\2\2|\u0332",
                        "\3\2\2\2~\u033a\3\2\2\2\u0080\u0342\3\2\2\2\u0082\u034a\3\2\2\2\u0084",
                        "\u0355\3\2\2\2\u0086\u0357\3\2\2\2\u0088\u035d\3\2\2\2\u008a\u0381",
                        "\3\2\2\2\u008c\u0385\3\2\2\2\u008e\u03a2\3\2\2\2\u0090\u03a4\3\2\2",
                        "\2\u0092\u03ba\3\2\2\2\u0094\u03bc\3\2\2\2\u0096\u03c2\3\2\2\2\u0098",
                        "\u03d1\3\2\2\2\u009a\u040c\3\2\2\2\u009c\u040e\3\2\2\2\u009e\u041a",
                        "\3\2\2\2\u00a0\u0431\3\2\2\2\u00a2\u0435\3\2\2\2\u00a4\u0438\3\2\2",
                        "\2\u00a6\u0441\3\2\2\2\u00a8\u0446\3\2\2\2\u00aa\u0448\3\2\2\2\u00ac",
                        "\u044f\3\2\2\2\u00ae\u00b4\7)\2\2\u00af\u00b4\5\36\20\2\u00b0\u00b1",
                        "\5P)\2\u00b1\u00b2\7)\2\2\u00b2\u00b4\3\2\2\2\u00b3\u00ae\3\2\2\2\u00b3",
                        "\u00af\3\2\2\2\u00b3\u00b0\3\2\2\2\u00b4\3\3\2\2\2\u00b5\u00b8\7)\2",
                        "\2\u00b6\u00b8\5\34\17\2\u00b7\u00b5\3\2\2\2\u00b7\u00b6\3\2\2\2\u00b8",
                        "\u00bb\3\2\2\2\u00b9\u00b7\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba\u00bc",
                        "\3\2\2\2\u00bb\u00b9\3\2\2\2\u00bc\u00bd\7\2\2\3\u00bd\5\3\2\2\2\u00be",
                        "\u00c2\5\u0098M\2\u00bf\u00c1\7)\2\2\u00c0\u00bf\3\2\2\2\u00c1\u00c4",
                        "\3\2\2\2\u00c2\u00c0\3\2\2\2\u00c2\u00c3\3\2\2\2\u00c3\u00c5\3\2\2",
                        "\2\u00c4\u00c2\3\2\2\2\u00c5\u00c6\7\2\2\3\u00c6\7\3\2\2\2\u00c7\u00c8",
                        "\7S\2\2\u00c8\u00ce\5H%\2\u00c9\u00cb\7\66\2\2\u00ca\u00cc\5\u009e",
                        "P\2\u00cb\u00ca\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00cd\3\2\2\2\u00cd",
                        "\u00cf\7\67\2\2\u00ce\u00c9\3\2\2\2\u00ce\u00cf\3\2\2\2\u00cf\u00d0",
                        "\3\2\2\2\u00d0\u00d1\7)\2\2\u00d1\t\3\2\2\2\u00d2\u00d4\5\b\5\2\u00d3",
                        "\u00d2\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5\u00d3\3\2\2\2\u00d5\u00d6",
                        "\3\2\2\2\u00d6\13\3\2\2\2\u00d7\u00db\5\n\6\2\u00d8\u00dc\5\u009cO",
                        "\2\u00d9\u00dc\5\20\t\2\u00da\u00dc\5\16\b\2\u00db\u00d8\3\2\2\2\u00db",
                        "\u00d9\3\2\2\2\u00db\u00da\3\2\2\2\u00dc\r\3\2\2\2\u00dd\u00de\7\'",
                        "\2\2\u00de\u00df\5\20\t\2\u00df\17\3\2\2\2\u00e0\u00e1\7\6\2\2\u00e1",
                        "\u00e2\7*\2\2\u00e2\u00e5\5\22\n\2\u00e3\u00e4\7T\2\2\u00e4\u00e6\5",
                        "d\63\2\u00e5\u00e3\3\2\2\2\u00e5\u00e6\3\2\2\2\u00e6\u00e7\3\2\2\2",
                        "\u00e7\u00e8\79\2\2\u00e8\u00e9\5b\62\2\u00e9\21\3\2\2\2\u00ea\u00ec",
                        "\7\66\2\2\u00eb\u00ed\5\24\13\2\u00ec\u00eb\3\2\2\2\u00ec\u00ed\3\2",
                        "\2\2\u00ed\u00ee\3\2\2\2\u00ee\u00ef\7\67\2\2\u00ef\23\3\2\2\2\u00f0",
                        "\u00f3\5\26\f\2\u00f1\u00f2\7<\2\2\u00f2\u00f4\5d\63\2\u00f3\u00f1",
                        "\3\2\2\2\u00f3\u00f4\3\2\2\2\u00f4\u00fd\3\2\2\2\u00f5\u00f6\78\2\2",
                        "\u00f6\u00f9\5\26\f\2\u00f7\u00f8\7<\2\2\u00f8\u00fa\5d\63\2\u00f9",
                        "\u00f7\3\2\2\2\u00f9\u00fa\3\2\2\2\u00fa\u00fc\3\2\2\2\u00fb\u00f5",
                        "\3\2\2\2\u00fc\u00ff\3\2\2\2\u00fd\u00fb\3\2\2\2\u00fd\u00fe\3\2\2",
                        "\2\u00fe\u0121\3\2\2\2\u00ff\u00fd\3\2\2\2\u0100\u011f\78\2\2\u0101",
                        "\u0103\7\65\2\2\u0102\u0104\5\26\f\2\u0103\u0102\3\2\2\2\u0103\u0104",
                        "\3\2\2\2\u0104\u010d\3\2\2\2\u0105\u0106\78\2\2\u0106\u0109\5\26\f",
                        "\2\u0107\u0108\7<\2\2\u0108\u010a\5d\63\2\u0109\u0107\3\2\2\2\u0109",
                        "\u010a\3\2\2\2\u010a\u010c\3\2\2\2\u010b\u0105\3\2\2\2\u010c\u010f",
                        "\3\2\2\2\u010d\u010b\3\2\2\2\u010d\u010e\3\2\2\2\u010e\u0118\3\2\2",
                        "\2\u010f\u010d\3\2\2\2\u0110\u0116\78\2\2\u0111\u0112\7;\2\2\u0112",
                        "\u0114\5\26\f\2\u0113\u0115\78\2\2\u0114\u0113\3\2\2\2\u0114\u0115",
                        "\3\2\2\2\u0115\u0117\3\2\2\2\u0116\u0111\3\2\2\2\u0116\u0117\3\2\2",
                        "\2\u0117\u0119\3\2\2\2\u0118\u0110\3\2\2\2\u0118\u0119\3\2\2\2\u0119",
                        "\u0120\3\2\2\2\u011a\u011b\7;\2\2\u011b\u011d\5\26\f\2\u011c\u011e",
                        "\78\2\2\u011d\u011c\3\2\2\2\u011d\u011e\3\2\2\2\u011e\u0120\3\2\2\2",
                        "\u011f\u0101\3\2\2\2\u011f\u011a\3\2\2\2\u011f\u0120\3\2\2\2\u0120",
                        "\u0122\3\2\2\2\u0121\u0100\3\2\2\2\u0121\u0122\3\2\2\2\u0122\u0142",
                        "\3\2\2\2\u0123\u0125\7\65\2\2\u0124\u0126\5\26\f\2\u0125\u0124\3\2",
                        "\2\2\u0125\u0126\3\2\2\2\u0126\u012f\3\2\2\2\u0127\u0128\78\2\2\u0128",
                        "\u012b\5\26\f\2\u0129\u012a\7<\2\2\u012a\u012c\5d\63\2\u012b\u0129",
                        "\3\2\2\2\u012b\u012c\3\2\2\2\u012c\u012e\3\2\2\2\u012d\u0127\3\2\2",
                        "\2\u012e\u0131\3\2\2\2\u012f\u012d\3\2\2\2\u012f\u0130\3\2\2\2\u0130",
                        "\u013a\3\2\2\2\u0131\u012f\3\2\2\2\u0132\u0138\78\2\2\u0133\u0134\7",
                        ";\2\2\u0134\u0136\5\26\f\2\u0135\u0137\78\2\2\u0136\u0135\3\2\2\2\u0136",
                        "\u0137\3\2\2\2\u0137\u0139\3\2\2\2\u0138\u0133\3\2\2\2\u0138\u0139",
                        "\3\2\2\2\u0139\u013b\3\2\2\2\u013a\u0132\3\2\2\2\u013a\u013b\3\2\2",
                        "\2\u013b\u0142\3\2\2\2\u013c\u013d\7;\2\2\u013d\u013f\5\26\f\2\u013e",
                        "\u0140\78\2\2\u013f\u013e\3\2\2\2\u013f\u0140\3\2\2\2\u0140\u0142\3",
                        "\2\2\2\u0141\u00f0\3\2\2\2\u0141\u0123\3\2\2\2\u0141\u013c\3\2\2\2",
                        "\u0142\25\3\2\2\2\u0143\u0146\7*\2\2\u0144\u0145\79\2\2\u0145\u0147",
                        "\5d\63\2\u0146\u0144\3\2\2\2\u0146\u0147\3\2\2\2\u0147\27\3\2\2\2\u0148",
                        "\u014b\5\32\16\2\u0149\u014a\7<\2\2\u014a\u014c\5d\63\2\u014b\u0149",
                        "\3\2\2\2\u014b\u014c\3\2\2\2\u014c\u0155\3\2\2\2\u014d\u014e\78\2\2",
                        "\u014e\u0151\5\32\16\2\u014f\u0150\7<\2\2\u0150\u0152\5d\63\2\u0151",
                        "\u014f\3\2\2\2\u0151\u0152\3\2\2\2\u0152\u0154\3\2\2\2\u0153\u014d",
                        "\3\2\2\2\u0154\u0157\3\2\2\2\u0155\u0153\3\2\2\2\u0155\u0156\3\2\2",
                        "\2\u0156\u0179\3\2\2\2\u0157\u0155\3\2\2\2\u0158\u0177\78\2\2\u0159",
                        "\u015b\7\65\2\2\u015a\u015c\5\32\16\2\u015b\u015a\3\2\2\2\u015b\u015c",
                        "\3\2\2\2\u015c\u0165\3\2\2\2\u015d\u015e\78\2\2\u015e\u0161\5\32\16",
                        "\2\u015f\u0160\7<\2\2\u0160\u0162\5d\63\2\u0161\u015f\3\2\2\2\u0161",
                        "\u0162\3\2\2\2\u0162\u0164\3\2\2\2\u0163\u015d\3\2\2\2\u0164\u0167",
                        "\3\2\2\2\u0165\u0163\3\2\2\2\u0165\u0166\3\2\2\2\u0166\u0170\3\2\2",
                        "\2\u0167\u0165\3\2\2\2\u0168\u016e\78\2\2\u0169\u016a\7;\2\2\u016a",
                        "\u016c\5\32\16\2\u016b\u016d\78\2\2\u016c\u016b\3\2\2\2\u016c\u016d",
                        "\3\2\2\2\u016d\u016f\3\2\2\2\u016e\u0169\3\2\2\2\u016e\u016f\3\2\2",
                        "\2\u016f\u0171\3\2\2\2\u0170\u0168\3\2\2\2\u0170\u0171\3\2\2\2\u0171",
                        "\u0178\3\2\2\2\u0172\u0173\7;\2\2\u0173\u0175\5\32\16\2\u0174\u0176",
                        "\78\2\2\u0175\u0174\3\2\2\2\u0175\u0176\3\2\2\2\u0176\u0178\3\2\2\2",
                        "\u0177\u0159\3\2\2\2\u0177\u0172\3\2\2\2\u0177\u0178\3\2\2\2\u0178",
                        "\u017a\3\2\2\2\u0179\u0158\3\2\2\2\u0179\u017a\3\2\2\2\u017a\u019a",
                        "\3\2\2\2\u017b\u017d\7\65\2\2\u017c\u017e\5\32\16\2\u017d\u017c\3\2",
                        "\2\2\u017d\u017e\3\2\2\2\u017e\u0187\3\2\2\2\u017f\u0180\78\2\2\u0180",
                        "\u0183\5\32\16\2\u0181\u0182\7<\2\2\u0182\u0184\5d\63\2\u0183\u0181",
                        "\3\2\2\2\u0183\u0184\3\2\2\2\u0184\u0186\3\2\2\2\u0185\u017f\3\2\2",
                        "\2\u0186\u0189\3\2\2\2\u0187\u0185\3\2\2\2\u0187\u0188\3\2\2\2\u0188",
                        "\u0192\3\2\2\2\u0189\u0187\3\2\2\2\u018a\u0190\78\2\2\u018b\u018c\7",
                        ";\2\2\u018c\u018e\5\32\16\2\u018d\u018f\78\2\2\u018e\u018d\3\2\2\2",
                        "\u018e\u018f\3\2\2\2\u018f\u0191\3\2\2\2\u0190\u018b\3\2\2\2\u0190",
                        "\u0191\3\2\2\2\u0191\u0193\3\2\2\2\u0192\u018a\3\2\2\2\u0192\u0193",
                        "\3\2\2\2\u0193\u019a\3\2\2\2\u0194\u0195\7;\2\2\u0195\u0197\5\32\16",
                        "\2\u0196\u0198\78\2\2\u0197\u0196\3\2\2\2\u0197\u0198\3\2\2\2\u0198",
                        "\u019a\3\2\2\2\u0199\u0148\3\2\2\2\u0199\u017b\3\2\2\2\u0199\u0194",
                        "\3\2\2\2\u019a\31\3\2\2\2\u019b\u019c\7*\2\2\u019c\33\3\2\2\2\u019d",
                        "\u01a0\5\36\20\2\u019e\u01a0\5P)\2\u019f\u019d\3\2\2\2\u019f\u019e",
                        "\3\2\2\2\u01a0\35\3\2\2\2\u01a1\u01a6\5 \21\2\u01a2\u01a3\7:\2\2\u01a3",
                        "\u01a5\5 \21\2\u01a4\u01a2\3\2\2\2\u01a5\u01a8\3\2\2\2\u01a6\u01a4",
                        "\3\2\2\2\u01a6\u01a7\3\2\2\2\u01a7\u01aa\3\2\2\2\u01a8\u01a6\3\2\2",
                        "\2\u01a9\u01ab\7:\2\2\u01aa\u01a9\3\2\2\2\u01aa\u01ab\3\2\2\2\u01ab",
                        "\u01ac\3\2\2\2\u01ac\u01ad\7)\2\2\u01ad\37\3\2\2\2\u01ae\u01b7\5\"",
                        "\22\2\u01af\u01b7\5*\26\2\u01b0\u01b7\5,\27\2\u01b1\u01b7\5.\30\2\u01b2",
                        "\u01b7\5:\36\2\u01b3\u01b7\5J&\2\u01b4\u01b7\5L\'\2\u01b5\u01b7\5N",
                        "(\2\u01b6\u01ae\3\2\2\2\u01b6\u01af\3\2\2\2\u01b6\u01b0\3\2\2\2\u01b6",
                        "\u01b1\3\2\2\2\u01b6\u01b2\3\2\2\2\u01b6\u01b3\3\2\2\2\u01b6\u01b4",
                        "\3\2\2\2\u01b6\u01b5\3\2\2\2\u01b7!\3\2\2\2\u01b8\u01c9\5&\24\2\u01b9",
                        "\u01ca\5$\23\2\u01ba\u01bd\5(\25\2\u01bb\u01be\5\u00aaV\2\u01bc\u01be",
                        "\5\u0098M\2\u01bd\u01bb\3\2\2\2\u01bd\u01bc\3\2\2\2\u01be\u01ca\3\2",
                        "\2\2\u01bf\u01c2\7<\2\2\u01c0\u01c3\5\u00aaV\2\u01c1\u01c3\5&\24\2",
                        "\u01c2\u01c0\3\2\2\2\u01c2\u01c1\3\2\2\2\u01c3\u01c5\3\2\2\2\u01c4",
                        "\u01bf\3\2\2\2\u01c5\u01c8\3\2\2\2\u01c6\u01c4\3\2\2\2\u01c6\u01c7",
                        "\3\2\2\2\u01c7\u01ca\3\2\2\2\u01c8\u01c6\3\2\2\2\u01c9\u01b9\3\2\2",
                        "\2\u01c9\u01ba\3\2\2\2\u01c9\u01c6\3\2\2\2\u01ca#\3\2\2\2\u01cb\u01cc",
                        "\79\2\2\u01cc\u01cf\5d\63\2\u01cd\u01ce\7<\2\2\u01ce\u01d0\5d\63\2",
                        "\u01cf\u01cd\3\2\2\2\u01cf\u01d0\3\2\2\2\u01d0%\3\2\2\2\u01d1\u01d4",
                        "\5d\63\2\u01d2\u01d4\5v<\2\u01d3\u01d1\3\2\2\2\u01d3\u01d2\3\2\2\2",
                        "\u01d4\u01dc\3\2\2\2\u01d5\u01d8\78\2\2\u01d6\u01d9\5d\63\2\u01d7\u01d9",
                        "\5v<\2\u01d8\u01d6\3\2\2\2\u01d8\u01d7\3\2\2\2\u01d9\u01db\3\2\2\2",
                        "\u01da\u01d5\3\2\2\2\u01db\u01de\3\2\2\2\u01dc\u01da\3\2\2\2\u01dc",
                        "\u01dd\3\2\2\2\u01dd\u01e0\3\2\2\2\u01de\u01dc\3\2\2\2\u01df\u01e1",
                        "\78\2\2\u01e0\u01df\3\2\2\2\u01e0\u01e1\3\2\2\2\u01e1\'\3\2\2\2\u01e2",
                        "\u01e3\t\2\2\2\u01e3)\3\2\2\2\u01e4\u01e5\7#\2\2\u01e5\u01e6\5\u0096",
                        "L\2\u01e6+\3\2\2\2\u01e7\u01e8\7$\2\2\u01e8-\3\2\2\2\u01e9\u01ef\5",
                        "\60\31\2\u01ea\u01ef\5\62\32\2\u01eb\u01ef\5\64\33\2\u01ec\u01ef\5",
                        "8\35\2\u01ed\u01ef\5\66\34\2\u01ee\u01e9\3\2\2\2\u01ee\u01ea\3\2\2",
                        "\2\u01ee\u01eb\3\2\2\2\u01ee\u01ec\3\2\2\2\u01ee\u01ed\3\2\2\2\u01ef",
                        "/\3\2\2\2\u01f0\u01f1\7&\2\2\u01f1\61\3\2\2\2\u01f2\u01f3\7%\2\2\u01f3",
                        "\63\3\2\2\2\u01f4\u01f6\7\7\2\2\u01f5\u01f7\5\u0098M\2\u01f6\u01f5",
                        "\3\2\2\2\u01f6\u01f7\3\2\2\2\u01f7\65\3\2\2\2\u01f8\u01f9\5\u00aaV",
                        "\2\u01f9\67\3\2\2\2\u01fa\u0200\7\b\2\2\u01fb\u01fe\5d\63\2\u01fc\u01fd",
                        "\7\t\2\2\u01fd\u01ff\5d\63\2\u01fe\u01fc\3\2\2\2\u01fe\u01ff\3\2\2",
                        "\2\u01ff\u0201\3\2\2\2\u0200\u01fb\3\2\2\2\u0200\u0201\3\2\2\2\u0201",
                        "9\3\2\2\2\u0202\u0205\5<\37\2\u0203\u0205\5> \2\u0204\u0202\3\2\2\2",
                        "\u0204\u0203\3\2\2\2\u0205;\3\2\2\2\u0206\u0207\7\n\2\2\u0207\u0208",
                        "\5F$\2\u0208=\3\2\2\2\u0209\u0216\7\t\2\2\u020a\u020c\t\3\2\2\u020b",
                        "\u020a\3\2\2\2\u020c\u020f\3\2\2\2\u020d\u020b\3\2\2\2\u020d\u020e",
                        "\3\2\2\2\u020e\u0210\3\2\2\2\u020f\u020d\3\2\2\2\u0210\u0217\5H%\2",
                        "\u0211\u0213\t\3\2\2\u0212\u0211\3\2\2\2\u0213\u0214\3\2\2\2\u0214",
                        "\u0212\3\2\2\2\u0214\u0215\3\2\2\2\u0215\u0217\3\2\2\2\u0216\u020d",
                        "\3\2\2\2\u0216\u0212\3\2\2\2\u0217\u0218\3\2\2\2\u0218\u021f\7\n\2",
                        "\2\u0219\u0220\7\65\2\2\u021a\u021b\7\66\2\2\u021b\u021c\5D#\2\u021c",
                        "\u021d\7\67\2\2\u021d\u0220\3\2\2\2\u021e\u0220\5D#\2\u021f\u0219\3",
                        "\2\2\2\u021f\u021a\3\2\2\2\u021f\u021e\3\2\2\2\u0220?\3\2\2\2\u0221",
                        "\u0224\7*\2\2\u0222\u0223\7\13\2\2\u0223\u0225\7*\2\2\u0224\u0222\3",
                        "\2\2\2\u0224\u0225\3\2\2\2\u0225A\3\2\2\2\u0226\u0229\5H%\2\u0227\u0228",
                        "\7\13\2\2\u0228\u022a\7*\2\2\u0229\u0227\3\2\2\2\u0229\u022a\3\2\2",
                        "\2\u022aC\3\2\2\2\u022b\u0230\5@!\2\u022c\u022d\78\2\2\u022d\u022f",
                        "\5@!\2\u022e\u022c\3\2\2\2\u022f\u0232\3\2\2\2\u0230\u022e\3\2\2\2",
                        "\u0230\u0231\3\2\2\2\u0231\u0234\3\2\2\2\u0232\u0230\3\2\2\2\u0233",
                        "\u0235\78\2\2\u0234\u0233\3\2\2\2\u0234\u0235\3\2\2\2\u0235E\3\2\2",
                        "\2\u0236\u023b\5B\"\2\u0237\u0238\78\2\2\u0238\u023a\5B\"\2\u0239\u0237",
                        "\3\2\2\2\u023a\u023d\3\2\2\2\u023b\u0239\3\2\2\2\u023b\u023c\3\2\2",
                        "\2\u023cG\3\2\2\2\u023d\u023b\3\2\2\2\u023e\u0243\7*\2\2\u023f\u0240",
                        "\7\63\2\2\u0240\u0242\7*\2\2\u0241\u023f\3\2\2\2\u0242\u0245\3\2\2",
                        "\2\u0243\u0241\3\2\2\2\u0243\u0244\3\2\2\2\u0244I\3\2\2\2\u0245\u0243",
                        "\3\2\2\2\u0246\u0247\7\f\2\2\u0247\u024c\7*\2\2\u0248\u0249\78\2\2",
                        "\u0249\u024b\7*\2\2\u024a\u0248\3\2\2\2\u024b\u024e\3\2\2\2\u024c\u024a",
                        "\3\2\2\2\u024c\u024d\3\2\2\2\u024dK\3\2\2\2\u024e\u024c\3\2\2\2\u024f",
                        "\u0250\7\r\2\2\u0250\u0255\7*\2\2\u0251\u0252\78\2\2\u0252\u0254\7",
                        "*\2\2\u0253\u0251\3\2\2\2\u0254\u0257\3\2\2\2\u0255\u0253\3\2\2\2\u0255",
                        "\u0256\3\2\2\2\u0256M\3\2\2\2\u0257\u0255\3\2\2\2\u0258\u0259\7\16",
                        "\2\2\u0259\u025c\5d\63\2\u025a\u025b\78\2\2\u025b\u025d\5d\63\2\u025c",
                        "\u025a\3\2\2\2\u025c\u025d\3\2\2\2\u025dO\3\2\2\2\u025e\u0268\5T+\2",
                        "\u025f\u0268\5V,\2\u0260\u0268\5X-\2\u0261\u0268\5Z.\2\u0262\u0268",
                        "\5\\/\2\u0263\u0268\5\20\t\2\u0264\u0268\5\u009cO\2\u0265\u0268\5\f",
                        "\7\2\u0266\u0268\5R*\2\u0267\u025e\3\2\2\2\u0267\u025f\3\2\2\2\u0267",
                        "\u0260\3\2\2\2\u0267\u0261\3\2\2\2\u0267\u0262\3\2\2\2\u0267\u0263",
                        "\3\2\2\2\u0267\u0264\3\2\2\2\u0267\u0265\3\2\2\2\u0267\u0266\3\2\2",
                        "\2\u0268Q\3\2\2\2\u0269\u026d\7\'\2\2\u026a\u026e\5\20\t\2\u026b\u026e",
                        "\5\\/\2\u026c\u026e\5X-\2\u026d\u026a\3\2\2\2\u026d\u026b\3\2\2\2\u026d",
                        "\u026c\3\2\2\2\u026eS\3\2\2\2\u026f\u0270\7\17\2\2\u0270\u0271\5d\63",
                        "\2\u0271\u0272\79\2\2\u0272\u027a\5b\62\2\u0273\u0274\7\20\2\2\u0274",
                        "\u0275\5d\63\2\u0275\u0276\79\2\2\u0276\u0277\5b\62\2\u0277\u0279\3",
                        "\2\2\2\u0278\u0273\3\2\2\2\u0279\u027c\3\2\2\2\u027a\u0278\3\2\2\2",
                        "\u027a\u027b\3\2\2\2\u027b\u0280\3\2\2\2\u027c\u027a\3\2\2\2\u027d",
                        "\u027e\7\21\2\2\u027e\u027f\79\2\2\u027f\u0281\5b\62\2\u0280\u027d",
                        "\3\2\2\2\u0280\u0281\3\2\2\2\u0281U\3\2\2\2\u0282\u0283\7\22\2\2\u0283",
                        "\u0284\5d\63\2\u0284\u0285\79\2\2\u0285\u0289\5b\62\2\u0286\u0287\7",
                        "\21\2\2\u0287\u0288\79\2\2\u0288\u028a\5b\62\2\u0289\u0286\3\2\2\2",
                        "\u0289\u028a\3\2\2\2\u028aW\3\2\2\2\u028b\u028c\7\23\2\2\u028c\u028d",
                        "\5\u0096L\2\u028d\u028e\7\24\2\2\u028e\u028f\5\u0098M\2\u028f\u0290",
                        "\79\2\2\u0290\u0294\5b\62\2\u0291\u0292\7\21\2\2\u0292\u0293\79\2\2",
                        "\u0293\u0295\5b\62\2\u0294\u0291\3\2\2\2\u0294\u0295\3\2\2\2\u0295",
                        "Y\3\2\2\2\u0296\u0297\7\25\2\2\u0297\u0298\79\2\2\u0298\u02ae\5b\62",
                        "\2\u0299\u029a\5`\61\2\u029a\u029b\79\2\2\u029b\u029c\5b\62\2\u029c",
                        "\u029e\3\2\2\2\u029d\u0299\3\2\2\2\u029e\u029f\3\2\2\2\u029f\u029d",
                        "\3\2\2\2\u029f\u02a0\3\2\2\2\u02a0\u02a4\3\2\2\2\u02a1\u02a2\7\21\2",
                        "\2\u02a2\u02a3\79\2\2\u02a3\u02a5\5b\62\2\u02a4\u02a1\3\2\2\2\u02a4",
                        "\u02a5\3\2\2\2\u02a5\u02a9\3\2\2\2\u02a6\u02a7\7\26\2\2\u02a7\u02a8",
                        "\79\2\2\u02a8\u02aa\5b\62\2\u02a9\u02a6\3\2\2\2\u02a9\u02aa\3\2\2\2",
                        "\u02aa\u02af\3\2\2\2\u02ab\u02ac\7\26\2\2\u02ac\u02ad\79\2\2\u02ad",
                        "\u02af\5b\62\2\u02ae\u029d\3\2\2\2\u02ae\u02ab\3\2\2\2\u02af[\3\2\2",
                        "\2\u02b0\u02b1\7\27\2\2\u02b1\u02b6\5^\60\2\u02b2\u02b3\78\2\2\u02b3",
                        "\u02b5\5^\60\2\u02b4\u02b2\3\2\2\2\u02b5\u02b8\3\2\2\2\u02b6\u02b4",
                        "\3\2\2\2\u02b6\u02b7\3\2\2\2\u02b7\u02b9\3\2\2\2\u02b8\u02b6\3\2\2",
                        "\2\u02b9\u02ba\79\2\2\u02ba\u02bb\5b\62\2\u02bb]\3\2\2\2\u02bc\u02bf",
                        "\5d\63\2\u02bd\u02be\7\13\2\2\u02be\u02c0\5x=\2\u02bf\u02bd\3\2\2\2",
                        "\u02bf\u02c0\3\2\2\2\u02c0_\3\2\2\2\u02c1\u02c7\7\30\2\2\u02c2\u02c5",
                        "\5d\63\2\u02c3\u02c4\7\13\2\2\u02c4\u02c6\7*\2\2\u02c5\u02c3\3\2\2",
                        "\2\u02c5\u02c6\3\2\2\2\u02c6\u02c8\3\2\2\2\u02c7\u02c2\3\2\2\2\u02c7",
                        "\u02c8\3\2\2\2\u02c8a\3\2\2\2\u02c9\u02d4\5\36\20\2\u02ca\u02cb\7)",
                        "\2\2\u02cb\u02cd\7d\2\2\u02cc\u02ce\5\34\17\2\u02cd\u02cc\3\2\2\2\u02ce",
                        "\u02cf\3\2\2\2\u02cf\u02cd\3\2\2\2\u02cf\u02d0\3\2\2\2\u02d0\u02d1",
                        "\3\2\2\2\u02d1\u02d2\7e\2\2\u02d2\u02d4\3\2\2\2\u02d3\u02c9\3\2\2\2",
                        "\u02d3\u02ca\3\2\2\2\u02d4c\3\2\2\2\u02d5\u02db\5l\67\2\u02d6\u02d7",
                        "\7\17\2\2\u02d7\u02d8\5l\67\2\u02d8\u02d9\7\21\2\2\u02d9\u02da\5d\63",
                        "\2\u02da\u02dc\3\2\2\2\u02db\u02d6\3\2\2\2\u02db\u02dc\3\2\2\2\u02dc",
                        "\u02df\3\2\2\2\u02dd\u02df\5h\65\2\u02de\u02d5\3\2\2\2\u02de\u02dd",
                        "\3\2\2\2\u02dfe\3\2\2\2\u02e0\u02e3\5l\67\2\u02e1\u02e3\5j\66\2\u02e2",
                        "\u02e0\3\2\2\2\u02e2\u02e1\3\2\2\2\u02e3g\3\2\2\2\u02e4\u02e6\7\31",
                        "\2\2\u02e5\u02e7\5\30\r\2\u02e6\u02e5\3\2\2\2\u02e6\u02e7\3\2\2\2\u02e7",
                        "\u02e8\3\2\2\2\u02e8\u02e9\79\2\2\u02e9\u02ea\5d\63\2\u02eai\3\2\2",
                        "\2\u02eb\u02ed\7\31\2\2\u02ec\u02ee\5\30\r\2\u02ed\u02ec\3\2\2\2\u02ed",
                        "\u02ee\3\2\2\2\u02ee\u02ef\3\2\2\2\u02ef\u02f0\79\2\2\u02f0\u02f1\5",
                        "f\64\2\u02f1k\3\2\2\2\u02f2\u02f7\5n8\2\u02f3\u02f4\7\32\2\2\u02f4",
                        "\u02f6\5n8\2\u02f5\u02f3\3\2\2\2\u02f6\u02f9\3\2\2\2\u02f7\u02f5\3",
                        "\2\2\2\u02f7\u02f8\3\2\2\2\u02f8m\3\2\2\2\u02f9\u02f7\3\2\2\2\u02fa",
                        "\u02ff\5p9\2\u02fb\u02fc\7\33\2\2\u02fc\u02fe\5p9\2\u02fd\u02fb\3\2",
                        "\2\2\u02fe\u0301\3\2\2\2\u02ff\u02fd\3\2\2\2\u02ff\u0300\3\2\2\2\u0300",
                        "o\3\2\2\2\u0301\u02ff\3\2\2\2\u0302\u0303\7\34\2\2\u0303\u0306\5p9",
                        "\2\u0304\u0306\5r:\2\u0305\u0302\3\2\2\2\u0305\u0304\3\2\2\2\u0306",
                        "q\3\2\2\2\u0307\u030d\5x=\2\u0308\u0309\5t;\2\u0309\u030a\5x=\2\u030a",
                        "\u030c\3\2\2\2\u030b\u0308\3\2\2\2\u030c\u030f\3\2\2\2\u030d\u030b",
                        "\3\2\2\2\u030d\u030e\3\2\2\2\u030es\3\2\2\2\u030f\u030d\3\2\2\2\u0310",
                        "\u031e\7L\2\2\u0311\u031e\7M\2\2\u0312\u031e\7N\2\2\u0313\u031e\7O",
                        "\2\2\u0314\u031e\7P\2\2\u0315\u031e\7Q\2\2\u0316\u031e\7R\2\2\u0317",
                        "\u031e\7\24\2\2\u0318\u0319\7\34\2\2\u0319\u031e\7\24\2\2\u031a\u031e",
                        "\7\35\2\2\u031b\u031c\7\35\2\2\u031c\u031e\7\34\2\2\u031d\u0310\3\2",
                        "\2\2\u031d\u0311\3\2\2\2\u031d\u0312\3\2\2\2\u031d\u0313\3\2\2\2\u031d",
                        "\u0314\3\2\2\2\u031d\u0315\3\2\2\2\u031d\u0316\3\2\2\2\u031d\u0317",
                        "\3\2\2\2\u031d\u0318\3\2\2\2\u031d\u031a\3\2\2\2\u031d\u031b\3\2\2",
                        "\2\u031eu\3\2\2\2\u031f\u0320\7\65\2\2\u0320\u0321\5x=\2\u0321w\3\2",
                        "\2\2\u0322\u0327\5z>\2\u0323\u0324\7?\2\2\u0324\u0326\5z>\2\u0325\u0323",
                        "\3\2\2\2\u0326\u0329\3\2\2\2\u0327\u0325\3\2\2\2\u0327\u0328\3\2\2",
                        "\2\u0328y\3\2\2\2\u0329\u0327\3\2\2\2\u032a\u032f\5|?\2\u032b\u032c",
                        "\7@\2\2\u032c\u032e\5|?\2\u032d\u032b\3\2\2\2\u032e\u0331\3\2\2\2\u032f",
                        "\u032d\3\2\2\2\u032f\u0330\3\2\2\2\u0330{\3\2\2\2\u0331\u032f\3\2\2",
                        "\2\u0332\u0337\5~@\2\u0333\u0334\7A\2\2\u0334\u0336\5~@\2\u0335\u0333",
                        "\3\2\2\2\u0336\u0339\3\2\2\2\u0337\u0335\3\2\2\2\u0337\u0338\3\2\2",
                        "\2\u0338}\3\2\2\2\u0339\u0337\3\2\2\2\u033a\u033f\5\u0080A\2\u033b",
                        "\u033c\t\4\2\2\u033c\u033e\5\u0080A\2\u033d\u033b\3\2\2\2\u033e\u0341",
                        "\3\2\2\2\u033f\u033d\3\2\2\2\u033f\u0340\3\2\2\2\u0340\177\3\2\2\2",
                        "\u0341\u033f\3\2\2\2\u0342\u0347\5\u0082B\2\u0343\u0344\t\5\2\2\u0344",
                        "\u0346\5\u0082B\2\u0345\u0343\3\2\2\2\u0346\u0349\3\2\2\2\u0347\u0345",
                        "\3\2\2\2\u0347\u0348\3\2\2\2\u0348\u0081\3\2\2\2\u0349\u0347\3\2\2",
                        "\2\u034a\u034f\5\u0084C\2\u034b\u034c\t\6\2\2\u034c\u034e\5\u0084C",
                        "\2\u034d\u034b\3\2\2\2\u034e\u0351\3\2\2\2\u034f\u034d\3\2\2\2\u034f",
                        "\u0350\3\2\2\2\u0350\u0083\3\2\2\2\u0351\u034f\3\2\2\2\u0352\u0353",
                        "\t\7\2\2\u0353\u0356\5\u0084C\2\u0354\u0356\5\u0086D\2\u0355\u0352",
                        "\3\2\2\2\u0355\u0354\3\2\2\2\u0356\u0085\3\2\2\2\u0357\u035a\5\u0088",
                        "E\2\u0358\u0359\7;\2\2\u0359\u035b\5\u0084C\2\u035a\u0358\3\2\2\2\u035a",
                        "\u035b\3\2\2\2\u035b\u0087\3\2\2\2\u035c\u035e\7(\2\2\u035d\u035c\3",
                        "\2\2\2\u035d\u035e\3\2\2\2\u035e\u035f\3\2\2\2\u035f\u0363\5\u008a",
                        "F\2\u0360\u0362\5\u008eH\2\u0361\u0360\3\2\2\2\u0362\u0365\3\2\2\2",
                        "\u0363\u0361\3\2\2\2\u0363\u0364\3\2\2\2\u0364\u0089\3\2\2\2\u0365",
                        "\u0363\3\2\2\2\u0366\u0369\7\66\2\2\u0367\u036a\5\u00aaV\2\u0368\u036a",
                        "\5\u008cG\2\u0369\u0367\3\2\2\2\u0369\u0368\3\2\2\2\u0369\u036a\3\2",
                        "\2\2\u036a\u036b\3\2\2\2\u036b\u0382\7\67\2\2\u036c\u036e\7=\2\2\u036d",
                        "\u036f\5\u008cG\2\u036e\u036d\3\2\2\2\u036e\u036f\3\2\2\2\u036f\u0370",
                        "\3\2\2\2\u0370\u0382\7>\2\2\u0371\u0373\7J\2\2\u0372\u0374\5\u009a",
                        "N\2\u0373\u0372\3\2\2\2\u0373\u0374\3\2\2\2\u0374\u0375\3\2\2\2\u0375",
                        "\u0382\7K\2\2\u0376\u0382\7*\2\2\u0377\u0382\7\4\2\2\u0378\u037a\7",
                        "\3\2\2\u0379\u0378\3\2\2\2\u037a\u037b\3\2\2\2\u037b\u0379\3\2\2\2",
                        "\u037b\u037c\3\2\2\2\u037c\u0382\3\2\2\2\u037d\u0382\7\64\2\2\u037e",
                        "\u0382\7\36\2\2\u037f\u0382\7\37\2\2\u0380\u0382\7 \2\2\u0381\u0366",
                        "\3\2\2\2\u0381\u036c\3\2\2\2\u0381\u0371\3\2\2\2\u0381\u0376\3\2\2",
                        "\2\u0381\u0377\3\2\2\2\u0381\u0379\3\2\2\2\u0381\u037d\3\2\2\2\u0381",
                        "\u037e\3\2\2\2\u0381\u037f\3\2\2\2\u0381\u0380\3\2\2\2\u0382\u008b",
                        "\3\2\2\2\u0383\u0386\5d\63\2\u0384\u0386\5v<\2\u0385\u0383\3\2\2\2",
                        "\u0385\u0384\3\2\2\2\u0386\u0395\3\2\2\2\u0387\u0396\5\u00a4S\2\u0388",
                        "\u038b\78\2\2\u0389\u038c\5d\63\2\u038a\u038c\5v<\2\u038b\u0389\3\2",
                        "\2\2\u038b\u038a\3\2\2\2\u038c\u038e\3\2\2\2\u038d\u0388\3\2\2\2\u038e",
                        "\u0391\3\2\2\2\u038f\u038d\3\2\2\2\u038f\u0390\3\2\2\2\u0390\u0393",
                        "\3\2\2\2\u0391\u038f\3\2\2\2\u0392\u0394\78\2\2\u0393\u0392\3\2\2\2",
                        "\u0393\u0394\3\2\2\2\u0394\u0396\3\2\2\2\u0395\u0387\3\2\2\2\u0395",
                        "\u038f\3\2\2\2\u0396\u008d\3\2\2\2\u0397\u0399\7\66\2\2\u0398\u039a",
                        "\5\u009eP\2\u0399\u0398\3\2\2\2\u0399\u039a\3\2\2\2\u039a\u039b\3\2",
                        "\2\2\u039b\u03a3\7\67\2\2\u039c\u039d\7=\2\2\u039d\u039e\5\u0090I\2",
                        "\u039e\u039f\7>\2\2\u039f\u03a3\3\2\2\2\u03a0\u03a1\7\63\2\2\u03a1",
                        "\u03a3\7*\2\2\u03a2\u0397\3\2\2\2\u03a2\u039c\3\2\2\2\u03a2\u03a0\3",
                        "\2\2\2\u03a3\u008f\3\2\2\2\u03a4\u03a9\5\u0092J\2\u03a5\u03a6\78\2",
                        "\2\u03a6\u03a8\5\u0092J\2\u03a7\u03a5\3\2\2\2\u03a8\u03ab\3\2\2\2\u03a9",
                        "\u03a7\3\2\2\2\u03a9\u03aa\3\2\2\2\u03aa\u03ad\3\2\2\2\u03ab\u03a9",
                        "\3\2\2\2\u03ac\u03ae\78\2\2\u03ad\u03ac\3\2\2\2\u03ad\u03ae\3\2\2\2",
                        "\u03ae\u0091\3\2\2\2\u03af\u03bb\5d\63\2\u03b0\u03b2\5d\63\2\u03b1",
                        "\u03b0\3\2\2\2\u03b1\u03b2\3\2\2\2\u03b2\u03b3\3\2\2\2\u03b3\u03b5",
                        "\79\2\2\u03b4\u03b6\5d\63\2\u03b5\u03b4\3\2\2\2\u03b5\u03b6\3\2\2\2",
                        "\u03b6\u03b8\3\2\2\2\u03b7\u03b9\5\u0094K\2\u03b8\u03b7\3\2\2\2\u03b8",
                        "\u03b9\3\2\2\2\u03b9\u03bb\3\2\2\2\u03ba\u03af\3\2\2\2\u03ba\u03b1",
                        "\3\2\2\2\u03bb\u0093\3\2\2\2\u03bc\u03be\79\2\2\u03bd\u03bf\5d\63\2",
                        "\u03be\u03bd\3\2\2\2\u03be\u03bf\3\2\2\2\u03bf\u0095\3\2\2\2\u03c0",
                        "\u03c3\5x=\2\u03c1\u03c3\5v<\2\u03c2\u03c0\3\2\2\2\u03c2\u03c1\3\2",
                        "\2\2\u03c3\u03cb\3\2\2\2\u03c4\u03c7\78\2\2\u03c5\u03c8\5x=\2\u03c6",
                        "\u03c8\5v<\2\u03c7\u03c5\3\2\2\2\u03c7\u03c6\3\2\2\2\u03c8\u03ca\3",
                        "\2\2\2\u03c9\u03c4\3\2\2\2\u03ca\u03cd\3\2\2\2\u03cb\u03c9\3\2\2\2",
                        "\u03cb\u03cc\3\2\2\2\u03cc\u03cf\3\2\2\2\u03cd\u03cb\3\2\2\2\u03ce",
                        "\u03d0\78\2\2\u03cf\u03ce\3\2\2\2\u03cf\u03d0\3\2\2\2\u03d0\u0097\3",
                        "\2\2\2\u03d1\u03d6\5d\63\2\u03d2\u03d3\78\2\2\u03d3\u03d5\5d\63\2\u03d4",
                        "\u03d2\3\2\2\2\u03d5\u03d8\3\2\2\2\u03d6\u03d4\3\2\2\2\u03d6\u03d7",
                        "\3\2\2\2\u03d7\u03da\3\2\2\2\u03d8\u03d6\3\2\2\2\u03d9\u03db\78\2\2",
                        "\u03da\u03d9\3\2\2\2\u03da\u03db\3\2\2\2\u03db\u0099\3\2\2\2\u03dc",
                        "\u03dd\5d\63\2\u03dd\u03de\79\2\2\u03de\u03df\5d\63\2\u03df\u03e3\3",
                        "\2\2\2\u03e0\u03e1\7;\2\2\u03e1\u03e3\5x=\2\u03e2\u03dc\3\2\2\2\u03e2",
                        "\u03e0\3\2\2\2\u03e3\u03f6\3\2\2\2\u03e4\u03f7\5\u00a4S\2\u03e5\u03ec",
                        "\78\2\2\u03e6\u03e7\5d\63\2\u03e7\u03e8\79\2\2\u03e8\u03e9\5d\63\2",
                        "\u03e9\u03ed\3\2\2\2\u03ea\u03eb\7;\2\2\u03eb\u03ed\5x=\2\u03ec\u03e6",
                        "\3\2\2\2\u03ec\u03ea\3\2\2\2\u03ed\u03ef\3\2\2\2\u03ee\u03e5\3\2\2",
                        "\2\u03ef\u03f2\3\2\2\2\u03f0\u03ee\3\2\2\2\u03f0\u03f1\3\2\2\2\u03f1",
                        "\u03f4\3\2\2\2\u03f2\u03f0\3\2\2\2\u03f3\u03f5\78\2\2\u03f4\u03f3\3",
                        "\2\2\2\u03f4\u03f5\3\2\2\2\u03f5\u03f7\3\2\2\2\u03f6\u03e4\3\2\2\2",
                        "\u03f6\u03f0\3\2\2\2\u03f7\u040d\3\2\2\2\u03f8\u03fb\5d\63\2\u03f9",
                        "\u03fb\5v<\2\u03fa\u03f8\3\2\2\2\u03fa\u03f9\3\2\2\2\u03fb\u040a\3",
                        "\2\2\2\u03fc\u040b\5\u00a4S\2\u03fd\u0400\78\2\2\u03fe\u0401\5d\63",
                        "\2\u03ff\u0401\5v<\2\u0400\u03fe\3\2\2\2\u0400\u03ff\3\2\2\2\u0401",
                        "\u0403\3\2\2\2\u0402\u03fd\3\2\2\2\u0403\u0406\3\2\2\2\u0404\u0402",
                        "\3\2\2\2\u0404\u0405\3\2\2\2\u0405\u0408\3\2\2\2\u0406\u0404\3\2\2",
                        "\2\u0407\u0409\78\2\2\u0408\u0407\3\2\2\2\u0408\u0409\3\2\2\2\u0409",
                        "\u040b\3\2\2\2\u040a\u03fc\3\2\2\2\u040a\u0404\3\2\2\2\u040b\u040d",
                        "\3\2\2\2\u040c\u03e2\3\2\2\2\u040c\u03fa\3\2\2\2\u040d\u009b\3\2\2",
                        "\2\u040e\u040f\7!\2\2\u040f\u0415\7*\2\2\u0410\u0412\7\66\2\2\u0411",
                        "\u0413\5\u009eP\2\u0412\u0411\3\2\2\2\u0412\u0413\3\2\2\2\u0413\u0414",
                        "\3\2\2\2\u0414\u0416\7\67\2\2\u0415\u0410\3\2\2\2\u0415\u0416\3\2\2",
                        "\2\u0416\u0417\3\2\2\2\u0417\u0418\79\2\2\u0418\u0419\5b\62\2\u0419",
                        "\u009d\3\2\2\2\u041a\u041f\5\u00a0Q\2\u041b\u041c\78\2\2\u041c\u041e",
                        "\5\u00a0Q\2\u041d\u041b\3\2\2\2\u041e\u0421\3\2\2\2\u041f\u041d\3\2",
                        "\2\2\u041f\u0420\3\2\2\2\u0420\u0423\3\2\2\2\u0421\u041f\3\2\2\2\u0422",
                        "\u0424\78\2\2\u0423\u0422\3\2\2\2\u0423\u0424\3\2\2\2\u0424\u009f\3",
                        "\2\2\2\u0425\u0427\5d\63\2\u0426\u0428\5\u00a4S\2\u0427\u0426\3\2\2",
                        "\2\u0427\u0428\3\2\2\2\u0428\u0432\3\2\2\2\u0429\u042a\5d\63\2\u042a",
                        "\u042b\7<\2\2\u042b\u042c\5d\63\2\u042c\u0432\3\2\2\2\u042d\u042e\7",
                        ";\2\2\u042e\u0432\5d\63\2\u042f\u0430\7\65\2\2\u0430\u0432\5d\63\2",
                        "\u0431\u0425\3\2\2\2\u0431\u0429\3\2\2\2\u0431\u042d\3\2\2\2\u0431",
                        "\u042f\3\2\2\2\u0432\u00a1\3\2\2\2\u0433\u0436\5\u00a4S\2\u0434\u0436",
                        "\5\u00a6T\2\u0435\u0433\3\2\2\2\u0435\u0434\3\2\2\2\u0436\u00a3\3\2",
                        "\2\2\u0437\u0439\7\'\2\2\u0438\u0437\3\2\2\2\u0438\u0439\3\2\2\2\u0439",
                        "\u043a\3\2\2\2\u043a\u043b\7\23\2\2\u043b\u043c\5\u0096L\2\u043c\u043d",
                        "\7\24\2\2\u043d\u043f\5l\67\2\u043e\u0440\5\u00a2R\2\u043f\u043e\3",
                        "\2\2\2\u043f\u0440\3\2\2\2\u0440\u00a5\3\2\2\2\u0441\u0442\7\17\2\2",
                        "\u0442\u0444\5f\64\2\u0443\u0445\5\u00a2R\2\u0444\u0443\3\2\2\2\u0444",
                        "\u0445\3\2\2\2\u0445\u00a7\3\2\2\2\u0446\u0447\7*\2\2\u0447\u00a9\3",
                        "\2\2\2\u0448\u044a\7\"\2\2\u0449\u044b\5\u00acW\2\u044a\u0449\3\2\2",
                        "\2\u044a\u044b\3\2\2\2\u044b\u00ab\3\2\2\2\u044c\u044d\7\t\2\2\u044d",
                        "\u0450\5d\63\2\u044e\u0450\5\u0098M\2\u044f\u044c\3\2\2\2\u044f\u044e",
                        "\3\2\2\2\u0450\u00ad\3\2\2\2\u00a8\u00b3\u00b7\u00b9\u00c2\u00cb\u00ce",
                        "\u00d5\u00db\u00e5\u00ec\u00f3\u00f9\u00fd\u0103\u0109\u010d\u0114",
                        "\u0116\u0118\u011d\u011f\u0121\u0125\u012b\u012f\u0136\u0138\u013a",
                        "\u013f\u0141\u0146\u014b\u0151\u0155\u015b\u0161\u0165\u016c\u016e",
                        "\u0170\u0175\u0177\u0179\u017d\u0183\u0187\u018e\u0190\u0192\u0197",
                        "\u0199\u019f\u01a6\u01aa\u01b6\u01bd\u01c2\u01c6\u01c9\u01cf\u01d3",
                        "\u01d8\u01dc\u01e0\u01ee\u01f6\u01fe\u0200\u0204\u020d\u0214\u0216",
                        "\u021f\u0224\u0229\u0230\u0234\u023b\u0243\u024c\u0255\u025c\u0267",
                        "\u026d\u027a\u0280\u0289\u0294\u029f\u02a4\u02a9\u02ae\u02b6\u02bf",
                        "\u02c5\u02c7\u02cf\u02d3\u02db\u02de\u02e2\u02e6\u02ed\u02f7\u02ff",
                        "\u0305\u030d\u031d\u0327\u032f\u0337\u033f\u0347\u034f\u0355\u035a",
                        "\u035d\u0363\u0369\u036e\u0373\u037b\u0381\u0385\u038b\u038f\u0393",
                        "\u0395\u0399\u03a2\u03a9\u03ad\u03b1\u03b5\u03b8\u03ba\u03be\u03c2",
                        "\u03c7\u03cb\u03cf\u03d6\u03da\u03e2\u03ec\u03f0\u03f4\u03f6\u03fa",
                        "\u0400\u0404\u0408\u040a\u040c\u0412\u0415\u041f\u0423\u0427\u0431",
                        "\u0435\u0438\u043f\u0444\u044a\u044f"].join("")

    @@_ATN = Antlr4::Runtime::ATNDeserializer.new().deserialize(@@_serializedATN)
  end
end
