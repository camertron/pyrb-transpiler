module Pyrb
  module Transpiler
    class WhileVisitor < StatementVisitor
      def visitWhile_stmt(ctx)
        context.write_ruby('while !!(')
        visit(ctx.test)
        context.write_ruby(')')
        context.enter_block
        context.write_ruby_line

        ctx.suite.each do |suite|
          visit(suite)
        end

        context.exit_block
        context.write_ruby_line("end\n")
      end
    end
  end
end
