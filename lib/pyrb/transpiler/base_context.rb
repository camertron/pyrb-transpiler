module Pyrb
  module Transpiler
    class BaseContext
      INDENT = '  '.freeze

      attr_reader :scope_stack, :indent_level, :indent_str
      attr_reader :output, :heredoc_buffer, :started_line

      attr_accessor :inside_try
      alias_method :inside_try?, :inside_try

      def initialize(scope_stack = nil)
        @scope_stack = scope_stack || [Scope.new(:file, symbol_table: SymbolTable::BASE_TABLE)]
        @indent_level = 0
        @indent_str = ''
        @output = ''
        @heredoc_buffer = ''
        @started_line = false
        @inside_try = false
      end

      def current_scope
        scope_stack.last
      end

      def symbol_table
        current_scope.symbol_table
      end

      def enter_block(should_indent = true)
        indent if should_indent
      end

      def exit_block(should_dedent = true)
        dedent if should_dedent
      end

      def indent
        @indent_level += 1
        @indent_str += INDENT
      end

      def dedent
        @indent_level -= 1
        @indent_str = indent_str[0...(indent_str.length - INDENT.length)]
      end

      def enter_scope(type, should_indent = true)
        enter_block(should_indent)
        scope_stack.push(Scope.new(type, parent: current_scope))
      end

      def exit_scope(should_dedent = true)
        exit_block(should_dedent)
        scope_stack.pop
      end

      def write_ruby(ruby)
        if !started_line
          @output << indent_str
          @started_line = true
        end

        @output << indent_newlines(ruby)
      end

      def write_ruby_line(ruby = nil)
        if ruby
          @output << indent_str if !started_line
          @output << indent_newlines(ruby)
        end

        @output << "\n"
        @started_line = false
        flush_heredoc
      end

      def ensure_newline
        if started_line
          @output << "\n"
          @started_line = false
        end
      end

      def write_heredoc(contents)
        heredoc_buffer << contents
      end

      def to_s
        output
      end

      def flush_heredoc
        if heredoc_buffer.size > 0
          ensure_newline
          output << indent_str
          output << indent_newlines(heredoc_buffer)
          output << "\n"
          heredoc_buffer.clear
        end
      end

      def parent_scope
        if scope_stack.size > 1
          scope_stack[scope_stack.size - 2]
        end
      end

      private

      def indent_newlines(str)
        str
          .gsub(/\r?\n/, "\n#{indent_str}")
          .gsub(/\n[\s]*\n/, "\n\n")
          .sub(/\n +\z/, "\n")
      end
    end
  end
end
