module Pyrb
  module Transpiler
    class ArithmeticExpressionVisitor < StatementVisitor
      def visitArith_expr(ctx)
        if ctx.term.size > 1
          ctx.children.each do |child|
            if child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
              context.write_ruby(Ruby.pad(child.text))
            else
              visit(child)
            end
          end
        else
          # is this not where unary operators would happen?
          # apparently not :(
          visit_children(ctx)
        end
      end
    end
  end
end
