# Generated from ./Python3.g4 by ANTLR 4.7.2
require "antlr4/runtime"

module Pyrb::Transpiler
  class Python3Visitor < Antlr4::Runtime::ParseTreeVisitor
    def visitSingle_input(ctx)
    end

    def visitFile_input(ctx)
    end

    def visitEval_input(ctx)
    end

    def visitDecorator(ctx)
    end

    def visitDecorators(ctx)
    end

    def visitDecorated(ctx)
    end

    def visitAsync_funcdef(ctx)
    end

    def visitFuncdef(ctx)
    end

    def visitParameters(ctx)
    end

    def visitTypedargslist(ctx)
    end

    def visitTfpdef(ctx)
    end

    def visitVarargslist(ctx)
    end

    def visitVfpdef(ctx)
    end

    def visitStmt(ctx)
    end

    def visitSimple_stmt(ctx)
    end

    def visitSmall_stmt(ctx)
    end

    def visitExpr_stmt(ctx)
    end

    def visitAnnassign(ctx)
    end

    def visitTestlist_star_expr(ctx)
    end

    def visitAugassign(ctx)
    end

    def visitDel_stmt(ctx)
    end

    def visitPass_stmt(ctx)
    end

    def visitFlow_stmt(ctx)
    end

    def visitBreak_stmt(ctx)
    end

    def visitContinue_stmt(ctx)
    end

    def visitReturn_stmt(ctx)
    end

    def visitYield_stmt(ctx)
    end

    def visitRaise_stmt(ctx)
    end

    def visitImport_stmt(ctx)
    end

    def visitImport_name(ctx)
    end

    def visitImport_from(ctx)
    end

    def visitImport_as_name(ctx)
    end

    def visitDotted_as_name(ctx)
    end

    def visitImport_as_names(ctx)
    end

    def visitDotted_as_names(ctx)
    end

    def visitDotted_name(ctx)
    end

    def visitGlobal_stmt(ctx)
    end

    def visitNonlocal_stmt(ctx)
    end

    def visitAssert_stmt(ctx)
    end

    def visitCompound_stmt(ctx)
    end

    def visitAsync_stmt(ctx)
    end

    def visitIf_stmt(ctx)
    end

    def visitWhile_stmt(ctx)
    end

    def visitFor_stmt(ctx)
    end

    def visitTry_stmt(ctx)
    end

    def visitWith_stmt(ctx)
    end

    def visitWith_item(ctx)
    end

    def visitExcept_clause(ctx)
    end

    def visitSuite(ctx)
    end

    def visitTest(ctx)
    end

    def visitTest_nocond(ctx)
    end

    def visitLambdef(ctx)
    end

    def visitLambdef_nocond(ctx)
    end

    def visitOr_test(ctx)
    end

    def visitAnd_test(ctx)
    end

    def visitNot_test(ctx)
    end

    def visitComparison(ctx)
    end

    def visitComp_op(ctx)
    end

    def visitStar_expr(ctx)
    end

    def visitExpr(ctx)
    end

    def visitXor_expr(ctx)
    end

    def visitAnd_expr(ctx)
    end

    def visitShift_expr(ctx)
    end

    def visitArith_expr(ctx)
    end

    def visitTerm(ctx)
    end

    def visitFactor(ctx)
    end

    def visitPower(ctx)
    end

    def visitAtom_expr(ctx)
    end

    def visitAtom(ctx)
    end

    def visitTestlist_comp(ctx)
    end

    def visitTrailer(ctx)
    end

    def visitSubscriptlist(ctx)
    end

    def visitSubscript(ctx)
    end

    def visitSliceop(ctx)
    end

    def visitExprlist(ctx)
    end

    def visitTestlist(ctx)
    end

    def visitDictorsetmaker(ctx)
    end

    def visitClassdef(ctx)
    end

    def visitArglist(ctx)
    end

    def visitArgument(ctx)
    end

    def visitComp_iter(ctx)
    end

    def visitComp_for(ctx)
    end

    def visitComp_if(ctx)
    end

    def visitEncoding_decl(ctx)
    end

    def visitYield_expr(ctx)
    end

    def visitYield_arg(ctx)
    end
  end
end
