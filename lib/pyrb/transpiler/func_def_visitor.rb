module Pyrb
  module Transpiler
    class FuncDefVisitor < StatementVisitor
      def visitFuncdef(ctx)
        name = ctx.NAME.text
        arg_list = ArgListVisitor.new(context)

        if ctx.parameters.typedargslist
          arg_list.visit(ctx.parameters.typedargslist)
        end

        case context.current_scope.type
          when :file, :invocation, :method
            define_function(name, arg_list.args, ctx)
          when :class
            define_mtd(name, arg_list.args, ctx)
        end
      end

      private

      def define_mtd(name, args, ctx)
        is_gen = generator?(ctx)
        ref = "klass.attrs['#{name}']"

        context.write_ruby(ref)
        context.write_ruby(' = Pyrb.defn(')
        context.write_ruby(args.to_metadata)

        context.write_ruby(') do |')
        context.write_ruby(args.to_args)
        context.write_ruby_line('|')
        context.enter_scope(:method)
        @context = BlockContext.enter(@context)

        # add args
        args.each do |arg|
          context.symbol_table << Symbol.new(arg.symbol.python_name, arg.symbol.ruby_expression)
        end

        context.current_scope.self_ref = args[0].symbol

        if is_gen
          context.write_ruby_line('Pyrb::Generator.new([')
          context.enter_scope(:invocation)
          context.write_ruby_line('Enumerator.new do |__pyrb_gen__|')
          context.enter_scope(:generator)
        end

        visit(ctx.suite)

        if is_gen
          context.exit_scope
          context.write_ruby_line('end')
          context.exit_scope()
          context.write_ruby_line('])')
        end

        @context = @context.exit
        context.exit_scope
        context.symbol_table << Symbol.new(name, ref, :def)

        context.ensure_newline
        context.write_ruby_line("end\n")
      end

      def define_function(name, args, ctx)
        is_gen = generator?(ctx)

        if context.current_scope.type == :file && !context.symbol_table.include?(name)
          context.write_ruby("exports['")
          context.write_ruby(name)
          context.write_ruby("'] = Pyrb.defn(")
          context.write_ruby(args.to_metadata)

          # so the function can call itself
          ref = "exports['#{name}']"
          context.symbol_table << Symbol.new(name, ref)
          context.write_ruby(') do |')
          context.write_ruby(args.to_args)
          context.write_ruby_line('|')
        elsif context.current_scope.type == :invocation
          context.write_ruby('Pyrb.defn(')
          context.write_ruby(args.to_metadata)
          context.write_ruby(') do |')
          context.write_ruby(args.to_args)
          context.write_ruby_line('|')

          # so the function can call itself
          # invocations happen for things like static and class method definitions, and should be
          # available on a local "klass" variable
          ref = "klass.attrs['#{name}']"
          context.symbol_table << Symbol.new(name, ref)
        else
          if sym = context.symbol_table[name]
            name = sym.ruby_expression
          end

          context.write_ruby(name)
          context.write_ruby(' = Pyrb.defn(')
          context.write_ruby(args.to_metadata)
          context.write_ruby(') do |')
          context.write_ruby(args.to_args)
          context.write_ruby_line('|')

          # so the function can call itself
          ref = name
          context.symbol_table << Symbol.new(name, ref, :def)
        end

        context.enter_scope(:method)
        @context = BlockContext.enter(@context)

        args.each do |arg|
          context.symbol_table << Symbol.new(arg.symbol.python_name, arg.symbol.ruby_expression)
        end

        if is_gen
          context.write_ruby_line('Pyrb::Generator.new([')
          context.enter_scope(:invocation)
          context.write_ruby_line('Enumerator.new do |__pyrb_gen__|')
          context.enter_scope(:generator)
        end

        visit(ctx.suite)

        if is_gen
          context.exit_scope
          context.write_ruby_line('end')
          context.exit_scope
          context.write_ruby_line('])')
        end

        @context = @context.exit
        context.exit_scope
        context.symbol_table << Symbol.new(name, ref)

        context.ensure_newline
        context.write_ruby_line("end\n")
      end

      def generator?(ctx)
        ctx.suite.text.include?('yield')
      end
    end
  end
end
