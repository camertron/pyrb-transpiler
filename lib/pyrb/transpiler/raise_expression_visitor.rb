module Pyrb
  module Transpiler
    class RaiseExpressionVisitor < StatementVisitor
      def visitRaise_stmt(ctx)
        context.write_ruby('Pyrb.raize')

        if ctx.test.size > 0
          context.write_ruby('(')

          # object to raise
          visit(ctx.test.first)

          # "from" part of the raise (sets the "cause" in rubyland)
          if ctx.test.size > 1
            context.write_ruby(', from: ')
            visit(ctx.test[1])
          end

          context.write_ruby_line(')')
        else
          context.write_ruby_line
        end
      end
    end
  end
end
