module Pyrb
  module Transpiler
    class ImportExportScanner < PyrbBaseVisitor
      attr_reader :import_visitor

      def initialize(context)
        super
        @import_visitor = ImportVisitor.new
      end

      def commit
        import_visitor.imports.each do |imp|
          imp_with_context = imp.apply_to(context)
          context.write_ruby_line(imp_with_context.to_ruby)
        end

        if import_visitor.imports.size > 0
          context.write_ruby_line
        end
      end

      def visitClassdef(ctx)
        class_name = ctx.NAME.text
        context.symbol_table << Symbol.new(class_name, "exports['#{class_name}']", :class)
      end

      def visitFuncdef(ctx)
        func_name = ctx.NAME.text
        context.symbol_table << Symbol.new(func_name, "exports['#{func_name}']", :def)
      end

      def visitExpr_stmt(ctx)
        return unless assignment?(ctx)
        var_name = ctx.children[0].text
        context.symbol_table << Symbol.new(var_name, "exports['#{var_name}']")
      end

      def visitTry_stmt(ctx)
        # don't let visitor descend into try/except block; leave those imports alone
      end

      def visitImport_stmt(ctx)
        import_visitor.visitImport_stmt(ctx)
      end

      private

      def assignment?(ctx)
        ctx.children.size > 1 &&
          ctx.children[1].class == Antlr4::Runtime::TerminalNodeImpl &&
          ctx.children[1].text == '=' &&
          Python.identifier?(ctx.children[0].text)
      end
    end
  end
end
