module Pyrb
  module Transpiler
    class ShiftVisitor < StatementVisitor
      def visitShift_expr(ctx)
        if ctx.children.size > 1
          ctx.children.each do |child|
            if child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
              context.write_ruby(Ruby.pad(child.text))
            else
              visit(child)
            end
          end
        else
          visit_children(ctx)
        end
      end
    end
  end
end
