module Pyrb
  module Transpiler
    class ContinueVisitor < StatementVisitor
      def visitContinue_stmt(ctx)
        context.write_ruby_line('next')
      end
    end
  end
end
