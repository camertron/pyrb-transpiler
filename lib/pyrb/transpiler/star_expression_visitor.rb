module Pyrb
  module Transpiler
    class StarExpressionVisitor < StatementVisitor
      def visitStar_expr(ctx)
        context.write_ruby('*')
        visit(ctx.expr)
      end
    end
  end
end
