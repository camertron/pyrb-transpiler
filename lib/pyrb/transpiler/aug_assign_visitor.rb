module Pyrb
  module Transpiler
    class AugAssignVisitor < StatementVisitor
      def visitAugassign(ctx)
        context.write_ruby(Ruby.pad(ctx.children[0].text))
      end
    end
  end
end
