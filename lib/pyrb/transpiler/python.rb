module Pyrb
  module Transpiler
    module Python
      IDENT_RE = /\A[a-zA-Z_][a-zA-Z0-9_]*\z/.freeze
      KEYWORDS = %w(None True False).freeze

      def identifier?(str)
        str =~ IDENT_RE && !KEYWORDS.include?(str)
      end

      extend self
    end
  end
end
