module Pyrb
  module Transpiler
    class DecorationVisitor < StatementVisitor
      def visitDecorated(ctx)
        ctx.decorators.decorator.each do |decorator|
          name = decorator.dotted_name.NAME[0].text

          # built-in decorators, i.e. ones that don't live in some imported module
          # and therefore are only one name long
          if decorator.dotted_name.NAME.size == 1
            if name == 'property'
              prop = PropertyDecorator.new(ctx.funcdef, 'klass')
              prop.apply_to(context)
              context.current_scope.add_decorator(ctx.funcdef.NAME.text, prop)
            elsif name == 'classmethod'
              ClassMethodDecorator.new(ctx.funcdef, 'klass').apply_to(context)
            elsif name == 'staticmethod'
              StaticMethodDecorator.new(ctx.funcdef, 'klass').apply_to(context)
            end
          elsif context.current_scope.has_decorator?(name)
            dec = context.current_scope.decorator_for(name)
            sub_names = []

            1.upto(decorator.dotted_name.NAME.size - 1) do |i|
              sub_names.add(decorator.dotted_name.NAME[i].text)
            end

            dec.sub_decorator_for(sub_names, ctx.funcdef).apply_to(context)
          end
        end
      end
    end
  end
end
