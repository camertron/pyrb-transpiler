module Pyrb
  module Transpiler
    class ArgumentList
      include Enumerable

      attr_reader :args

      def initialize
        @args = []
      end

      def add(symbol, type, splat, kwsplat, default_exp = nil)
        args << Argument.new(symbol, type, args.size, splat, kwsplat, default_exp)
      end

      def to_args
        ''.tap do |result|
          args.each_with_index do |arg, idx|
            result << ', ' if idx > 0
            result << arg.symbol.ruby_expression
          end
        end
      end

      def to_metadata
        ''.tap do |result|
          args.each_with_index do |arg, idx|
            result << ", " if idx > 0
            result << arg.to_metadata
          end
        end
      end

      def each(&block)
        args.each(&block)
      end

      def [](idx)
        args[idx]
      end

      def size
        args.size
      end
    end
  end
end
