module Pyrb
  module Transpiler
    class IfStatementVisitor < StatementVisitor
      # You may be wondering why !! is added to every if statement. Well, for
      # the simple reason that, in Python, 0 == False and 1 == True. In the
      # pyrb runtime, the Integer, TrueClass, and FalseClass classes have been
      # modified so these equivalences (and their associative inverses) hold,
      # but unfortunately that only gets us so far. In Ruby, it's impossible to
      # define another object as being falsey - the built-in "false" and "nil"
      # values are the only falsey values we get. Consider these two examples:
      #
      # Ruby: if 0              Python: if 0:
      #         puts 'abc'                print('abc')
      #       end
      #
      # Although at first glance equivalent, the Ruby version will print "abc"
      # while the Python version will not. In Ruby, 0 is not nil or false, so
      # it must be true. In Python, 0 evaluates to false. To mitigate the
      # problem, the pyrb runtime redefines the ! method on Integer. Calling it
      # twice will effectively cast the value to a boolean, respecting the
      # Python rules in the process.
      #
      def visitIf_stmt(ctx)
        ctx.children.each do |child|
          if child.text == 'if'
            context.write_ruby('if !!(')
          elsif child.text == 'elif'
            context.ensure_newline
            context.write_ruby("elsif !!(")
          elsif child.text == 'else'
            context.ensure_newline
            context.write_ruby_line('else')
            context.enter_block
          elsif child.is_a?(Python3Parser::TestContext)
            visit(child)
            context.write_ruby_line(')')
            context.enter_block
          elsif child.is_a?(Python3Parser::SuiteContext)
            visit(child)
            context.exit_block
          end
        end

        context.ensure_newline
        context.write_ruby_line("end\n")
      end
    end
  end
end
