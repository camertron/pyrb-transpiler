module Pyrb
  module Transpiler
    class ExpressionChild
      attr_reader :parent, :child_index

      def initialize(parent, child_index)
        @parent = parent
        @child_index = child_index
      end

      def operator
        @operator ||= begin
          operator_index = if child_index == 0
            1
          elsif child_index == parent.children.size - 1
            child_index - 1
          else
            child_index + 1
          end

          if operator_index <= parent.children.size - 1
            parent.children[operator_index].text
          else
            ''
          end
        end
      end

      def side
        @side ||= if child_index == 0
          :left
        elsif child_index == parent.children.size - 1
          :right
        else
          :left
        end
      end

      def assignment?
        operator.end_with?('=')
      end
    end
  end
end
