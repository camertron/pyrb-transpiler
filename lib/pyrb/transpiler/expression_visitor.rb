module Pyrb
  module Transpiler
    class ExpressionVisitor < StatementVisitor
      def visitExpr_stmt(ctx)
        lhs_length = 0
        deconstruct = false
        open_groups = 0

        (0...ctx.children.size).step(2) do |i|
          operand = ctx.children[i]
          operator = nil

          if i < ctx.children.size - 1
            operator = ctx.children[i + 1].text
          end

          if i == ctx.children.size - 1
            # we have reached the right-hand side of the expression
            if lhs_length > 1
              deconstruct = true
              context.write_ruby("Pyrb.deconstruct(#{lhs_length}, ")
            end
          else
            term_count = 0

            # count the number of lhs terms
            operand.children.each do |operand_child|
              if !operand_child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
                term_count += 1
              end
            end

            # we're still on the left-hand side of the expression
            if term_count > lhs_length
              # trying to get the max number of lhs assignments
              lhs_length = term_count
            end

            # since we're still on the left-hand side, wrap nested deconstructed assignments in
            # parens, since ruby can't handle stuff like a = b, c = ['e', 'f'] without them
            if i >= 1 && lhs_length > 1
              context.write_ruby('(')
              open_groups += 1
            end
          end

          if operator && operator.end_with?('=')
            operand.children.each do |operand_child|
              if operand_child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
                context.write_ruby(Ruby.pad(operand_child.text))
              else
                ident = operand_child.text

                if Python.identifier?(ident)
                  sym = context.symbol_table[ident]

                  if sym.nil? || sym.type != :global
                    if context.current_scope.type == :class
                      # assignment to a variable inside a class definition
                      ruby_expression = "klass.attrs['#{ident}']"
                      context.write_ruby(ruby_expression)
                      context.symbol_table << Symbol.new(ident, ruby_expression)
                    elsif context.current_scope.type == :file
                      ruby_expression = "exports['#{ident}']"
                      context.write_ruby(ruby_expression)
                      context.symbol_table << Symbol.new(ident, ruby_expression)
                    else
                      # assignment to local variable (allows shadowing function with same name)
                      ruby_expression = Ruby.to_identifier(ident)
                      context.write_ruby(ruby_expression)
                      context.symbol_table << Symbol.new(ident, ruby_expression)
                    end
                  else
                    context.write_ruby(sym.ruby_expression)
                  end
                elsif ident.start_with?('(')
                  # this is probably a tuple since it's being assigned to, so remove the parens
                  # and visit everything in between
                  atom = find_next_atom_expr(operand_child).atom

                  1.upto(atom.children.size - 1) do |tc|
                    visit(atom.children[tc])
                  end
                else
                  visit(operand_child)
                end
              end
            end
          else
            visit(operand)
          end

          if operator
            context.write_ruby(Ruby.pad(operator))
          end
        end

        context.write_ruby(')') if deconstruct
        open_groups.times { context.write_ruby(")") }
        context.write_ruby_line
      end

      def visitExpr(ctx)
        ctx.children.each do |child|
          if child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
            context.write_ruby(Ruby.pad(child.text))
          else
            visit(child)
          end
        end
      end
    end
  end
end
