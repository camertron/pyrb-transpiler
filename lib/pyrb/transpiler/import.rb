module Pyrb
  module Transpiler
    class Import
      attr_reader :importables, :from, :reassignments

      def initialize(importables, from, reassignments = nil)
        @importables = importables
        @from = from
        @reassignments = reassignments || {}
      end

      def apply_to(context)
        new_importables = {}
        new_reassignments = {}

        importables.each_pair do |var, aliass|
          # Do this crap because python treats a single underscore as a variable name
          # differently than ruby does. For example in Python:
          #
          # >>> _, = ['abc']
          # >>> _
          # "abc"
          #
          # Whereas in Ruby:
          #
          # irb> _, = ['abc']
          # irb> _
          # ["abc"]
          #
          # BUT also in Ruby:
          #
          # irb> a, = ['abc']
          # irb> a
          # "abc"
          #
          # Apparently ruby thinks "_" is special. Damn it.
          #
          if aliass == '_'
            tmp = Ruby.get_unique_variable_name('_tmp_', context.current_scope.symbol_table)
            new_reassignments[tmp] = aliass
            new_importables[var] = tmp
            context.symbol_table << Symbol.new(aliass, aliass, :import)
          elsif Ruby.is_constant?(aliass)
            case context.current_scope.type
              when :file, :class
              else
                lowercased_alias = "#{aliass[0...1].downcase}#{aliass[1..-1]}"
                aliass = Ruby.get_unique_variable_name(lowercased_alias, context.current_scope.symbol_table)
            end

            context.symbol_table << Symbol.new(var, aliass, :import)
            new_importables[var] = aliass
          else
            new_importables[var] = aliass
            context.symbol_table << Symbol.new(aliass, aliass, :import)
          end
        end

        self.class.new(new_importables, from, new_reassignments)
      end

      def to_ruby
        ''.tap do |result|
          result << importables.values.join(', ')

          result << ', _' if importables.size == 1
          result << ' = import('

          importables.keys.each_with_index do |mod, idx|
            result << ', ' if idx > 0
            result << "'#{mod}'"
          end

          result << ", from: '#{from}'" if from
          result << ')'

          reassignments.each_pair do |k, v|
            result << "\n#{v} = #{k}"
          end
        end
      end
    end
  end
end
