module Pyrb
  module Transpiler
    module Ruby
      KEYWORDS = %w(
        __ENCODING__ __LINE__ __FILE__ BEGIN END
        alias and begin break case class def
        defined? do else elsif end ensure false
        for if in module next nil not or
        redo rescue retry return self super then
        true undef unless until when while yield
        proc
      ).freeze

      # there are obviously more of these...
      KERNEL_FUNCTIONS = %w(format).freeze

      LEFT_RIGHT_PAD = %w(
        = == != > < >= <= + - / * ^ % | & += -= *= /= << >> <<= >>=
      )

      RIGHT_PAD = [',']

      INTERPOLATION_RE = /(?<!\\)#/.freeze

      RAW_STRING_WRAPPERS = [%w[( )], %w([ ]), %w({ }), %w(| |), %w(< >)].freeze

      def to_identifier(str)
        return "#{str}_" if KEYWORDS.include?(str) || KERNEL_FUNCTIONS.include?(str)
        str
      end

      def constantize(str)
        "#{str[0].upcase}#{str[1..-1]}"
      end

      def modularize(str)
        constantize(
          str
            .gsub(/\.py\z/, '')
            .gsub(/[-:\\\/.]/, '_')
        )
      end

      def escape_interpolation_chars(str)
        return '' if str.empty?
        return str if str == '#'

        chunks = str.split(INTERPOLATION_RE)
        result = chunks[0].dup

        (1...chunks.length).each do |i|
          result << '\#'
          result << chunks[i]
        end

        # String.split doesn't include the right-hand side of the split if it's empty
        result << '#' if str.end_with?('#')
        result
      end

      def escape_double_quoted_string(str)
        str = escape_interpolation_chars(str)
        escaping = false
        result = ''

        str.each_char do |char|
          case char
            when '\\'
              if escaping
                # indicates escaped backslash
                result << '\\\\'
                escaping = false
              else
                escaping = true
              end

            when '"'
              result << '\\"'
              escaping = false

            else
              if escaping
                # indicates escaped character of some kind (i.e. \n or \t)
                result << '\\'
                escaping = false
              end

              result << char
          end
        end

        result
      end

      def wrap_raw_string(raw)
        RAW_STRING_WRAPPERS.each do |left, right|
          unless raw.include?(left) || raw.include?(right)
            return "%q#{left}#{raw}#{right}"
          end
        end

        raise RuntimeException, "Could not find a way to wrap the raw string '#{raw}'"
      end

      def get_unique_variable_name(original_name, symbol_table)
        original_ident = to_identifier(original_name)

        unless symbol_table.includes_ruby?(original_ident)
          return original_ident
        end

        counter = 2

        loop do
          current = to_identifier("#{original_name}#{counter}")
          return current unless symbol_table.includes_ruby?(current)
          counter += 1
        end
      end

      def pad(str)
        if LEFT_RIGHT_PAD.include?(str)
          " #{str} "
        elsif RIGHT_PAD.include?(str)
          "#{str} "
        else
          str
        end
      end

      def is_constant?(ident)
        # checking if first character is uppercase
        ident[0].ord >= 65 && ident[0].ord <= 90
      end

      extend self
    end
  end
end
