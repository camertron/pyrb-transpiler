# Generated from ./Python3.g4 by ANTLR 4.7.2
require "antlr4/runtime"

module Pyrb::Transpiler
  class Python3BaseListener < Python3Listener
    def enterSingle_input(ctx)
    end

    def exitSingle_input(ctx)
    end

    def enterFile_input(ctx)
    end

    def exitFile_input(ctx)
    end

    def enterEval_input(ctx)
    end

    def exitEval_input(ctx)
    end

    def enterDecorator(ctx)
    end

    def exitDecorator(ctx)
    end

    def enterDecorators(ctx)
    end

    def exitDecorators(ctx)
    end

    def enterDecorated(ctx)
    end

    def exitDecorated(ctx)
    end

    def enterAsync_funcdef(ctx)
    end

    def exitAsync_funcdef(ctx)
    end

    def enterFuncdef(ctx)
    end

    def exitFuncdef(ctx)
    end

    def enterParameters(ctx)
    end

    def exitParameters(ctx)
    end

    def enterTypedargslist(ctx)
    end

    def exitTypedargslist(ctx)
    end

    def enterTfpdef(ctx)
    end

    def exitTfpdef(ctx)
    end

    def enterVarargslist(ctx)
    end

    def exitVarargslist(ctx)
    end

    def enterVfpdef(ctx)
    end

    def exitVfpdef(ctx)
    end

    def enterStmt(ctx)
    end

    def exitStmt(ctx)
    end

    def enterSimple_stmt(ctx)
    end

    def exitSimple_stmt(ctx)
    end

    def enterSmall_stmt(ctx)
    end

    def exitSmall_stmt(ctx)
    end

    def enterExpr_stmt(ctx)
    end

    def exitExpr_stmt(ctx)
    end

    def enterAnnassign(ctx)
    end

    def exitAnnassign(ctx)
    end

    def enterTestlist_star_expr(ctx)
    end

    def exitTestlist_star_expr(ctx)
    end

    def enterAugassign(ctx)
    end

    def exitAugassign(ctx)
    end

    def enterDel_stmt(ctx)
    end

    def exitDel_stmt(ctx)
    end

    def enterPass_stmt(ctx)
    end

    def exitPass_stmt(ctx)
    end

    def enterFlow_stmt(ctx)
    end

    def exitFlow_stmt(ctx)
    end

    def enterBreak_stmt(ctx)
    end

    def exitBreak_stmt(ctx)
    end

    def enterContinue_stmt(ctx)
    end

    def exitContinue_stmt(ctx)
    end

    def enterReturn_stmt(ctx)
    end

    def exitReturn_stmt(ctx)
    end

    def enterYield_stmt(ctx)
    end

    def exitYield_stmt(ctx)
    end

    def enterRaise_stmt(ctx)
    end

    def exitRaise_stmt(ctx)
    end

    def enterImport_stmt(ctx)
    end

    def exitImport_stmt(ctx)
    end

    def enterImport_name(ctx)
    end

    def exitImport_name(ctx)
    end

    def enterImport_from(ctx)
    end

    def exitImport_from(ctx)
    end

    def enterImport_as_name(ctx)
    end

    def exitImport_as_name(ctx)
    end

    def enterDotted_as_name(ctx)
    end

    def exitDotted_as_name(ctx)
    end

    def enterImport_as_names(ctx)
    end

    def exitImport_as_names(ctx)
    end

    def enterDotted_as_names(ctx)
    end

    def exitDotted_as_names(ctx)
    end

    def enterDotted_name(ctx)
    end

    def exitDotted_name(ctx)
    end

    def enterGlobal_stmt(ctx)
    end

    def exitGlobal_stmt(ctx)
    end

    def enterNonlocal_stmt(ctx)
    end

    def exitNonlocal_stmt(ctx)
    end

    def enterAssert_stmt(ctx)
    end

    def exitAssert_stmt(ctx)
    end

    def enterCompound_stmt(ctx)
    end

    def exitCompound_stmt(ctx)
    end

    def enterAsync_stmt(ctx)
    end

    def exitAsync_stmt(ctx)
    end

    def enterIf_stmt(ctx)
    end

    def exitIf_stmt(ctx)
    end

    def enterWhile_stmt(ctx)
    end

    def exitWhile_stmt(ctx)
    end

    def enterFor_stmt(ctx)
    end

    def exitFor_stmt(ctx)
    end

    def enterTry_stmt(ctx)
    end

    def exitTry_stmt(ctx)
    end

    def enterWith_stmt(ctx)
    end

    def exitWith_stmt(ctx)
    end

    def enterWith_item(ctx)
    end

    def exitWith_item(ctx)
    end

    def enterExcept_clause(ctx)
    end

    def exitExcept_clause(ctx)
    end

    def enterSuite(ctx)
    end

    def exitSuite(ctx)
    end

    def enterTest(ctx)
    end

    def exitTest(ctx)
    end

    def enterTest_nocond(ctx)
    end

    def exitTest_nocond(ctx)
    end

    def enterLambdef(ctx)
    end

    def exitLambdef(ctx)
    end

    def enterLambdef_nocond(ctx)
    end

    def exitLambdef_nocond(ctx)
    end

    def enterOr_test(ctx)
    end

    def exitOr_test(ctx)
    end

    def enterAnd_test(ctx)
    end

    def exitAnd_test(ctx)
    end

    def enterNot_test(ctx)
    end

    def exitNot_test(ctx)
    end

    def enterComparison(ctx)
    end

    def exitComparison(ctx)
    end

    def enterComp_op(ctx)
    end

    def exitComp_op(ctx)
    end

    def enterStar_expr(ctx)
    end

    def exitStar_expr(ctx)
    end

    def enterExpr(ctx)
    end

    def exitExpr(ctx)
    end

    def enterXor_expr(ctx)
    end

    def exitXor_expr(ctx)
    end

    def enterAnd_expr(ctx)
    end

    def exitAnd_expr(ctx)
    end

    def enterShift_expr(ctx)
    end

    def exitShift_expr(ctx)
    end

    def enterArith_expr(ctx)
    end

    def exitArith_expr(ctx)
    end

    def enterTerm(ctx)
    end

    def exitTerm(ctx)
    end

    def enterFactor(ctx)
    end

    def exitFactor(ctx)
    end

    def enterPower(ctx)
    end

    def exitPower(ctx)
    end

    def enterAtom_expr(ctx)
    end

    def exitAtom_expr(ctx)
    end

    def enterAtom(ctx)
    end

    def exitAtom(ctx)
    end

    def enterTestlist_comp(ctx)
    end

    def exitTestlist_comp(ctx)
    end

    def enterTrailer(ctx)
    end

    def exitTrailer(ctx)
    end

    def enterSubscriptlist(ctx)
    end

    def exitSubscriptlist(ctx)
    end

    def enterSubscript(ctx)
    end

    def exitSubscript(ctx)
    end

    def enterSliceop(ctx)
    end

    def exitSliceop(ctx)
    end

    def enterExprlist(ctx)
    end

    def exitExprlist(ctx)
    end

    def enterTestlist(ctx)
    end

    def exitTestlist(ctx)
    end

    def enterDictorsetmaker(ctx)
    end

    def exitDictorsetmaker(ctx)
    end

    def enterClassdef(ctx)
    end

    def exitClassdef(ctx)
    end

    def enterArglist(ctx)
    end

    def exitArglist(ctx)
    end

    def enterArgument(ctx)
    end

    def exitArgument(ctx)
    end

    def enterComp_iter(ctx)
    end

    def exitComp_iter(ctx)
    end

    def enterComp_for(ctx)
    end

    def exitComp_for(ctx)
    end

    def enterComp_if(ctx)
    end

    def exitComp_if(ctx)
    end

    def enterEncoding_decl(ctx)
    end

    def exitEncoding_decl(ctx)
    end

    def enterYield_expr(ctx)
    end

    def exitYield_expr(ctx)
    end

    def enterYield_arg(ctx)
    end

    def exitYield_arg(ctx)
    end

    def enter_every_rule(ctx)
    end

    def exit_every_rule(ctx)
    end

    def visit_terminal(node)
    end

    def visit_error_node(node)
    end
  end
end
