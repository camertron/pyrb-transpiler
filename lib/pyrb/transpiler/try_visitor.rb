module Pyrb
  module Transpiler
    class TryVisitor < StatementVisitor
      def visitTry_stmt(ctx)
        ctx.children.each do |child|
          if child.is_a?(Python3Parser::SuiteContext)
            context.enter_block
            context.inside_try = true
            visit(child)
            context.inside_try = false
            context.exit_block
          elsif child.is_a?(Python3Parser::Except_clauseContext)
            if child.test
              context.write_ruby('rescue *Pyrb.rezcue(')
              visit(child.test)
              context.write_ruby(')')

              if child.NAME
                context.write_ruby(' => ')
                context.write_ruby(child.NAME.to_s)
              end
            else
              context.write_ruby('rescue')
            end

            context.write_ruby_line
          elsif child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
            keyword = child.text

            case keyword
              when 'try'
                context.write_ruby_line('begin')
              when 'finally'
                context.write_ruby_line('ensure')
              when 'else'
                context.write_ruby_line('else')
            end
          end
        end

        context.write_ruby_line("end\n")
      end
    end
  end
end
