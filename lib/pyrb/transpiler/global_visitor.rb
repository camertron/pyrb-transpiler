module Pyrb
  module Transpiler
    class GlobalVisitor < StatementVisitor
      def visitGlobal_stmt(ctx)
        ctx.NAME.each do |name|
          sym = context.current_scope.symbol_table.global_symbol(name.text)

          unless sym
            raise RuntimeError, "Could not find symbol '#{name.text}'"
          end

          sym.type = :global
        end
      end
    end
  end
end
