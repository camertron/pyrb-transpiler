module Pyrb
  module Transpiler
    class YieldVisitor < StatementVisitor
      def visitYield_stmt(ctx)
        if ctx.yield_expr.yield_arg.children[0].text == 'from'
          # indicates a "yield from" statement
          visit(ctx.yield_expr)
          context.write_ruby_line('.each { |item| __pyrb_gen__ << item }')
        else
          yielding_list = ctx.yield_expr.yield_arg.testlist.children.any? do |child|
            child.text == ','
          end

          context.write_ruby('__pyrb_gen__ << ')
          context.write_ruby('[') if yielding_list
          visit(ctx.yield_expr)
          context.write_ruby(']') if yielding_list
          context.ensure_newline
        end
      end
    end
  end
end
