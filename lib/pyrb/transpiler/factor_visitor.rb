module Pyrb
  module Transpiler
    class FactorVisitor < StatementVisitor
      def visitFactor(ctx)
        ctx.children.each do |child|
          if child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
            # this handles negative signs on numbers, eg. -1
            context.write_ruby(child.text)
          else
            visit(child)
          end
        end
      end
    end
  end
end
