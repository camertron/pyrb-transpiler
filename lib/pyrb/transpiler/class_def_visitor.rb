module Pyrb
  module Transpiler
    class ClassDefVisitor < StatementVisitor
      def visitClassdef(ctx)
        class_name = ctx.NAME.text
        ruby_class_name = Ruby.constantize(class_name)

        if context.current_scope.type == :file
          context.write_ruby("exports['")
          context.write_ruby(class_name)
          context.write_ruby("'] = ")
        else
          context.write_ruby(class_name)
          context.write_ruby(' ||= ')
        end

        context.write_ruby("Pyrb::PythonClass.new('")
        context.write_ruby(class_name)

        context.symbol_table << Symbol.new(class_name, ruby_class_name)

        context.write_ruby("', [")

        # superclasses
        if ctx.arglist
          ctx.arglist.argument.each_with_index do |argument, idx|
            context.write_ruby(', ') if idx > 0
            visit(argument)
          end
        end

        context.write_ruby('])')

        if ctx.suite.text.strip != 'pass'
          context.write_ruby_line('.tap do |klass|')

          context.enter_scope(:class)
          @context = BlockContext.enter(@context)
          context.current_scope.class_ref = context.symbol_table[class_name]

          visit(ctx.suite)

          @context = @context.exit
          context.exit_scope
          context.write_ruby_line("end\n")
        else
          context.write_ruby_line
        end

        # define ruby constant
        if context.current_scope.type == :file
          context.write_ruby(ruby_class_name)
          context.write_ruby(" = exports['")
          context.write_ruby(class_name)
          context.write_ruby_line("']\n")
        end
      end
    end
  end
end
