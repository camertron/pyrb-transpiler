module Pyrb
  module Transpiler
    class TermVisitor < StatementVisitor
      def visitTerm(ctx)
        if ctx.children.size <= 1
          visit_children(ctx)
          return
        end

        total_int_divs = count_int_divisions(ctx)

        if total_int_divs == 0
          context.write_ruby('(')
        else
          total_int_divs.times do
            context.write_ruby('Pyrb.int_divide(')
          end
        end

        open_divs = 0

        (0...ctx.children.size).step(2) do |idx|
          visit(ctx.children[idx])

          operator = if idx < ctx.children.size - 1
            ctx.children[idx + 1].text
          else
            ''
          end

          if open_divs > 0
            context.write_ruby(')')
            open_divs -= 1
          end

          if operator == '//'
            open_divs += 1
            context.write_ruby(', ')
          else
            context.write_ruby(Ruby.pad(operator))
          end
        end

        if total_int_divs == 0
          context.write_ruby(')')
        end
      end

      private

      def count_int_divisions(ctx)
        count = 0

        ctx.children.each do |child|
          if child.is_a?(Antlr4::Runtime::TerminalNodeImpl) && child.text == '//'
            count += 1
          end
        end

        count
      end
    end
  end
end
