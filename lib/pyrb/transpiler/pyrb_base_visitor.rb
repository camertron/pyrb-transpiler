module Pyrb
  module Transpiler
    class PyrbBaseVisitor < Python3BaseVisitor
      attr_reader :context

      def initialize(context)
        @context = context
      end

      private

      def each_child(ctx)
        ctx.child_count.times do |i|
          yield ctx.child_at(i)
        end
      end

      def find_next_atom_expr(ctx)
        return ctx if ctx.is_a?(Python3Parser::Atom_exprContext)

        each_child(ctx) do |child|
          atom = find_next_atom_expr(child)
          return atom if atom
        end

        nil
      end

      def find_child_in_parent_expr(ctx)
        current = ctx
        last = ctx

        while current
          if current.is_a?(Python3Parser::Expr_stmtContext)
            return ExpressionChild.new(current, current.children.index(last))
          end

          last = current
          current = current.parent
        end

        nil
      end
    end
  end
end
