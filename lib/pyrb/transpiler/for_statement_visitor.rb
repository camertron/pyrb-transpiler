module Pyrb
  module Transpiler
    class ForStatementVisitor < StatementVisitor
      def visitFor_stmt(ctx)
        # Find closest atom so we know how to iterate over it (i.e. each vs. each_char).
        iterable_atom = find_next_atom_expr(ctx.testlist)

        possibly_start_else_clause(ctx)

        if ctx.testlist.test.size > 1
          context.write_ruby('[')
        else
          context.write_ruby('(')
        end

        # enter in parens in order to call .each on the result
        visit(ctx.testlist)

        if ctx.testlist.test.size > 1
          context.write_ruby(']')
        else
          context.write_ruby(')')
        end

        if ctx.testlist.test.size == 1 && iterable_atom.atom.STRING[0]
          context.write_ruby('.each_char do |')
        else
          context.write_ruby('.each do |')
        end

        # print out all the arguments, i.e. for arg in foo
        arg_visitor = ArgListVisitor.new(context)
        arg_visitor.visitExprlist(ctx.exprlist)

        arg_visitor.args.each do |arg|
          context.write_ruby(', ') if arg.position > 0
          context.write_ruby(arg.to_param)
        end

        context.write_ruby_line('|')

        context.enter_scope(:block)
        @context = BlockContext.enter(@context)

        arg_visitor.args.each do |arg|
          context.symbol_table.override(arg.symbol)
        end

        # For loops can have more than one suite if they include an "else", which is
        # executed iff no "break" is encountered.
        visit(ctx.suite[0])
        @context = @context.exit
        context.exit_scope
        context.ensure_newline
        context.write_ruby_line('end')

        possibly_end_else_clause(ctx)
      end

      def visitComp_for(ctx)
        if ctx.parent.parent.children[0].text == '{'
          construct_dict_or_set(ctx)
        else
          construct_list(ctx)
        end
      end

      private

      def construct_dict_or_set(ctx)
        old_scope_count = context.scope_stack.size
        outermost = !ctx.parent.is_a?(Python3Parser::Comp_iterContext)

        # print out the thing we're looping over, i.e. for foo in thing
        visit(ctx.or_test)

        if outermost
            injectable = if ctx.parent.test.size == 1
              'Pyrb::Set.new'
            else
              'Pyrb::Dict.new'
            end

            context.write_ruby(".fcall('__iter__').each_with_object(#{injectable}) do |(")
        else
          context.write_ruby(".fcall('__iter__').each do |")
        end

        context.enter_scope(:block)

        # print out all the arguments, i.e. for arg in foo
        arg_visitor = ArgListVisitor.new(context)
        arg_visitor.visitExprlist(ctx.exprlist)

        arg_visitor.args.each do |arg|
          if arg.position > 0
            context.write_ruby(', ')
          end

          context.write_ruby(arg.to_param)
          context.symbol_table.override(Symbol.new(arg.symbol.python_name, arg.symbol.ruby_expression))
        end

        # close block args and enter loop scope
        if outermost
          context.write_ruby_line('), _ret_|')
        else
          context.write_ruby_line('|')
        end

        # visit any other for statements chained off this one
        if ctx.comp_iter && ctx.comp_iter.comp_for
          visit(ctx.comp_iter.comp_for)
        end

        # if this is the start of the chain, print out the return value (i.e. return_value for foo in bar)
        # and close all the scopes we've opened handling all the potentially chained loops
        if outermost
          # find the conditional
          conditional = find_conditional(ctx)
          is_conditional = conditional != nil

          if is_conditional
            # for an explanation as to why we double-negate the conditional, see the comment
            # in the IfStatementVisitor
            context.write_ruby('if !!(')
            visit(conditional)
            context.write_ruby_line(')')
            context.enter_block
          end

          parent = ctx.parent

          if parent.test.size == 1
            context.write_ruby('_ret_ << ')
            visit(parent.test[0])
          else
            context.write_ruby('_ret_[')
            visit(parent.test[0])
            context.write_ruby('] = ')
            visit(parent.test[1])
          end

          context.ensure_newline

          # exit the conditional
          if is_conditional
            context.exit_block
            context.write_ruby_line('end')
          end

          new_scope_count = context.scope_stack.size

          # Close all the open blocks.
          # We have to do this at the end because the outermost loop is the one
          # that contains the test, meaning by the time we get here the inner
          # loops will have already been closed. Instead we intentionally leave
          # the chained loops open and leave it to the outermost loop to close
          # everything based on how many scopes were entered.
          (new_scope_count - old_scope_count).times do
            context.exit_scope
            context.write_ruby_line('end')
          end
        end
      end

      def construct_list(ctx)
        old_scope_count = context.scope_stack.size
        is_conditional = false

        # print out the thing we're looping over, i.e. for foo in thing
        visit(ctx.or_test)

        context.write_ruby(".fcall('__iter__')")

        if ctx.comp_iter
          if ctx.comp_iter.comp_if
            # for loop with a conditional
            is_conditional = true
            context.write_ruby('.each_with_object([]) do |')
          elsif ctx.comp_iter.comp_for
            # for loop with a chained loop after
            context.write_ruby('.flat_map do |')
          else
            # for loop with no chained loop OR conditional
            context.write_ruby('.map do |')
          end
        else
          # for loop with no conditional or chained loop...
          context.write_ruby('.map do |')
        end

        context.enter_scope(:block)

        # enable arg deconstruction
        if ctx.exprlist.expr.size > 1
          context.write_ruby('(')
        end

        # print out all the arguments, i.e. for arg in foo
        ctx.exprlist.children.each do |child|
          if child.is_a?(Python3Parser::ExprContext)
            # Find the atom and loop over its children, emitting exactly what
            # we find. We do this in the interest of argument deconstruction.
            # If the exprlist's only element is a tuple, then the length of
            # the list itself is only 1 and will not cause the above code to
            # emit enclosing parens. Instead we have to look at the closest
            # atom (i.e. the arguments passed to the loop's block). It is not
            # sufficient to simply call visit() on the child because the atom
            # expression visitor is designed to turn what it thinks are tuples
            # into arrays, so `for (first, second) in foo` becomes
            # `foo.each do |[first, second]|`. To prevent this, we emit what
            # Python considers tuple parens and what Ruby thinks are arg
            # deconstruction parens (fortunately they happen to be equivalent
            # in this case). As a result, we end up emitting
            # `foo.each do |(first, second)|`.
            atom_expr = find_next_atom_expr(child)

            atom_expr.atom.children.each do |atom_child|
              if atom_child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
                context.write_ruby(atom_child.text)
              else
                visit(atom_child)
              end
            end
          elsif child.is_a?(Antlr4::Runtime::TerminalNodeImpl)
            # handle things like commas, splats, etc
            context.write_ruby(Ruby.pad(child.text))
          end
        end

        if ctx.exprlist.expr.size > 1
          context.write_ruby(')')
        end

        # Conditional for loops validate before returning. To support this
        # case, we use each_with_object([]) and conditionally add items to
        # the _ret_ array.
        if is_conditional
          context.write_ruby(', _ret_')
        end

        # close block args and enter loop scope
        context.write_ruby_line('|')

        # visit any other for statements chained off this one
        if ctx.comp_iter && ctx.comp_iter.comp_for
          visit(ctx.comp_iter.comp_for)
        end

        # if this is the start of the chain, print out the return value (i.e.
        # return_value for foo in bar) and close all the scopes we've opened
        # handling all the potentially chained loops
        if !ctx.parent.is_a?(Python3Parser::Comp_iterContext)
          # find the conditional
          conditional = find_conditional(ctx)
          is_conditional = !conditional.nil?

          if is_conditional
            # for an explanation as to why we double-negate the conditional,
            # see the comment in the IfStatementVisitor
            context.write_ruby('if !!(')
            visit(conditional)
            context.write_ruby_line(')')
            context.enter_block
            context.write_ruby('_ret_ << ')
          end

          if ctx.parent.children[0].is_a?(Python3Parser::TestContext)
            visit(ctx.parent.children[0])
          end

          context.ensure_newline

          # exit the conditional
          if is_conditional
            context.exit_block
            context.write_ruby_line('end')
          end

          new_scope_count = context.scope_stack.size

          # Close all the open blocks.
          # We have to do this at the end because the outermost loop is the one
          # that contains the test, meaning by the time we get here the inner
          # loops will have already been closed. Instead we intentionally leave
          # the chained loops open and leave it to the outermost loop to close
          # everything based on how many scopes were entered.
          (new_scope_count - old_scope_count).times do
            context.exit_scope
            context.write_ruby_line('end')
          end
        end
      end

      def find_conditional(ctx)
        innermost = ctx

        while innermost.comp_iter && innermost.comp_iter.comp_for
          innermost = innermost.comp_iter.comp_for
        end

        if innermost.comp_iter
          return innermost.comp_iter.comp_if
        end

        nil
      end

      def possibly_start_else_clause(ctx)
        # indicates a for statement with an else clause, wrap with catch
        if ctx.suite.size > 1
          context.write_ruby_line('catch(:break) do')
          context.enter_scope(:for_loop_with_else)
        end
      end

      def possibly_end_else_clause(ctx)
        if ctx.suite.size > 1
          # causes the catch() to return false if no break was encountered
          context.write_ruby_line('false')
          context.exit_scope
          context.write_ruby_line('end.tap do |_caught|')
          context.enter_scope(:block)
          context.write_ruby_line('next if _caught')
          visit(ctx.suite[1])
          context.exit_scope
          context.write_ruby_line("end\n")
        end
      end
    end
  end
end
