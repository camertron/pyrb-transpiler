$:.unshift File.join(File.dirname(__FILE__), 'lib')
require 'pyrb/transpiler/version'

Gem::Specification.new do |s|
  s.name     = 'pyrb-transpiler'
  s.version  = ::Pyrb::Transpiler::VERSION
  s.authors  = ['Cameron Dutro']
  s.email    = ['camertron@gmail.com']
  s.homepage = 'http://gitlab.com/camertron/pyrb-transpiler'

  s.description = s.summary = 'Python to Ruby transpiler.'
  s.platform = Gem::Platform::RUBY

  s.add_dependency 'antlr4-runtime', '~> 0.2'

  s.require_path = 'lib'
  s.files = Dir['{lib,spec}/**/*', 'Gemfile', 'CHANGELOG.md', 'README.md', 'Rakefile', 'pyrb-transpiler.gemspec']
end
