require 'pyrb/transpiler'
require 'pry-byebug'

# puts Pyrb::Transpiler.transpile('main.py', "a = 'a\\\\bc'\n")
# puts Pyrb::Transpiler.transpile('main.py', "f\"Hello, {name}. You are {age}.\"\n")
# puts Pyrb::Transpiler.transpile('main.py', "\"abc \\xE9 def\"\n")
# puts Pyrb::Transpiler.transpile('main.py', "abc[::1]\n")
# puts Pyrb::Transpiler.transpile('main.py', "a = b\n")
# puts Pyrb::Transpiler.transpile('main.py', "def foo(self):\n  self.foo = 'bar'\n")
# puts Pyrb::Transpiler.transpile('main.py', "def foo(self):\n  return self.foo['bar']\n")
# puts Pyrb::Transpiler.transpile('main.py', "3 in [1, 2, 3]\n")
puts Pyrb::Transpiler.transpile('main.py', "'ümläut'\n")
# puts Pyrb::Transpiler.transpile('hamming.py', File.read('/Users/cameron/workspace/pyrb-runtime/spec/integration/source/hamming.py'))
