require 'pyrb/transpiler'
require 'ruby-prof'
require 'pry-byebug'
require 'benchmark/ips'
require 'benchmark'

STDOUT.sync = true

code = File.read('/Users/cameron/workspace/pyrb-runtime/lib/pyrb/heapq.py')
# code = File.read('/Users/cameron/workspace/pyrb-runtime/lib/pyrb/bisect.py')
# $tally = Hash.new { |h, k| h[k] = 0 }
# $visitor_total = 0
# $dfa = { hit: 0, miss: 0 }

# Class.prepend(Module.new do
#   def new(*args, &block)
#     $tally[self.name] += 1
#     $visitor_total += 1 if self.name.end_with?('Visitor')
#     super
#   end
# end)

# Pyrb::Transpiler.transpile('heapq.py', code, STDOUT)
# $tally.keys.sort_by { |k| $tally[k] }.each do |k|
#   puts "#{k} => #{$tally[k]}"
# end

# puts "\nVisitor total: #{$visitor_total}"
# puts "\nGrand total: #{$tally.values.inject(0) { |sum, val| sum + val }}"
# puts "DFA metrics: #{$dfa.inspect}"
# exit 0


# require 'rumourhash/rumourhash'

# arr = (1..100).to_a

# Benchmark.ips do |x|
#   # x.report('mixed types') { RumourHash.calculate([Class.new, nil, false, 1, 2, false, nil, true]) }
#   x.report('all ints') { RumourHash.calculate([123, 0, 0, 1123432, 21231232, 0, 0, 1]) }
#   # x.report('array') { RumourHash.rumour_hash(arr) }
#   # x.report('int updates') do
#   #   hash_code = 7

#   #   arr.each do |i|
#   #     hash_code = RumourHash.rumour_hash_update_int(hash_code, i)
#   #   end

#   #   RumourHash.rumour_hash_finish(hash_code, 3)
#   # end

#   # x.compare!
# end

# exit 0


if ENV['PROF'] == 'true'
  # RubyProf.start
  # RubyProf.pause
  result = RubyProf.profile do
    puts Pyrb::Transpiler.transpile('heapq.py', code)
  end
  # result = RubyProf.stop

  printer = RubyProf::FlatPrinter.new(result)
  printer.print(STDOUT)
  exit 0
end



puts(Benchmark.measure do
  Pyrb::Transpiler.transpile('heapq.py', code)
end)
